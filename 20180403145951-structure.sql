﻿USE [Celere]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- LIMPEZA DAS TABELAS INTERNAS
DELETE FROM _Authorizations;
DELETE FROM _Transactions;
DELETE FROM _Usergroups;
DELETE FROM _Tasks;
DELETE FROM _Groups;
DELETE FROM _Auth;
DELETE FROM _Users;
DELETE FROM _Devices;


-- RESET DOS ID'S
DBCC CHECKIDENT ('_Authorizations', RESEED, 0);
DBCC CHECKIDENT ('_Transactions', RESEED, 0);
DBCC CHECKIDENT ('_Usergroups', RESEED, 0);
DBCC CHECKIDENT ('_Tasks', RESEED, 0);
DBCC CHECKIDENT ('_Groups', RESEED, 0);
DBCC CHECKIDENT ('_Auth', RESEED, 0);
DBCC CHECKIDENT ('_Devices', RESEED, 0);


-- TAREFAS
SET IDENTITY_INSERT _Tasks ON;

INSERT INTO _Tasks (TaskId, Name, Description, EnumTask) VALUES (1, 'ControlPanelAccess', 'Acesso ao Painel de Controle', 1);
INSERT INTO _Tasks (TaskId, Name, Description, EnumTask) VALUES (2, 'ManageUsers', 'Gerenciar Usuários', 2);
INSERT INTO _Tasks (TaskId, Name, Description, EnumTask) VALUES (3, 'ManageGroups', 'Gerenciar Grupos de Usuários', 3);
INSERT INTO _Tasks (TaskId, Name, Description, EnumTask) VALUES (4, 'ManageLogs', 'Gerenciar Logs', 4);
INSERT INTO _Tasks (TaskId, Name, Description, EnumTask) VALUES (5, 'ManageParameters', 'Gerenciar Parametros', 5);
INSERT INTO _Tasks (TaskId, Name, Description, EnumTask) VALUES (6, 'ManageTasks', 'Gerenciar Tarefas', 6);
INSERT INTO _Tasks (TaskId, Name, Description, EnumTask) VALUES (7, 'ManageAddresses', 'Gerenciar Addresses', 7);
INSERT INTO _Tasks (TaskId, Name, Description, EnumTask) VALUES (8, 'ManageAttempts', 'Gerenciar Attempts', 8);
INSERT INTO _Tasks (TaskId, Name, Description, EnumTask) VALUES (9, 'ManageCities', 'Gerenciar Cities', 9);
INSERT INTO _Tasks (TaskId, Name, Description, EnumTask) VALUES (10, 'ManageCustomers', 'Gerenciar Customers', 10);
INSERT INTO _Tasks (TaskId, Name, Description, EnumTask) VALUES (11, 'ManageLogTrackingCodes', 'Gerenciar LogTrackingCodes', 11);
INSERT INTO _Tasks (TaskId, Name, Description, EnumTask) VALUES (12, 'ManageMovements', 'Gerenciar Movements', 12);
INSERT INTO _Tasks (TaskId, Name, Description, EnumTask) VALUES (13, 'ManageNotifications', 'Gerenciar Notifications', 13);
INSERT INTO _Tasks (TaskId, Name, Description, EnumTask) VALUES (14, 'ManageNotificationTypes', 'Gerenciar NotificationTypes', 14);
INSERT INTO _Tasks (TaskId, Name, Description, EnumTask) VALUES (15, 'ManageOperationTypes', 'Gerenciar OperationTypes', 15);
INSERT INTO _Tasks (TaskId, Name, Description, EnumTask) VALUES (16, 'ManageParts', 'Gerenciar Parts', 16);
INSERT INTO _Tasks (TaskId, Name, Description, EnumTask) VALUES (17, 'ManagePlans', 'Gerenciar Plans', 17);
INSERT INTO _Tasks (TaskId, Name, Description, EnumTask) VALUES (18, 'ManageProcesses', 'Gerenciar Processes', 18);
INSERT INTO _Tasks (TaskId, Name, Description, EnumTask) VALUES (19, 'ManageSchedulerCreations', 'Gerenciar SchedulerCreations', 19);
INSERT INTO _Tasks (TaskId, Name, Description, EnumTask) VALUES (20, 'ManageSchedulerStatus', 'Gerenciar SchedulerStatus', 20);
INSERT INTO _Tasks (TaskId, Name, Description, EnumTask) VALUES (21, 'ManageSchedulerTrackingCodes', 'Gerenciar SchedulerTrackingCodes', 21);
INSERT INTO _Tasks (TaskId, Name, Description, EnumTask) VALUES (22, 'ManageStates', 'Gerenciar States', 22);
INSERT INTO _Tasks (TaskId, Name, Description, EnumTask) VALUES (23, 'ManageTrackingCodes', 'Gerenciar TrackingCodes', 23);

SET IDENTITY_INSERT _Tasks OFF;


-- GRUPOS
SET IDENTITY_INSERT _Groups ON;

INSERT INTO _Groups (GroupId, Name, IsActive) VALUES (1, 'Administrators', 1);

SET IDENTITY_INSERT _Groups OFF;


-- AUTH
SET IDENTITY_INSERT _Auth ON;

INSERT INTO _Auth (
	AuthId,
	Email,
	Password,
	IsEmailConfirmed,
	IsActive
)  VALUES (
	1,
	'dev@asteria.com.br',
	'sha1:64000:18:SeQs60corkfDi4QDfV6bypmEII9Vjv35:QLt1B/W3h1AcpeiuFjxcwOi8',
	1,
	1
);

SET IDENTITY_INSERT _Auth OFF;


-- USUÁRIOS
INSERT INTO _Users (
	AuthId,
	Name,
	ImageAvatar,
	EnumLanguage,
	CreatedOn,
	IsActive
)  VALUES (
	1,
	'Administrator',
	'default.png',
	1,
	GETUTCDATE(),
	1
);


-- USERGROUPS
SET IDENTITY_INSERT _Usergroups ON;

INSERT INTO _Usergroups (UsergroupId, GroupId, AuthId) VALUES (1, 1, 1);

SET IDENTITY_INSERT _Usergroups OFF;


-- AUTHORIZATIONS
SET IDENTITY_INSERT _Authorizations ON;

INSERT INTO _Authorizations (AuthorizationId, TaskId, GroupId) VALUES (1, 1, 1);
INSERT INTO _Authorizations (AuthorizationId, TaskId, GroupId) VALUES (2, 2, 1);
INSERT INTO _Authorizations (AuthorizationId, TaskId, GroupId) VALUES (3, 3, 1);
INSERT INTO _Authorizations (AuthorizationId, TaskId, GroupId) VALUES (4, 4, 1);
INSERT INTO _Authorizations (AuthorizationId, TaskId, GroupId) VALUES (5, 5, 1);
INSERT INTO _Authorizations (AuthorizationId, TaskId, GroupId) VALUES (6, 6, 1);
INSERT INTO _Authorizations (AuthorizationId, TaskId, GroupId) VALUES (7, 7, 1);
INSERT INTO _Authorizations (AuthorizationId, TaskId, GroupId) VALUES (8, 8, 1);
INSERT INTO _Authorizations (AuthorizationId, TaskId, GroupId) VALUES (9, 9, 1);
INSERT INTO _Authorizations (AuthorizationId, TaskId, GroupId) VALUES (10, 10, 1);
INSERT INTO _Authorizations (AuthorizationId, TaskId, GroupId) VALUES (11, 11, 1);
INSERT INTO _Authorizations (AuthorizationId, TaskId, GroupId) VALUES (12, 12, 1);
INSERT INTO _Authorizations (AuthorizationId, TaskId, GroupId) VALUES (13, 13, 1);
INSERT INTO _Authorizations (AuthorizationId, TaskId, GroupId) VALUES (14, 14, 1);
INSERT INTO _Authorizations (AuthorizationId, TaskId, GroupId) VALUES (15, 15, 1);
INSERT INTO _Authorizations (AuthorizationId, TaskId, GroupId) VALUES (16, 16, 1);
INSERT INTO _Authorizations (AuthorizationId, TaskId, GroupId) VALUES (17, 17, 1);
INSERT INTO _Authorizations (AuthorizationId, TaskId, GroupId) VALUES (18, 18, 1);
INSERT INTO _Authorizations (AuthorizationId, TaskId, GroupId) VALUES (19, 19, 1);
INSERT INTO _Authorizations (AuthorizationId, TaskId, GroupId) VALUES (20, 20, 1);
INSERT INTO _Authorizations (AuthorizationId, TaskId, GroupId) VALUES (21, 21, 1);
INSERT INTO _Authorizations (AuthorizationId, TaskId, GroupId) VALUES (22, 22, 1);
INSERT INTO _Authorizations (AuthorizationId, TaskId, GroupId) VALUES (23, 23, 1);

SET IDENTITY_INSERT _Authorizations OFF;


-- PROCEDURES
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('__spCheckPermission'))
	DROP PROCEDURE __spCheckPermission;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('__spUsersPage'))
	DROP PROCEDURE __spUsersPage;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('__spUserGet'))
	DROP PROCEDURE __spUserGet;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('__spUsersListIds'))
	DROP PROCEDURE __spUsersListIds;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('__spTransactionInsert'))
	DROP PROCEDURE __spTransactionInsert;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('__spTransactionsGet'))
	DROP PROCEDURE __spTransactionsGet;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('__spUserInsert'))
	DROP PROCEDURE __spUserInsert;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('__spUserUpdate'))
	DROP PROCEDURE __spUserUpdate;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('__spUserDelete'))
	DROP PROCEDURE __spUserDelete;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('__spAuthGet'))
	DROP PROCEDURE __spAuthGet;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('__spAuthDelete'))
	DROP PROCEDURE __spAuthDelete;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('__spUserPermissionsGet'))
	DROP PROCEDURE __spUserPermissionsGet;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('__spAuthRefreshToken'))
	DROP PROCEDURE __spAuthRefreshToken;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('__spAuthUpdatePassword'))
	DROP PROCEDURE __spAuthUpdatePassword;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('__spDashboardGet'))
	DROP PROCEDURE __spDashboardGet;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('__spTasksList'))
	DROP PROCEDURE __spTasksList;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('__spParametersPage'))
	DROP PROCEDURE __spParametersPage;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('__spParameterUpdate'))
	DROP PROCEDURE __spParameterUpdate;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('__spLogsPage'))
	DROP PROCEDURE __spLogsPage;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('__spGroupsPage'))
	DROP PROCEDURE __spGroupsPage;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('__spGroupsListIds'))
	DROP PROCEDURE __spGroupsListIds;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('__spGroupInsert'))
	DROP PROCEDURE __spGroupInsert;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('__spGroupUpdate'))
	DROP PROCEDURE __spGroupUpdate;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('__spGroupDelete'))
	DROP PROCEDURE __spGroupDelete;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('__spReportUsers'))
	DROP PROCEDURE __spReportUsers;
GO

CREATE PROCEDURE __spCheckPermission

	@AuthId INT,
	@EnumTask INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT CASE WHEN EXISTS (
		SELECT
			*
		FROM
			_Tasks T
		INNER JOIN
			_Authorizations A1
		ON
			A1.TaskId = T.TaskId
		INNER JOIN
			_Groups G
		ON
			G.GroupId = A1.GroupId
		INNER JOIN
			_Usergroups UG
		ON
			UG.GroupId = G.GroupId
		INNER JOIN
			_Users U
		ON
			U.AuthId = UG.AuthId
		INNER JOIN
			_Auth A2
		ON
			A2.AuthId = U.AuthId
		WHERE
			T.EnumTask = @EnumTask
		AND
			A2.AuthId = @AuthId
		AND	
			A2.IsActive = 1
		AND	
			U.IsActive = 1
		AND
			G.IsActive = 1
	)
	THEN CAST(1 AS BIT)
	ELSE CAST(0 AS BIT) END;

END
GO

CREATE PROCEDURE __spUsersPage

	@Page int = 0,
	@PageRows int = 1,
	@Keyword nvarchar(64) = NULL,
	@OrderBy nvarchar(64) = NULL,

	@Email NVARCHAR(64) = NULL,
	@AuthId INT = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL DROP TABLE #TEMP;

	WITH TempResult AS (
		SELECT
			A.AuthId,
			U.Name,
			U.ImageAvatar,
			U.EnumLanguage,
			A.Email,
			A.Password,
			A.PasswordResetToken,
			A.FacebookId,
			A.FacebookAccessToken,
			A.IsEmailConfirmed,
			A.EmailConfirmationToken,
			A.EmailConfirmationExpiryDate,
			A.IsActive IsAuthActive,
			U.IsActive IsUserActive
		FROM
			_Users U
		INNER JOIN
			_Auth A
		ON
			A.AuthId = U.AuthId
		WHERE
			(@Email IS NULL OR A.Email = @Email)
		AND
			(@AuthId IS NULL OR A.AuthId = @AuthId)
		AND
			(@Keyword IS NULL OR U.Name LIKE '%' + @Keyword + '%')
		AND
			U.IsActive = 1
		AND
			A.IsActive = 1
	), TempCount AS (
		SELECT COUNT(*) AS MaxRows FROM TempResult
	)
	SELECT
		*
	INTO
		#TEMP
	FROM
		TempResult,
		TempCount
	ORDER BY
		TempResult.Name ASC
	OFFSET
		@Page * @PageRows ROWS
	FETCH NEXT
		@PageRows ROWS ONLY;

	SELECT * FROM #TEMP;

	IF EXISTS(SELECT TOP 1 MaxRows FROM #TEMP)
		SELECT TOP 1 MaxRows FROM #TEMP
	ELSE
		SELECT 0 AS MaxRows;

END
GO

CREATE PROCEDURE __spUserGet

    @AuthId INT

AS
BEGIN

	SELECT
		U.AuthId,
		U.Name,
		U.ImageAvatar,
		U.EnumLanguage,
		U.CreatedOn,
		U.IsActive IsUserActive,
		A.Email,
		A.Password,
		A.PasswordResetToken,
		A.PasswordResetExpiryDate,
		A.FacebookId,
		A.FacebookAccessToken,
		A.IsEmailConfirmed,
		A.EmailConfirmationToken,
		A.EmailConfirmationExpiryDate,
		A.IsActive IsAuthActive
	FROM
		_Users U
	INNER JOIN
		_Auth A
	ON
		A.AuthId = U.AuthId
	WHERE
		U.AuthId = @AuthId
	AND
		U.IsActive = 1
	AND
		A.IsActive = 1;

END
GO

CREATE PROCEDURE __spUsersListIds

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		U.AuthId AS [Key],
		U.Name AS Value
	FROM
		_Users U
	WHERE
		U.IsActive = 1
	ORDER BY
		U.Name ASC;

END
GO

CREATE PROCEDURE __spTransactionInsert

	@TableName NVARCHAR(64),
	@TableId INT,
	@Type NVARCHAR(16),
	@AuthId INT = NULL,
	@IP NVARCHAR(39) = NULL

AS
BEGIN
	INSERT INTO _Transactions (
		TableName,
		TableId,
		AuthId,
		Date,
		Type,
		IP
	) VALUES (
		@TableName,
		@TableId,
		@AuthId,
		GETUTCDATE(),
		@Type,
		@IP
	);
END
GO

CREATE PROCEDURE __spTransactionsGet

	@TableName NVARCHAR(64),
	@TableId INT

AS
BEGIN
	SELECT
		T.Date,
		T.Type,
		T.AuthId,
		U.Name,
		A.Email,
		T.IP
	FROM
		_Transactions T
	LEFT JOIN
		_Users U
	ON
		U.AuthId = T.AuthId
	INNER JOIN
		_Auth A
	ON
		A.AuthId = U.AuthId
	WHERE
		T.TableName = @TableName
	AND
		T.TableId = @TableId
	ORDER BY
		T.Date ASC;
END
GO

CREATE PROCEDURE __spUserInsert

	@Name NVARCHAR(32),
	@Email NVARCHAR(64),
	@Password CHAR(71),
	@FacebookId NVARCHAR(128),
	@FacebookAccessToken NVARCHAR(MAX),
	@IsEmailConfirmed BIT,
	@ImageAvatar NVARCHAR(128) = NULL,
	@EmailConfirmationToken CHAR(71) = NULL,
	@EmailConfirmationExpiryDate DATETIME2 = NULL,
	@EnumLanguage TINYINT,
	@CreatedOn DATETIME2,
	@IsActive BIT,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY

		INSERT INTO _Auth
		(
			Email,
			Password,
			FacebookId,
			FacebookAccessToken,
			IsEmailConfirmed,
			EmailConfirmationToken,
			EmailConfirmationExpiryDate,
			IsActive
		)
		VALUES
		(
			@Email,
			@Password,
			@FacebookId,
			@FacebookAccessToken,
			@IsEmailConfirmed,
			@EmailConfirmationToken,
			@EmailConfirmationExpiryDate,
			@IsActive
		);

		DECLARE @AuthId INT = SCOPE_IDENTITY();
		
		INSERT INTO _Users
		(
			AuthId,
			Name,
			ImageAvatar,
			EnumLanguage,
			CreatedOn,
			IsActive
		)
		VALUES
		(
			@AuthId,
			@Name,
			@ImageAvatar,
			@EnumLanguage,
			@CreatedOn,
			@IsActive
		);

		EXEC __spTransactionInsert '_Users', @AuthId, 'INSERT', @__AuthId, @__AuthIP;

		SELECT @AuthId;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END
GO

CREATE PROCEDURE __spUserUpdate

	@AuthId INT,
	@Name NVARCHAR(32),
	@Email NVARCHAR(64),
	@Password CHAR(71),
	@FacebookId NVARCHAR(128),
	@FacebookAccessToken NVARCHAR(MAX),
	@IsEmailConfirmed BIT,
	@ImageAvatar NVARCHAR(128) = NULL,
	@EmailConfirmationToken CHAR(71) = NULL,
	@EmailConfirmationExpiryDate DATETIME2 = NULL,
	@EnumLanguage TINYINT,
	@IsActive BIT,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY

		UPDATE
			_Auth
		SET
			Email = @Email,
			Password = @Password,
			FacebookId = @FacebookId,
			FacebookAccessToken = @FacebookAccessToken,
			IsEmailConfirmed = @IsEmailConfirmed,
			EmailConfirmationToken = @EmailConfirmationToken,
			EmailConfirmationExpiryDate = @EmailConfirmationExpiryDate
		WHERE
			AuthId = @AuthId;

		UPDATE
			_Users
		SET 
			Name = @Name,
			ImageAvatar = @ImageAvatar,
			EnumLanguage = @EnumLanguage,
			IsActive = @IsActive
		WHERE
			AuthId = @AuthId;

		EXEC __spTransactionInsert '_Users', @AuthId, 'UPDATE', @__AuthId, @__AuthIP;

		SELECT @AuthId;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END
GO

CREATE PROCEDURE __spUserDelete

	@AuthId INT,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY

		UPDATE _Users SET IsActive = 0 WHERE AuthId = @AuthId;

		EXEC __spTransactionInsert '_Users', @AuthId, 'DELETE', @__AuthId, @__AuthIP;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END
GO

CREATE PROCEDURE __spAuthGet

	@Email NVARCHAR(64) = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		AuthId,
		Email,
		Password,
		PasswordResetToken,
		PasswordResetExpiryDate,
		FacebookId,
		FacebookAccessToken,
		IsEmailConfirmed,
		EmailConfirmationToken,
		EmailConfirmationExpiryDate,
		IsActive	IsAuthActive
	FROM
		_Auth
	WHERE
		Email = @Email
	AND
		IsActive = 1;

END
GO

CREATE PROCEDURE __spAuthDelete

	@AuthId INT,
	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY

		UPDATE _Auth SET IsActive = 0 WHERE AuthId = @AuthId;

		EXEC __spTransactionInsert '_Auth', @AuthId, 'DELETE', @__AuthId, @__AuthIP;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END
GO

CREATE PROCEDURE __spUserPermissionsGet

	@AuthId INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT
		T.EnumTask
	FROM
		_Tasks T
	INNER JOIN
		_Authorizations A
	ON
		A.TaskId = T.TaskId
	INNER JOIN
		_Groups G
	ON
		G.GroupId = A.GroupId
	INNER JOIN
		_Usergroups UG
	ON
		UG.GroupId = G.GroupId
	AND
		UG.AuthId = @AuthId
	WHERE
		G.IsActive = 1;

END
GO

CREATE PROCEDURE __spAuthRefreshToken

	@Email NVARCHAR(64),
	@EmailConfirmationToken CHAR(71),
	@EmailConfirmationExpiryDate DATETIME

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE
		_Auth
	SET
		EmailConfirmationToken = @EmailConfirmationToken,
		EmailConfirmationExpiryDate = @EmailConfirmationExpiryDate
	WHERE
		Email = @Email;

END
GO

CREATE PROCEDURE __spAuthUpdatePassword

	@Email NVARCHAR(64),
	@Password CHAR(71)

AS
BEGIN
	UPDATE
		_Auth
	SET 
		Password = @Password,
		EmailConfirmationToken = NULL,
		EmailConfirmationExpiryDate = NULL
	WHERE
		Email = @Email;

END
GO

CREATE PROCEDURE __spDashboardGet

AS
BEGIN
	-- Total de Addresses
	SELECT COUNT(*) FROM Addresses WHERE IsActive = 1;

	-- Total de Attempts
	SELECT COUNT(*) FROM Attempts;

	-- Total de Cities
	SELECT COUNT(*) FROM Cities;

	-- Total de Customers
	SELECT COUNT(*) FROM Customers;

	-- Total de LogTrackingCodes
	SELECT COUNT(*) FROM LogTrackingCodes;

	-- Total de Movements
	SELECT COUNT(*) FROM Movements;

	-- Total de Notifications
	SELECT COUNT(*) FROM Notifications;

	-- Total de NotificationTypes
	SELECT COUNT(*) FROM NotificationTypes;

	-- Total de OperationTypes
	SELECT COUNT(*) FROM OperationTypes;

	-- Total de Parts
	SELECT COUNT(*) FROM Parts;

	-- Total de Plans
	SELECT COUNT(*) FROM Plans WHERE IsActive = 1;

	-- Total de Processes
	SELECT COUNT(*) FROM Processes;

	-- Total de SchedulerCreations
	SELECT COUNT(*) FROM SchedulerCreations;

	-- Total de SchedulerStatus
	SELECT COUNT(*) FROM SchedulerStatus;

	-- Total de SchedulerTrackingCodes
	SELECT COUNT(*) FROM SchedulerTrackingCodes;

	-- Total de States
	SELECT COUNT(*) FROM States;

	-- Total de TrackingCodes
	SELECT COUNT(*) FROM TrackingCodes WHERE IsActive = 1;

END
GO

CREATE PROCEDURE __spTasksList

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		T.TaskId AS [Key],
		T.Name AS Value
	FROM
		_Tasks T
	ORDER BY
		T.Name ASC;

END
GO

CREATE PROCEDURE __spParametersPage

	@Page int = 0,
	@PageRows int = 1,
	@Keyword nvarchar(64) = NULL,
    @OrderBy nvarchar(64) = NULL,

    @ParameterId INT = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL DROP TABLE #TEMP;

	WITH TempResult AS (
		SELECT
			ParameterId,
			Entry,
			Value,
			ValueDev,
			ValueQA,
			Description,
			IsReadOnly
		FROM
			_Parameters
		WHERE
			(@ParameterId IS NULL OR ParameterId = @ParameterId)
		AND
			(
				@Keyword IS NULL
			OR
				Entry LIKE '%' + @Keyword + '%'
			OR
				Value LIKE '%' + @Keyword + '%'
			OR
				ValueDev LIKE '%' + @Keyword + '%'
			OR
				ValueQA LIKE '%' + @Keyword + '%'
			OR
				Description LIKE '%' + @Keyword + '%'
			)
	), TempCount AS (
		SELECT COUNT(*) AS MaxRows FROM TempResult
	)
	SELECT
		*
	INTO
		#TEMP
	FROM
		TempResult,
		TempCount
	ORDER BY
		TempResult.Entry ASC
	OFFSET
		@Page * @PageRows ROWS
	FETCH NEXT
		@PageRows ROWS ONLY;

	SELECT * FROM #TEMP;

	IF EXISTS(SELECT TOP 1 MaxRows FROM #TEMP)
		SELECT TOP 1 MaxRows FROM #TEMP
	ELSE
		SELECT 0 AS MaxRows;

END
GO

CREATE PROCEDURE __spParameterUpdate

	@ParameterId INT,
	@Entry NVARCHAR(32),
	@Value NVARCHAR(256),
	@ValueDev NVARCHAR(256) = NULL,
	@ValueQA NVARCHAR(256) = NULL,
	@Description NVARCHAR(64),
	@IsReadOnly BIT,
	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

	BEGIN TRY

		UPDATE
			_Parameters
		SET
			Entry = @Entry,
			Value = @Value,
			ValueDev = @ValueDev,
			ValueQA = @ValueQA,
			Description = @Description,
			IsReadOnly = @IsReadOnly
		WHERE
			ParameterId = @ParameterId;

		EXEC __spTransactionInsert '_Parameters', @ParameterId, 'UPDATE', @__AuthId, @__AuthIP;

		SELECT @ParameterId;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END
GO

CREATE PROCEDURE __spLogsPage

	@Page int = 0,
	@PageRows int = 1,
	@Keyword nvarchar(64) = NULL,
    @OrderBy nvarchar(64) = NULL,

    @LogId INT = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL DROP TABLE #TEMP;

	WITH TempResult AS (
		SELECT
			LogId,
			Date,
			Thread,
			Level,
			Logger,
			Message,
			Exception,
			EnumLogSource
		FROM
			_Logs
		WHERE
			(@LogId IS NULL OR LogId = @LogId)
		AND
			(
				@Keyword IS NULL
			OR
				Thread LIKE '%' + @Keyword + '%'
			OR
				Level LIKE '%' + @Keyword + '%'
			OR
				Logger LIKE '%' + @Keyword + '%'
			)
	), TempCount AS (
		SELECT COUNT(*) AS MaxRows FROM TempResult
	)
	SELECT
		*
	INTO
		#TEMP
	FROM
		TempResult,
		TempCount
	ORDER BY
		TempResult.Thread ASC
	OFFSET
		@Page * @PageRows ROWS
	FETCH NEXT
		@PageRows ROWS ONLY;

	SELECT * FROM #TEMP;

	IF EXISTS(SELECT TOP 1 MaxRows FROM #TEMP)
		SELECT TOP 1 MaxRows FROM #TEMP
	ELSE
		SELECT 0 AS MaxRows;

END
GO

CREATE PROCEDURE __spGroupsPage

	@Page int = 0,
	@PageRows int = 1,
	@Keyword nvarchar(64) = NULL,
    @OrderBy nvarchar(64) = NULL,

    @GroupId INT = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL DROP TABLE #TEMP;

	WITH TempResult AS (
		SELECT
			GroupId,
			Name,
			IsActive
		FROM
			_Groups
		WHERE
			(@GroupId IS NULL OR GroupId = @GroupId)
		AND
			(@Keyword IS NULL OR Name LIKE '%' + @Keyword + '%')
		AND
			IsActive = 1
	), TempCount AS (
		SELECT COUNT(*) AS MaxRows FROM TempResult
	)
	SELECT
		*
	INTO
		#TEMP
	FROM
		TempResult,
		TempCount
	ORDER BY
		TempResult.Name ASC
	OFFSET
		@Page * @PageRows ROWS
	FETCH NEXT
		@PageRows ROWS ONLY;

	SELECT * FROM #TEMP;

	IF EXISTS(SELECT TOP 1 MaxRows FROM #TEMP)
		SELECT TOP 1 MaxRows FROM #TEMP
	ELSE
		SELECT 0 AS MaxRows;

END
GO

CREATE PROCEDURE __spGroupsListIds

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		GroupId AS [Key],
		Name AS Value
	FROM
		_Groups
	WHERE
		IsActive = 1
	ORDER BY
		Name ASC;

END
GO

CREATE PROCEDURE __spGroupInsert

	@Name NVARCHAR(32),
	@IsActive BIT,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY

		INSERT INTO _Groups
		(
			Name,
			IsActive
		)
		VALUES
		(
			@Name,
			@IsActive
		);

		DECLARE @GroupId INT = SCOPE_IDENTITY();

		EXEC __spTransactionInsert '_Groups', @GroupId, 'INSERT', @__AuthId, @__AuthIP;

		SELECT @GroupId;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END
GO

CREATE PROCEDURE __spGroupUpdate

	@GroupId INT,
	@Name NVARCHAR(32),
	@IsActive BIT,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	BEGIN TRANSACTION;

    BEGIN TRY

		UPDATE
			_Groups
		SET
			Name = @Name,
			IsActive = @IsActive
		WHERE
			GroupId = @GroupId;
		
		EXEC __spTransactionInsert '_Groups', @GroupId, 'UPDATE', @__AuthId, @__AuthIP;

		SELECT @GroupId;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END
GO

CREATE PROCEDURE __spGroupDelete

	@GroupId INT,
	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY

		DELETE FROM _Groups WHERE GroupId = @GroupId;

		EXEC __spTransactionInsert '_Groups', @GroupId, 'DELETE', @__AuthId, @__AuthIP;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END
GO

CREATE PROCEDURE __spReportUsers

	@From DATE,
	@To DATE
	 
AS

	SELECT
		U.Name,
		A.Email,
		U.CreatedOn
	FROM
		_Users U
	INNER JOIN
		_Auth A
	ON
		A.AuthId = U.AuthId
	WHERE
		U.CreatedOn BETWEEN @From AND @To
	AND
		U.IsActive = 1
	ORDER BY
		U.CreatedOn DESC;

GO