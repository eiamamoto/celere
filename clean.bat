﻿@echo off
rd Celere.PrivateApi\bin /S /Q
rd Celere.PrivateApi\obj /S /Q

rd Celere.PublicApi\bin /S /Q
rd Celere.PublicApi\obj /S /Q

rd Celere.Business\bin /S /Q
rd Celere.Business\obj /S /Q

rd Celere.ControlPanel\bin /S /Q
rd Celere.ControlPanel\obj /S /Q

rd Celere.Site\bin /S /Q
rd Celere.Site\obj /S /Q

echo Cleaned!