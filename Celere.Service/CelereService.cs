﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace CelereService
{
    public partial class CelereService : ServiceBase
    {
        private int eventId = 1;

        // This is a flag to indicate the service status
        private bool serviceStarted = false;
        private bool schedulerCreatorStarted = false;
        private bool processMonitorStarted = false;
        private bool notificatorStarted = false;

        // the thread that will do the work
        Thread workerThread;
        Thread schedulerCreatorThread;
        Thread processMonitorThread;
        Thread notificatorThread;

        public CelereService()
        {
            InitializeComponent();

            eventLog1 = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists("MySource"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "MySource", "MyNewLog");
            }
            eventLog1.Source = "MySource";
            eventLog1.Log = "MyNewLog";

            //System.Timers.Timer timer = new System.Timers.Timer();
            //timer.Interval = 60000; // 60 seconds  
            //timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            //timer.Start();




        }


        protected override void OnStart(string[] args)
        {
            eventLog1.WriteEntry("In OnStart with single thread.");
            // Create worker thread; this will invoke the WorkerFunction
            // when we start it.
            // Since we use a separate worker thread, the main service
            // thread will return quickly, telling Windows that service has started
            //ThreadStart st = new ThreadStart(WorkerFunction);
            ThreadStart stSchedulerCreator = new ThreadStart(SchedulerCreator);
            ThreadStart stProcessMonitor = new ThreadStart(ProcessMonitor);
            ThreadStart stNotificator = new ThreadStart(Notificator);

            //workerThread = new Thread(st);
            schedulerCreatorThread = new Thread(stSchedulerCreator);
            processMonitorThread = new Thread(stProcessMonitor);
            notificatorThread = new Thread(stNotificator);


            // set flag to indicate worker thread is active
            //serviceStarted = true;
            schedulerCreatorStarted = true;
            processMonitorStarted = true;
            notificatorStarted = true;

            // start the thread
            //workerThread.Start();
            schedulerCreatorThread.Start();
            processMonitorThread.Start();
            notificatorThread.Start();
        }

        protected override void OnStop()
        {
            eventLog1.WriteEntry("In onStop with single thread.");
            // flag to tell the worker process to stop
            //serviceStarted = false;
            schedulerCreatorStarted = false;
            processMonitorStarted = false;
            notificatorStarted = false;

            // give it a little time to finish any pending work
            //workerThread.Join(new TimeSpan(0, 2, 0));
            schedulerCreatorThread.Join(new TimeSpan(0, 2, 0));
            processMonitorThread.Join(new TimeSpan(0, 2, 0));
            notificatorThread.Join(new TimeSpan(0, 2, 0));
        }



        /// <summary>
        /// This function will do all the work
        /// Once it is done with its tasks, it will be suspended for some time;
        /// it will continue to repeat this until the service is stopped
        /// </summary>
        //private void WorkerFunction()
        //{
        //    // start an endless loop; loop will abort only when "serviceStarted" flag = false
        //    while (serviceStarted)
        //    {
        //        // do something
        //        // exception handling omitted here for simplicity
        //        eventLog1.WriteEntry("Service working", System.Diagnostics.EventLogEntryType.Information);

        //        // yield
        //        if (serviceStarted)
        //        {
        //            Thread.Sleep(new TimeSpan(0, 1, 0));
        //        }
        //    }

        //    // time to end the thread
        //    Thread.CurrentThread.Abort();
        //}



        private void SchedulerCreator()
        {
            // start an endless loop; loop will abort only when "serviceStarted" flag = false
            while (schedulerCreatorStarted)
            {
                // do something
                // exception handling omitted here for simplicity
                eventLog1.WriteEntry("SchedulerCreator working", System.Diagnostics.EventLogEntryType.Information);

                // yield
                if (schedulerCreatorStarted)
                {
                    Thread.Sleep(new TimeSpan(0, 4, 0));
                }
            }

            // time to end the thread
            Thread.CurrentThread.Abort();
        }


        private void ProcessMonitor()
        {
            Thread.Sleep(new TimeSpan(0, 2, 0));

            // start an endless loop; loop will abort only when "serviceStarted" flag = false
            while (processMonitorStarted)
            {
                // do something
                // exception handling omitted here for simplicity
                eventLog1.WriteEntry("ProcessMonitor working", System.Diagnostics.EventLogEntryType.Information);

                // yield
                if (processMonitorStarted)
                {
                    Thread.Sleep(new TimeSpan(0, 1, 0));
                }
            }

            // time to end the thread
            Thread.CurrentThread.Abort();
        }



        private void Notificator()
        {
            Thread.Sleep(new TimeSpan(0, 3, 0));

            // start an endless loop; loop will abort only when "serviceStarted" flag = false
            while (notificatorStarted)
            {
                // do something
                // exception handling omitted here for simplicity
                eventLog1.WriteEntry("Notificator working", System.Diagnostics.EventLogEntryType.Information);

                // yield
                if (notificatorStarted)
                {
                    Thread.Sleep(new TimeSpan(0, 5, 0));
                }
            }

            // time to end the thread
            Thread.CurrentThread.Abort();
        }




    }
}
