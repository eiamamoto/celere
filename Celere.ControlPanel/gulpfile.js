var gulp = require('gulp');
var less = require('gulp-less');
var concat = require('gulp-concat');
var clean = require('gulp-clean');
var uglify = require('gulp-uglify');
var gulpif = require('gulp-if');
var merge = require('merge-stream');
var argv = require('yargs').argv;
var cleanCSS = require('gulp-clean-css');
var ngAnnotate = require('gulp-ng-annotate');
var config = require('./config.json');
var jsonMerge = require('gulp-merge-json');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

var destination = './build';

// Clean
gulp.task('clean', function () {
	return gulp.src(destination, { read: false }).pipe(clean());
});

// Application
gulp.task('app', function () {
	return gulp
		.src(['modules/**/*.js'])
		.pipe(concat('app.js'))
		.pipe(ngAnnotate())
		.pipe(gulpif(argv.dist, uglify()))
		.pipe(gulp.dest(destination + '/js'));
});

// Javascript Libraries
gulp.task('javascript', function () {
	return gulp
		.src(config.javascript)
		.pipe(concat('scripts.js', { newLine: ';' }))
		.pipe(gulpif(argv.dist, uglify()))
		.pipe(gulp.dest(destination + '/js'));
});

// AngularJS Libraries
gulp.task('angularjs', function () {
	var angularjs = gulp
		.src(config.angularjs)
		.pipe(concat('angular.js', { newLine: ';' }))
		.pipe(gulpif(argv.dist, uglify()))
		.pipe(gulp.dest(destination + '/js'));

	// Additional files
	var lang = gulp.src(['node_modules/angular-auto-validate/dist/lang/*.json']).pipe(gulp.dest(destination + '/lang/angular-auto-validate'));

	return merge(angularjs, lang);
});

// CSS Libraries
gulp.task('css', function () {
	return gulp
		.src(config.css)
		.pipe(concat('libraries.css'))
		.pipe(gulpif(argv.dist, cleanCSS()))
		.pipe(gulp.dest(destination + '/css'));
});

// Languages
gulp.task('lang', function () {
	var ptBR = gulp
		.src('./modules/**/pt-BR.json')
		.pipe(jsonMerge({ fileName: 'pt-BR.json' }))
		.pipe(gulp.dest(destination + '/lang'));

	var en = gulp
		.src('./modules/**/en.json')
		.pipe(jsonMerge({ fileName: 'en.json' }))
		.pipe(gulp.dest(destination + '/lang'));

	return merge(ptBR, en);
});

// LESS Files
gulp.task('less', function () {
	return gulp
		.src('./less/default.less')
		.pipe(less())
		.pipe(concat('style.css'))
		.pipe(gulpif(argv.dist, cleanCSS()))
		.pipe(gulp.dest(destination + '/css'));
});

// Assets
gulp.task('assets', function () {
	var images = gulp.src('./images/**/*').pipe(gulp.dest(destination + '/images'));

	var favicons = gulp.src('./favicons/**/*').pipe(gulp.dest(destination + '/favicons'));

	return merge(images, favicons);
});

// Fonts
gulp.task('fonts', function () {
	var fontawesome = gulp.src('./node_modules/@fortawesome/fontawesome-pro-webfonts/webfonts/*').pipe(gulp.dest(destination + '/webfonts'));

	var summernote = gulp.src('./node_modules/summernote/dist/font/*').pipe(gulp.dest(destination + '/css/font'));

	return merge(fontawesome, summernote);
});

// Default Build
gulp.task('build', ['javascript', 'angularjs', 'css', 'app', 'less', 'lang', 'assets', 'fonts']);

// Watch
gulp.task('default', ['build'], function () {
	gulp.watch(['./modules/**/*.js'], ['app']);
	gulp.watch(['./less/**/*.less'], ['less']);
	gulp.watch(['./modules/**/*.json'], ['lang']);
});

// Browsersync
gulp.task('serve', ['default'], function () {
	browserSync.init({
		server: {
			baseDir: './',
		},
	});

	gulp.watch(['./*.html', './modules/**/*.*', './less/**/*.less', './modules/**/*.json']).on('change', reload);
});
