﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).config(function ($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/signin');

	$stateProvider.state('main', {
		url: '/',
		views: {
			'': {
				templateUrl: '/modules/core/views/layout.html',
				controller: 'controlPanelCtrl',
				controllerAs: 'vm'
			}
		}
	});
});