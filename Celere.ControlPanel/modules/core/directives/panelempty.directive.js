﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).directive("panelEmpty", function () {
	return {
		restrict: "E",
		template: "<div class='panel-empty'><p translate='global.emptyResult'></p></div>"
	};
});