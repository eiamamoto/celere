﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).directive('keywordFilter', function () {
	return {
		restrict: 'E',
		scope: {
			keyword: '=',
			help: '=?'
		},
		templateUrl: '/modules/core/views/keywordfilter.html'
	};
});