﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).directive('tagInput', [function () {
	return {
		restrict: 'A',
		scope: {
			tagField: '=',
			fieldLabel: '=',
			isRequired: '='
		},
		templateUrl: '/modules/core/views/taginput.html',
		link: function (scope, element, attrs) {

			scope.tagArray = null;

			scope.setTagField = function () {
				scope.tagField = null;

				if (scope.tagArray && scope.tagArray.length) {
					scope.tagField = scope.tagArray.map(function (item) { return item.text; }).join('|');
				}
			}

			if (scope.tagField && scope.tagField.length) {
				scope.tagArray = scope.tagField.split('|').map(function (item) { return { text: item } });
			}
		}
	};
}]);