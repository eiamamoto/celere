﻿'use strict';

// Fonte: https://github.com/terryh/angular-pickadate-directive
angular.module(ApplicationConfiguration.applicationModuleName).directive('pickAdate', ['$parse', function ($parse) {
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function (scope, element, attrs, ngModel) {
			element.pickadate({
				format: 'dd/mm/yyyy',
				selectMonths: true,
				selectYears: 100,
				onOpen: function () {
					this.set('select', ngModel.$modelValue);
				},
				onSet: function (e) {
					//console.log(e);
					ngModel.$modelValue = this.get();
				}
			});
		}
	};
}]).
directive('pickAtime', ['$parse', function ($parse) {
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function (scope, element, attrs, ngModel) {
			element.pickatime({
				format: 'HH:i',
				formatSubmit: 'HH:i',
				onOpen: function () {
					this.set('select', ngModel.$modelValue);
				},
				onSet: function (e) {
					//console.log(e);
					ngModel.$modelValue = this.get();
				}
			});
		}
	};
}]);