﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).directive('dateTime', [function () {
	return {
		restrict: 'A',
		scope: {
			dateField: '=',
			isRequired: '='
		},
		templateUrl: '/modules/core/views/datetime.html',
		link: function (scope, element, attrs) {

			scope.valueAsDate = null;
			scope.valueAsTime = null;

			scope.setFormFields = function () {
				if (scope.dateField) {
					scope.valueAsDate = moment(scope.dateField).format("DD/MM/YYYY");
					scope.valueAsTime = moment(scope.dateField).format("HH:mm");
				}

				if (!scope.valueAsDate || (scope.valueAsTime && scope.valueAsTime == "Invalid Date")) {
					scope.valueAsTime = null;
				}
			}

			scope.setDateField = function () {

				if (scope.valueAsDate) {

					scope.valueAsDate = moment(moment(scope.valueAsDate, 'DD/MM/YYYY').toDate()).format("YYYY-MM-DD");

					if (scope.valueAsTime && scope.valueAsTime != "Invalid Date") {
						scope.dateField = new Date(scope.valueAsDate + ' ' + scope.valueAsTime);
					}
					else {
						scope.dateField = new Date(scope.valueAsDate);
					}
				}
				else {
					scope.dateField = null;
				}

				scope.setFormFields();
			}

			scope.setFormFields();
		}
	};
}]);