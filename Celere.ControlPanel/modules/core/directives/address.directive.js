﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).directive('addressForm', ['LocationService', 'MessageService', function (LocationService, MessageService) {
	return {
		restrict: 'A',
		scope: {
			address: '=',
			isRequired: '='
		},
		templateUrl: '/modules/core/views/address.html',
		link: function (scope, element, attrs) {
			scope.states = [];
			scope.cities = [];
			scope.formerZipCode = null;
			if (scope.address) {
				scope.formerZipCode = scope.address.zipCode;
			}

			scope.zipCodeChange = function () {

				if (scope.address.zipCode && scope.address.zipCode != scope.formerZipCode) {

					LocationService.getZipCodeLocation(scope.address.zipCode).then(function (response) {

						if (response.data) {
							scope.formerZipCode = scope.address.zipCode;

							if (response.data.stateId && scope.address.stateId != response.data.stateId) {
								scope.address.stateId = response.data.stateId;

								scope.cities = [];
								scope.getCities(function () {
									scope.address.cityId = response.data.cityId;
								});
							}

							scope.address.address1 = response.data.address;
							scope.address.address2 = null;
							scope.address.neighborhood = response.data.neighborhood;
						}

					}).catch(MessageService.error);

				}
			}

			LocationService.getStates().then(function (response) {
				scope.states = response.data;
			}).catch(MessageService.error);

			scope.getCities = function (callback) {
				LocationService.getCities(scope.address.stateId).then(function (response) {
					scope.cities = response.data;

					if (callback) {
						callback();
					}
				}).catch(MessageService.error);
			}

			scope.onStateChange = function () {
				scope.cities = [];

				if (scope.address.stateId) {
					scope.getCities();
				}
			}

			if (scope.address && scope.address.stateId) {
				scope.getCities();
			}
		}
	};
}]);