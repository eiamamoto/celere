﻿/**!
 * AngularJS Image Crop directive
 * @author Flávio Banyai <flavio.banyai@asteria.com.br>
 */

'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).directive('imagecrop', ['apiSettings', 'dataURItoBlob', '$localStorage', function (apiSettings, dataURItoBlob, $localStorage) {
	return {
		restrict: 'E',
		scope: {
			url: '@',
			areaType: '@',
			aspectRatio: '@',
			width: '@',
			height: '@',
			targetWidth: '@',
			targetHeight: '@',
			image: '='
		},
		templateUrl: '/modules/core/views/imagecrop.html',
		link: function (scope, element, attrs) {
			scope.uploader = new Flow({
				singleFile: true,
				testChunks: false,
				target: apiSettings.baseUri + scope.url + '?isFlow=true',
				headers: {
					'Authorization': 'Bearer ' + $localStorage.authorizationData.token
				}
			});

			// Default settings
			if (scope.width && scope.height) {
				scope.imageSize = {
					w: scope.width,
					h: scope.height
				}
			};

			if (scope.targetWidth && scope.targetHeight) {
				scope.resultImageSize = {
					w: scope.targetWidth,
					h: scope.targetHeight
				}
			};

			// Upload temporário para memória do navegador
			scope.temporaryFileUpload = function ($file, $event, $flow) {
				$event.preventDefault();

				var reader = new FileReader();

				reader.onload = function (evt) {
					scope.$apply(function () {
						scope.sourceCropImage = evt.target.result;
					});
				};

				scope.filename = $file.file.name;

				reader.readAsDataURL($file.file);
			};

			// Upload real após o crop
			scope.croppedImageUpload = function () {
				scope.cropping = true;

				var blob = dataURItoBlob.convert(scope.targetCropImage);

				blob.name = scope.filename;

				scope.uploader.files[0] = new Flow.FlowFile(scope.uploader, blob);
				
				scope.uploader.upload();
			}

			// Botão Cancelar
			scope.cancelCrop = function () {
				scope.cropping = false;

				delete scope.sourceCropImage;
			}

			// Evento chamado em caso de sucesso no upload
			scope.uploader.on("fileSuccess", function (file, message, chunk) {
				scope.cropping = false;

				delete scope.sourceCropImage;

				scope.image = JSON.parse(message);
				// scope.imageFile = scope.image.split('/').pop();

				scope.$apply();
			});

			// Evento chamado em caso de falha no upload
			scope.uploader.on("error", function (message, file, chunk) {
				scope.cropping = false;

				scope.$parent.alertError(message);

				scope.$apply();
			});
		}
	}
}]);