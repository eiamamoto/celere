'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('sidebarCtrl', function($scope, menuSettings, settings, permissionService) {
	var vm = this;

	vm.permissions = permissionService;

	vm.menu = menuSettings.items;

	vm.toggleSubmenu = function(item) {
		item.toggle = !item.toggle;
	};
});