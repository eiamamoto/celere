﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('controlPanelCtrl', function ($scope, $filter, $window, $state, $localStorage, settings, permissionService) {
	var vm = this;

	var $translate = $filter('translate');

	vm.permissions = permissionService;

	// Server settings
	$scope.globalConfig = $window.config;

	// Menu size
	$scope.resize = function () {
		var h = $window.innerHeight;
		var t1 = $(".sidebar .user").outerHeight(true);
		var t2 = $(".sidebar .signature").outerHeight(true);

		$(".menu").height(h - t1 - t2);
	};

	$scope.resize();

	angular.element($window).bind('resize', $scope.resize);

	// Settings
	$scope.settings = settings || {};

	// Methods
	vm.toggleMenu = function () {
		vm.menuOpened = !vm.menuOpened;
	}

	vm.signout = function () {
		$state.go('signin');
	}

	vm.insertNew = function (url, enumContentArea, routeData) {
		angular.extend({}, routeData);
		//console.log('ddddd');
		if (enumContentArea) {
			angular.extend(routeData, { enumContentArea: enumContentArea });
		}

		$state.go(url, routeData);
	}
});