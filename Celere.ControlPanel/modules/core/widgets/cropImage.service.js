﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).service('cropImage', function () {
	var self = {};

	self.image = '';
	
	self.setImage = function (value) {
		self.image = value;
	}

	return self;
});