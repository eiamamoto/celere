﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).service('MessageService', ['$ngConfirm', '$rootScope', function ($ngConfirm, $rootScope) {
	var self = this;

	self.error = function (err) {
		console.log(err);
		self.alertError(JSON.parse(err._body));
	}

	// Alert windows
	self.alertInfo = function (message, title) {
		$ngConfirm({
			title: title || 'Aviso importante',
			icon: 'glyphicon glyphicon-info-sign',
			content: message,
			theme: 'supervan',
			type: 'blue',
			autoClose: 'ok|5000',
			buttons: {
				ok: {
					keys: ['enter'],
					action: function (scope) {
						if (scope.laddaLoading) {
							scope.laddaLoading = false;

							scope.$apply();
						}
					}
				}
			},
		});
	}

	self.alertWarning = function (message, title) {
		$ngConfirm({
			title: title || 'Aviso importante',
			icon: 'glyphicon glyphicon-exclamation-sign',
			content: message,
			theme: 'supervan',
			type: 'dark',
			buttons: {
				ok: {
					keys: ['enter'],
					action: function (scope) {
						if (scope.laddaLoading) {
							scope.laddaLoading = false;

							scope.$apply();
						}
					}
				}
			},
		});
	}

	self.alertError = function (message, title) {
		$ngConfirm({
			title: title || 'Ocorreu um erro',
			icon: 'glyphicon glyphicon-minus-sign',
			content: message,
			theme: 'supervan',
			type: 'red',
			scope: $rootScope,
			buttons: {
				ok: {
					keys: ['enter'],
					action: function (scope) {
						if (scope.laddaLoading) {
							scope.laddaLoading = false;

							scope.$apply();
						}
					}
				}
			},
		});
	}

	self.confirmDelete = function (callback) {
		$ngConfirm({
			title: 'Exclusão de Registro',
			content: 'Você tem certeza que deseja excluir este item?',
			icon: 'glyphicon glyphicon-trash',
			theme: 'supervan',
			type: 'red',
			buttons: {
				yes: {
					text: 'Sim',
					action: callback
				},
				no: {
					text: 'Não'
				}
			}
		});
	}

	return self;
}]);