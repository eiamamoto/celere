'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).service('pageService', function () {
  var self = {};

  self.update = function (dest, src) {
    var diff = Math.abs(dest.length - src.length);
    if (dest.length > src.length) {
      dest.splice(-1, diff);
    }
    else if (dest.length < src.length) {
      for (var i = 0; i < diff; i++) {
        dest.push({});
      }
    }
    for (var i = 0, _len = src.length; i < _len; i++) {
      angular.extend(dest[i], src[i]);
    }
  };

  return self;
});