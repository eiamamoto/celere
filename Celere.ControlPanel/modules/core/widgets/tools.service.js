﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).service('tools', [function () {
	var self = {};
	
	self.getFromArray = function (arr, propName, propValue) {
		var arrLength = arr.length;

		if (arrLength) {
			for (var i = 0; i < arrLength; i++)
				if (arr[i][propName] == propValue)
					return arr[i];
		}

		// will return undefined if not found; you could return a default instead
	}

	return self;
}]);