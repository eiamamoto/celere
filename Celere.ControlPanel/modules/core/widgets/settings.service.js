﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).service('settings', ['$localStorage', function ($localStorage) {
	var self = {};

	// Properties
	self.email = $localStorage.email;
	// self.internalUserName = $localStorage.internalUserName;
	self.authId = $localStorage.authId;
	// self.avatar = $localStorage.avatar;
	self.permissions = $localStorage.permissions;

	// Methods
	self.setInternalUser = function (auth) {
		self.email = $localStorage.email = auth.email;
		// self.internalUserName = $localStorage.internalUserName = internalUser.displayName;
		self.authId = $localStorage.authId = auth.authId;
		// self.avatar = $localStorage.avatar = auth.imageAvatar;
		self.permissions = $localStorage.permissions = auth.permissions;
	}

	return self;
}]);