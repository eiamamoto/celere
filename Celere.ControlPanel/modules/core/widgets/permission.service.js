﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).service('permissionService', ['settings', function (settings) {
	var self = {};

	self.isAdmin = function () {
		return (settings.staffId != null && settings.staffId != 0);
	}

	self.isAuthorized = function (task) {
		if (!task) {
			return self.isAdmin();
		}

		return (
			self.isAdmin()
			|| (settings.permissions && settings.permissions.indexOf(task) != -1)
		);
	}



	return self;
}]);