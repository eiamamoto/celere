'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).directive('kpiHBarChart', function() {
	return {
		restrict: 'E',
		scope: {
			name: '=',
			dataset: '=',
		},
		templateUrl: '/modules/core/widgets/kpi/hbarChart/hbarchart.html',
		link: function(scope, element, attrs) {
			scope.options = {
				maintainAspectRatio: false,
				legend: {
					position: 'bottom',
				},
			};
		},
	};
});
