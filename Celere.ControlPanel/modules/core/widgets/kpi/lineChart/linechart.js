'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).directive('kpiLineChart', function() {
	return {
		restrict: 'E',
		scope: {
			name: '=',
			dataset: '=',
		},
		templateUrl: '/modules/core/widgets/kpi/lineChart/linechart.html',
		link: function(scope, element, attrs) {
			scope.options = {
				maintainAspectRatio: false,
				legend: {
					position: 'bottom',
				},
				scales: {
					xAxes: [
						{
							gridLines: {
								zeroLineColor: 'transparent',
							},
							ticks: {
								padding: 20,
								fontColor: 'rgba(0,0,0,0.5)',
								fontStyle: 'bold',
							},
						},
					],
					yAxes: [
						{
							ticks: {
								fontColor: 'rgba(0,0,0,0.5)',
								fontStyle: 'bold',
								beginAtZero: true,
								maxTicksLimit: 5,
								padding: 20,
							},
							gridLines: {
								drawTicks: false,
								display: false,
							},
						},
					],
				},
			};
		},
	};
});
