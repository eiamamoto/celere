'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).directive('kpiDoughnutChart', function() {
	return {
		restrict: 'E',
		scope: {
			name: '=',
			dataset: '=',
		},
		templateUrl: '/modules/core/widgets/kpi/doughnutChart/doughnutchart.html',
		link: function(scope, element, attrs) {
			scope.options = {
				maintainAspectRatio: false,
				legend: {
					position: 'bottom',
				},
			};
		},
	};
});
