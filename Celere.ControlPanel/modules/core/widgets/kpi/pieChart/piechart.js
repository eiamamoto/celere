'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).directive('kpiPieChart', function() {
	return {
		restrict: 'E',
		scope: {
			name: '=',
			dataset: '=',
		},
		templateUrl: '/modules/core/widgets/kpi/pieChart/piechart.html',
		link: function(scope, element, attrs) {
			scope.options = {
				maintainAspectRatio: false,
				legend: {
					position: 'bottom',
				},
			};
		},
	};
});
