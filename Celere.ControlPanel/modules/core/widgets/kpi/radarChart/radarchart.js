'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).directive('kpiRadarChart', function() {
	return {
		restrict: 'E',
		scope: {
			name: '=',
			dataset: '=',
		},
		templateUrl: '/modules/core/widgets/kpi/radarChart/radarchart.html',
		link: function(scope, element, attrs) {
			scope.options = {
				maintainAspectRatio: false,
			};
		},
	};
});
