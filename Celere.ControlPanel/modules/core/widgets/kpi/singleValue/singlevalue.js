'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).directive('kpiSingleValue', function() {
	return {
		restrict: 'E',
		scope: {
			icon: '=',
			name: '=',
			value: '=',
		},
		templateUrl: '/modules/core/widgets/kpi/singleValue/singlevalue.html',
	};
});
