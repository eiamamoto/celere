'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).directive('kpiPolarAreaChart', function() {
	return {
		restrict: 'E',
		scope: {
			name: '=',
			dataset: '=',
		},
		templateUrl: '/modules/core/widgets/kpi/polarAreaChart/polarareachart.html',
		link: function(scope, element, attrs) {
			scope.options = {
				maintainAspectRatio: false
			};
		},
	};
});
