'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).directive('kpiMultiValues', function() {
	return {
		restrict: 'E',
		scope: {
			name: '=',
			dataset: '=',
		},
		templateUrl: '/modules/core/widgets/kpi/multiValues/multivalues.html'
	};
});
