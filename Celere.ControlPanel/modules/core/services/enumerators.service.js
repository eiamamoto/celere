'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).factory('Enumerators', ['$resource', '$stateParams', 'apiSettings', function ($resource, $stateParams, apiSettings) {
	return $resource(apiSettings.baseUri + 'v1/enumerators/:name', { name: '@name' });
}]);