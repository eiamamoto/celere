﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).factory('Dashboard', ['$resource', 'apiSettings', function ($resource, apiSettings) {
	return $resource(null, null, {
		dashboard: {
			url: apiSettings.baseUri + 'v1/dashboard',
			method: 'GET',
			isArray: false
		}
	});
}]);