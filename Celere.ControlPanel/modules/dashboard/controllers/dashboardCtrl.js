﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('dashboardCtrl', function($scope, $filter, model, Dashboard, permissionService) {
	var vm = this;

	var $translate = $filter('translate');

	vm.permissions = permissionService;

	angular.extend($scope.$parent.vm, {
		title: 'dashboard.settings.title',
		insertUrl: null,
	});

	vm.model = model;

	/* As variáveis abaixo são apenas para referência e devem ser substituidas ou removidas nos projetos */

	vm.singlevalue1 = {
		title: 'Total Users',
		icon: 'users',
		value: '100',
	};

	vm.multivalues = [
		{ label: 'Valor 1', data: 100, icon: 'users' },
		{ label: 'Valor 2', data: 200, icon: 'dollar-sign' },
		{ label: 'Valor 3', data: 300, icon: 'calendar-alt' },
	];

	vm.simpledata = {
		labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May'],
		data: [65, 59, 80, 100, 110],
	};

	vm.dataset = {
		series: ['Series A', 'Series B', 'Series C'],
		labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul'],
		data: [[65, 59, 80, 81, 56, 55, 40], [28, 48, 40, 19, 86, 27, 90], [18, 48, 77, 9, 100, 27, 40]],
	};
});
