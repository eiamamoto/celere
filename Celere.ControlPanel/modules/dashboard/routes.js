﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider.state('main.dashboard', {
		url: 'dashboard',
		resolve: {
			model: ['Dashboard', function (Dashboard) {
				return Dashboard.dashboard().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/dashboard/views/dashboard.html',
				controller: 'dashboardCtrl',
				controllerAs: 'vm'
			}
		}
	});
});