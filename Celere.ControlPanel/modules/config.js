﻿'use strict';

// Init the application configuration module for AngularJS application
var ApplicationConfiguration = (function () {
	// Init module configuration options
	var applicationModuleName = 'controlPanelApp';

	var applicationModuleVendorDependencies = [
		'ngAnimate',
		'ngResource',
		'ngSanitize',
		'jcs-autoValidate',
		'ngStorage',
		'ui.router',
		'ui.bootstrap',
		'ngTouch',
		'summernote',
		'angular-loading-bar',
		'flow',
		'ui.utils.masks',
		'uiCropper',
		'cp.ngConfirm',
		'angular-ladda',
		'ngTagsInput',
		'idf.br-filters',
		'ngRateIt',
		'chart.js',
		'pascalprecht.translate',
		'ngFileSaver'
	]

	// Add a new vertical module
	var registerModule = function (moduleName, dependencies) {
		// Create angular module
		angular.module(moduleName, dependencies || []);

		// Add the module to the AngularJS configuration file
		angular.module(applicationModuleName).requires.push(moduleName);
	};

	return {
		applicationModuleName: applicationModuleName,
		applicationModuleVendorDependencies: applicationModuleVendorDependencies,
		registerModule: registerModule
	};
})();