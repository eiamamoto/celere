'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).factory('Reports', ['$resource', '$stateParams', 'apiSettings', function ($resource, $stateParams, apiSettings) {
	return $resource(null, null, {
		'users': {
			url: apiSettings.baseUri + 'v1/reports/users',
			method: 'GET',
			isArray: true
		},
		'usersExcel': {
			url: apiSettings.baseUri + 'v1/reports/users/excel',
			method: 'GET',
			headers: {
				'Accept': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
				'Content-type': 'application/json'
			},
			responseType: 'arraybuffer',
			transformResponse: function (data) {
				var xlsx;

				if (data) {
					xlsx = new Blob([data], {
						type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
					});
				}

				return {
					data: xlsx
				};
			}
		}
	});
}]);