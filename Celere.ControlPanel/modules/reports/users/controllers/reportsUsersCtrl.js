﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('reportsUsersCtrl', function ($scope, $rootScope, $state, $http, $filter, settings, apiSettings, MessageService, Reports, FileSaver) {
	var vm = this;

	var $translate = $filter('translate');

	angular.extend($scope.$parent.vm, {
		title: 'reports.users.title',
		insertUrl: null
	});

	vm.model = [];

	vm.refresh = function () {
		var params = {
			from: vm.from,
			to: vm.to
		};

		Reports.users(params).$promise.then(function (response) {
			vm.model = response;
		});
	}

	vm.excel = function () {
		var params = {
			from: vm.from,
			to: vm.to
		};

		Reports.usersExcel(params, function (response) {
			var filename = moment().format('YYYYMMDDHHmmss')  + '.xlsx';

			FileSaver.saveAs(response.data, filename);
		}, function (response) {
			var msg = $translate('errors.couldNotCompleteRequest');

			MessageService.alertError(msg);

			console.log(response);
		});
	}
});