'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider.state('main.reports', {
		url: 'users',
		views: {
			'main': {
				templateUrl: '/modules/reports/users/views/reports.users.html',
				controller: 'reportsUsersCtrl',
				controllerAs: 'vm'
			}
		}
	});
});