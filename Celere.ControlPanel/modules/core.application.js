﻿'use strict';

Date.prototype.toJSON = function () { return moment(this).toISOString(); }

angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// API Settings
angular.module(ApplicationConfiguration.applicationModuleName).constant('apiSettings', {
	baseUri: window.config.apiUrl,
	defaultPageSize: 10
});

angular.module(ApplicationConfiguration.applicationModuleName).config(['$httpProvider', function ($httpProvider) {
	// Date format
	var regexIso8601 = /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z)/;

	var convertDateStringsToDates = function (input) {
		// Ignore things that aren't objects.
		if (typeof input !== "object") return input;

		for (var key in input) {
			if (!input.hasOwnProperty(key)) continue;

			var value = input[key];
			var match;

			// Check for string properties which look like dates.
			if (typeof value === "string" && (match = value.match(regexIso8601))) {
				var milliseconds = Date.parse(match[0]);

				if (!isNaN(milliseconds)) {
					input[key] = moment.utc(milliseconds).toDate();
				}
			} else if (typeof value === "object") {
				// Recurse into object
				convertDateStringsToDates(value);
			}
		}
	};

	$httpProvider.defaults.transformResponse.push(function (response) {
		convertDateStringsToDates(response);

		return response;
	});

	$httpProvider.interceptors.push('authInterceptorService');
}]);

// Translation
angular.module(ApplicationConfiguration.applicationModuleName).config(['$translateProvider', function ($translateProvider) {
	$translateProvider.useStaticFilesLoader({
		prefix: '/build/lang/',
		suffix: '.json'
	});

	$translateProvider.preferredLanguage('pt-BR');

	$translateProvider.useSanitizeValueStrategy('escape');
}]);

// Flow
angular.module(ApplicationConfiguration.applicationModuleName).config(['flowFactoryProvider', function (flowFactoryProvider) {
	flowFactoryProvider.defaults = {
		target: '/upload',
		permanentErrors: [404, 500, 501],
		minFileSize: 0
	};
}]);

// Chart.js
angular.module(ApplicationConfiguration.applicationModuleName).config(['ChartJsProvider', function (ChartJsProvider) {
	ChartJsProvider.setOptions({
		chartColors: [
			{
				backgroundColor: '#FF6D00',
				borderColor: '#FF6D00',
				pointBackgroundColor: '#FF6D00',
				pointBorderColor: '#FF6D00',
				pointBorderWidth: 4,
				pointRadius: 4,
				borderWidth: 0,
				fill: false,
			},
			{
				backgroundColor: '#FF9100',
				borderColor: '#FF9100',
				pointBackgroundColor: '#FF9100',
				pointBorderColor: '#FF9100',
				pointBorderWidth: 4,
				pointRadius: 4,
				borderWidth: 0,
				fill: false,
			},
			{
				backgroundColor: '#FFAB40',
				borderColor: '#FFAB40',
				pointBackgroundColor: '#FFAB40',
				pointBorderColor: '#FFAB40',
				pointBorderWidth: 4,
				pointRadius: 4,
				borderWidth: 0,
				fill: false,
			},
			{
				backgroundColor: '#FFD180',
				borderColor: '#FFD180',
				pointBackgroundColor: '#FFD180',
				pointBorderColor: '#FFD180',
				pointBorderWidth: 4,
				pointRadius: 4,
				borderWidth: 0,
				fill: false,
			},
			{
				backgroundColor: '#E65100',
				borderColor: '#E65100',
				pointBackgroundColor: '#E65100',
				pointBorderColor: '#E65100',
				pointBorderWidth: 4,
				pointRadius: 4,
				borderWidth: 0,
				fill: false,
			},
			{
				backgroundColor: '#F57C00',
				borderColor: '#F57C00',
				pointBackgroundColor: '#F57C00',
				pointBorderColor: '#F57C00',
				pointBorderWidth: 4,
				pointRadius: 4,
				borderWidth: 0,
				fill: false,
			},
		],
		responsive: true
	 });

	 ChartJsProvider.setOptions('line', {
		borderWidth: 1,
		radius: 0,
	 });
}]);

// Ladda
angular.module(ApplicationConfiguration.applicationModuleName).config(['laddaProvider', function (laddaProvider) {
	laddaProvider.setOption({
		style: 'expand-left',
		spinnerSize: 35,
		spinnerColor: '#ffffff'
	});
}]);

// Custom Error Messages
angular.module(ApplicationConfiguration.applicationModuleName).run(['defaultErrorMessageResolver', function (defaultErrorMessageResolver) {
	defaultErrorMessageResolver.setI18nFileRootPath('/build/lang/angular-auto-validate');

	defaultErrorMessageResolver.setCulture('pt-BR');

	defaultErrorMessageResolver.getErrorMessages().then(function (errorMessages) {
		errorMessages['required'] = 'Campo obrigatório';
		errorMessages['brPhoneNumber'] = 'Telefone inválido';
		errorMessages['cep'] = 'CEP inválido';
	});
}]);

// Global Error Handling
angular.module(ApplicationConfiguration.applicationModuleName).run(['$rootScope', '$log', '$ngConfirm', function ($rootScope, $log, $ngConfirm) {
	$rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
		$log.warn(error);

		// VERIFICAR SE A CHAMADA DO MessageService FUNCIONARÁ - OU SE PRECISA MANTER O TRECHO QUE CARREGA O $ngConfirm

		MessageService.alertError(error);

		// $ngConfirm({
		// 	title: 'Ocorreu um erro...',
		// 	icon: 'glyphicon glyphicon-exclamation-sign',
		// 	content: error.data,
		// 	theme: 'supervan',
		// 	type: 'red',
		// 	scope: $rootScope,
		// 	buttons: {
		// 		ok: {
		// 			keys: ['enter']
		// 		}
		// 	},
		// });
	});
}]);

// ExceptionHandler Circular Dependency
// Info: https://stackoverflow.com/questions/22332130/injecting-http-into-angular-factoryexceptionhandler-results-in-a-circular-de
angular.module(ApplicationConfiguration.applicationModuleName).factory('$exceptionHandler', function ($injector) {
	return function (exception, cause) {
		var $log = $injector.get("$log");
		var MessageService = $injector.get("MessageService");

		$log.warn(exception, cause);

		MessageService.alertError(exception);
	};
});