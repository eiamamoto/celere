﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).factory('Auth', ['$resource', 'apiSettings', function ($resource, apiSettings) {
	return $resource(null, { id: '@id' }, {
		signin: {
			url: apiSettings.baseUri + 'v1/auth/signin',
			method: 'POST'
		},
		validateToken: {
			url: apiSettings.baseUri + 'v1/auth/validate-password-reset-token',
			method: 'POST'
		},
		forgotPassword: {
			url: apiSettings.baseUri + 'v1/auth/forgot-password',
			method: 'POST'
		},
		passwordReset: {
			url: apiSettings.baseUri + 'v1/auth/password-reset',
			method: 'POST'
		}
	});
}]);