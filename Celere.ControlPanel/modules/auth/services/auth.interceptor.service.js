﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).factory('authInterceptorService', ['$q', '$location', '$localStorage', function ($q, $location, $localStorage) {
	var authInterceptorServiceFactory = {};

	var _request = function (config) {
		config.headers = config.headers || {};

		var authData = $localStorage.authorizationData;

		if (authData) {
			config.headers.Authorization = 'Bearer ' + authData.token;
		}

		return config;
	}

	authInterceptorServiceFactory.request = _request;

	return authInterceptorServiceFactory;
}]);