﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).config(function ($stateProvider, $urlRouterProvider) {
	// Sign In
	$stateProvider.state('signin', {
		url: '/signin',
		views: {
			'': {
				templateUrl: '/modules/auth/views/auth.html'
			},
			'auth@signin': {
				templateUrl: '/modules/auth/views/signin.html',
				controller: 'signinCtrl',
				controllerAs: 'vm'
			}
		}
	});

	// Forgot Password
	$stateProvider.state('forgotPassword', {
		url: '/password',
		views: {
			'': {
				templateUrl: '/modules/auth/views/auth.html'
			},
			'auth@forgotPassword': {
				templateUrl: '/modules/auth/views/password.forgot.html',
				controller: 'forgotPasswordCtrl',
				controllerAs: 'vm'
			}
		}
	});

	// Password Reset
	$stateProvider.state('passwordReset', {
		url: '/passwordreset?email&token',
		views: {
			'': {
				templateUrl: '/modules/auth/views/auth.html'
			},
			'auth@passwordReset': {
				templateUrl: '/modules/auth/views/password.reset.html',
				controller: 'passwordResetCtrl',
				controllerAs: 'vm'
			}
		}
	});
});