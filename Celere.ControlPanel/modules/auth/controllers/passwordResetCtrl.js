﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('passwordResetCtrl', function ($scope, $rootScope, $state, $stateParams, $http, $filter, $localStorage, Auth, MessageService, settings, apiSettings) {
	var vm = this;

	var $translate = $filter('translate');

	$rootScope.laddaLoading = false;

	if (!$stateParams.email || !$stateParams.token) {
		$state.go('signin');
	}

	Auth.validateToken({
		email: $stateParams.email,
		token: $stateParams.token
	}).$promise.then(null, function (response) {
		MessageService.alertError(response.data);

		$state.go('signin');
	});

	vm.model = {
		email: $stateParams.email,
		token: $stateParams.token,
	};

	vm.changePassword = function () {
		if (vm.model.password != vm.model.passwordConfirm) {
			vm.auth_error = 'Senhas não conferem!';
			return;
		}

		$rootScope.laddaLoading = true;

		Auth.passwordReset({
			email: vm.model.email,
			newPassword: vm.model.password,
			passwordConfirmation: vm.model.passwordConfirm,
			token: vm.model.token
		}, function (response) {
			MessageService.alertInfo("Senha alterada com sucesso!", "Redefinir senha");

			$state.go('signin');
		}, function (response) {
			vm.auth_error = response.data;

			$rootScope.laddaLoading = false;
		});

	};
});