﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('signinCtrl', function($scope, $filter, $rootScope, $state, $http, $localStorage, Auth, settings, apiSettings, permissionService) {
	var vm = this;

	var $translate = $filter('translate');

	// Disable the loader
	$rootScope.laddaLoading = false;

	// Reset all the stored info
	$localStorage.$reset();

	vm.submit = function() {
		vm.auth_error = null;

		$rootScope.laddaLoading = true;

		Auth.signin({
			email: vm.model.email,
			password: vm.model.password,
		}).$promise.then(
			function(response) {
				// Store the user info
				$localStorage.authorizationData = response;

				// Fill the object with the current user
				settings.setInternalUser(response.auth);

				$state.go('main.dashboard');
			},
			function(response) {
				vm.auth_error = response.data || 'Email e/ou senha inválidos!';

				$rootScope.laddaLoading = false;
			}
		);
	};
});
