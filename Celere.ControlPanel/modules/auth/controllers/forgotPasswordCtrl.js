﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('forgotPasswordCtrl', function ($scope, $rootScope, $state, $stateParams, $http, $filter, $localStorage, Auth, MessageService, settings, apiSettings) {
	var vm = this;

	var $translate = $filter('translate');

	$rootScope.laddaLoading = false;

	vm.sendEmail = function () {
		if (!vm.model || !vm.model.email) {
			vm.auth_error = 'Preencha o email para solicitar a recuperação de senha!';
			return;
		}

		$rootScope.laddaLoading = true;

		$http.post(apiSettings.baseUri + 'v1/auth/forgot-password', {
			email: vm.model.email
		}).then(
			function (response) {
				MessageService.alertInfo(response.data);

				$state.go("signin");
			},
			function (response) {
				vm.auth_error = response.data;

				$rootScope.laddaLoading = false;
			}
		);
	};
});