﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).constant('menuSettings', {
	items: [
		{
			name: 'global.menu.main',
			icon: 'star_rate',
			task: 'ControlPanelAccess',
			submenu: [
				{ name: 'addresses.settings.title', path: 'main.address', task: 'ManageAddresses' },
				{ name: 'attempts.settings.title', path: 'main.attempt', task: 'ManageAttempts' },
				{ name: 'cities.settings.title', path: 'main.city', task: 'ManageCities' },
				{ name: 'customers.settings.title', path: 'main.customer', task: 'ManageCustomers' },
				{ name: 'logTrackingCodes.settings.title', path: 'main.logtrackingcode', task: 'ManageLogTrackingCodes' },
				{ name: 'movements.settings.title', path: 'main.movement', task: 'ManageMovements' },
				{ name: 'notifications.settings.title', path: 'main.notification', task: 'ManageNotifications' },
				{ name: 'notificationTypes.settings.title', path: 'main.notificationtype', task: 'ManageNotificationTypes' },
				{ name: 'operationTypes.settings.title', path: 'main.operationtype', task: 'ManageOperationTypes' },
				{ name: 'parts.settings.title', path: 'main.part', task: 'ManageParts' },
				{ name: 'plans.settings.title', path: 'main.plan', task: 'ManagePlans' },
				{ name: 'processes.settings.title', path: 'main.process', task: 'ManageProcesses' },
				{ name: 'schedulerCreations.settings.title', path: 'main.schedulercreation', task: 'ManageSchedulerCreations' },
				{ name: 'schedulerStatus.settings.title', path: 'main.schedulerstatu', task: 'ManageSchedulerStatus' },
				{ name: 'schedulerTrackingCodes.settings.title', path: 'main.schedulertrackingcode', task: 'ManageSchedulerTrackingCodes' },
				{ name: 'states.settings.title', path: 'main.state', task: 'ManageStates' },
				{ name: 'trackingCodes.settings.title', path: 'main.trackingcode', task: 'ManageTrackingCodes' },
			]
		},
		{
			name: 'global.menu.secondary',
			icon: 'star_rate',
			task: 'ControlPanelAccess',
			submenu: [
			]
		},
		{
			name: 'global.menu.reports',
			icon: 'description',
			task: 'ControlPanelAccess',
			submenu: [
				{ name: 'reports.users.title', path: 'main.reports', task: 'ManageUsers' }
			]
		},
		{
			name: 'global.menu.management',
			icon: 'settings',
			task: 'ControlPanelAccess',
			submenu: [
				{ name: 'users.settings.title', path: 'main.user', task: 'ManageUsers' }
			]
		}
	]
});