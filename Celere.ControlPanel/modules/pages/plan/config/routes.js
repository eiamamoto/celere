﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider.state('main.plan', {
		url: 'plan',
		resolve: {
			list: ['$stateParams', 'Plan', function ($stateParams, Plan) {
				return Plan.list().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/plan/views/plan.list.html',
				controller: 'planListCtrl',
				controllerAs: 'vm'
			}
		}
	});

	$stateProvider.state('main.plan-edit', {
		url: 'plan/:id',
		resolve: {
			model: ['$stateParams', 'Plan', function ($stateParams, Plan) {
				if ($stateParams.id) {
					return Plan.get({ id: $stateParams.id }).$promise;
				}

				return {};
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/plan/views/plan.edit.html',
				controller: 'planEditCtrl',
				controllerAs: 'vm'
			}
		}
	});
});