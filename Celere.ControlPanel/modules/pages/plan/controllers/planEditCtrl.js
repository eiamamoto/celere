﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('planEditCtrl', function ($scope, $rootScope, $window, $state, $http, $filter, MessageService, model, Plan) {
	var vm = this;

	var $translate = $filter('translate');

	vm.model = model;

	angular.extend($scope.$parent.vm, {
		title: 'plans.settings.title',
		insertUrl: null
	});

	$rootScope.laddaLoading = false;

	vm.save = function () {
		$rootScope.laddaLoading = true;


		Plan.upsert(vm.model, function () {
			$state.go('main.plan');
		}, function (response) {
			MessageService.alertError(response.data);
		});
	}
});