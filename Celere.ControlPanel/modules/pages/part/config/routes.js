﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider.state('main.part', {
		url: 'part',
		resolve: {
			list: ['$stateParams', 'Part', function ($stateParams, Part) {
				return Part.list().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/part/views/part.list.html',
				controller: 'partListCtrl',
				controllerAs: 'vm'
			}
		}
	});

	$stateProvider.state('main.part-edit', {
		url: 'part/:id',
		resolve: {
			model: ['$stateParams', 'Part', function ($stateParams, Part) {
				if ($stateParams.id) {
					return Part.get({ id: $stateParams.id }).$promise;
				}

				return {};
			}],
			processes: ['Process', function (Process) {
				return Process.listIds().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/part/views/part.edit.html',
				controller: 'partEditCtrl',
				controllerAs: 'vm'
			}
		}
	});
});