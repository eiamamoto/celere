﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('partEditCtrl', function ($scope, $rootScope, $window, $state, $http, $filter, MessageService, model, processes, Part) {
	var vm = this;

	var $translate = $filter('translate');

	vm.model = model;
	vm.processes = processes;

	angular.extend($scope.$parent.vm, {
		title: 'parts.settings.title',
		insertUrl: null
	});

	$rootScope.laddaLoading = false;

	vm.save = function () {
		$rootScope.laddaLoading = true;


		Part.upsert(vm.model, function () {
			$state.go('main.part');
		}, function (response) {
			MessageService.alertError(response.data);
		});
	}
});