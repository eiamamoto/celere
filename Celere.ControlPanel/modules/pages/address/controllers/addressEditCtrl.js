﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('addressEditCtrl', function ($scope, $rootScope, $window, $state, $http, $filter, MessageService, model, cities, Address) {
	var vm = this;

	var $translate = $filter('translate');

	vm.model = model;
	vm.cities = cities;

	angular.extend($scope.$parent.vm, {
		title: 'addresses.settings.title',
		insertUrl: null
	});

	$rootScope.laddaLoading = false;

	vm.save = function () {
		$rootScope.laddaLoading = true;


		Address.upsert(vm.model, function () {
			$state.go('main.address');
		}, function (response) {
			MessageService.alertError(response.data);
		});
	}
});