﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider.state('main.address', {
		url: 'address',
		resolve: {
			list: ['$stateParams', 'Address', function ($stateParams, Address) {
				return Address.list().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/address/views/address.list.html',
				controller: 'addressListCtrl',
				controllerAs: 'vm'
			}
		}
	});

	$stateProvider.state('main.address-edit', {
		url: 'address/:id',
		resolve: {
			model: ['$stateParams', 'Address', function ($stateParams, Address) {
				if ($stateParams.id) {
					return Address.get({ id: $stateParams.id }).$promise;
				}

				return {};
			}],
			cities: ['City', function (City) {
				return City.listIds().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/address/views/address.edit.html',
				controller: 'addressEditCtrl',
				controllerAs: 'vm'
			}
		}
	});
});