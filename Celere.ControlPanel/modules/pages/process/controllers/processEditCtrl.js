﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('processEditCtrl', function ($scope, $rootScope, $window, $state, $http, $filter, MessageService, model, Process) {
	var vm = this;

	var $translate = $filter('translate');

	vm.model = model;

	angular.extend($scope.$parent.vm, {
		title: 'processes.settings.title',
		insertUrl: null
	});

	$rootScope.laddaLoading = false;

	vm.save = function () {
		$rootScope.laddaLoading = true;


		Process.upsert(vm.model, function () {
			$state.go('main.process');
		}, function (response) {
			MessageService.alertError(response.data);
		});
	}
});