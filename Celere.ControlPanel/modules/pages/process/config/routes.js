﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider.state('main.process', {
		url: 'process',
		resolve: {
			list: ['$stateParams', 'Process', function ($stateParams, Process) {
				return Process.list().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/process/views/process.list.html',
				controller: 'processListCtrl',
				controllerAs: 'vm'
			}
		}
	});

	$stateProvider.state('main.process-edit', {
		url: 'process/:id',
		resolve: {
			model: ['$stateParams', 'Process', function ($stateParams, Process) {
				if ($stateParams.id) {
					return Process.get({ id: $stateParams.id }).$promise;
				}

				return {};
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/process/views/process.edit.html',
				controller: 'processEditCtrl',
				controllerAs: 'vm'
			}
		}
	});
});