﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider.state('main.state', {
		url: 'state',
		resolve: {
			list: ['$stateParams', 'State', function ($stateParams, State) {
				return State.list().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/state/views/state.list.html',
				controller: 'stateListCtrl',
				controllerAs: 'vm'
			}
		}
	});

	$stateProvider.state('main.state-edit', {
		url: 'state/:id',
		resolve: {
			model: ['$stateParams', 'State', function ($stateParams, State) {
				if ($stateParams.id) {
					return State.get({ id: $stateParams.id }).$promise;
				}

				return {};
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/state/views/state.edit.html',
				controller: 'stateEditCtrl',
				controllerAs: 'vm'
			}
		}
	});
});