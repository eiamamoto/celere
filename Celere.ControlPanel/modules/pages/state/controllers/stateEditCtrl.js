﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('stateEditCtrl', function ($scope, $rootScope, $window, $state, $http, $filter, MessageService, model, State) {
	var vm = this;

	var $translate = $filter('translate');

	vm.model = model;

	angular.extend($scope.$parent.vm, {
		title: 'states.settings.title',
		insertUrl: null
	});

	$rootScope.laddaLoading = false;

	vm.save = function () {
		$rootScope.laddaLoading = true;


		State.upsert(vm.model, function () {
			$state.go('main.state');
		}, function (response) {
			MessageService.alertError(response.data);
		});
	}
});