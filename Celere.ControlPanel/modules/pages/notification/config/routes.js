﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider.state('main.notification', {
		url: 'notification',
		resolve: {
			list: ['$stateParams', 'Notification', function ($stateParams, Notification) {
				return Notification.list().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/notification/views/notification.list.html',
				controller: 'notificationListCtrl',
				controllerAs: 'vm'
			}
		}
	});

	$stateProvider.state('main.notification-edit', {
		url: 'notification/:id',
		resolve: {
			model: ['$stateParams', 'Notification', function ($stateParams, Notification) {
				if ($stateParams.id) {
					return Notification.get({ id: $stateParams.id }).$promise;
				}

				return {};
			}],
			processes: ['Process', function (Process) {
				return Process.listIds().$promise;
			}],
			customers: ['Customer', function (Customer) {
				return Customer.listIds().$promise;
			}],
			notificationtypes: ['NotificationType', function (NotificationType) {
				return NotificationType.listIds().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/notification/views/notification.edit.html',
				controller: 'notificationEditCtrl',
				controllerAs: 'vm'
			}
		}
	});
});