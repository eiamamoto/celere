﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('notificationEditCtrl', function ($scope, $rootScope, $window, $state, $http, $filter, MessageService, model, processes, customers, notificationtypes, Notification) {
	var vm = this;

	var $translate = $filter('translate');

	vm.model = model;
	vm.processes = processes;
	vm.customers = customers;
	vm.notificationtypes = notificationtypes;

	angular.extend($scope.$parent.vm, {
		title: 'notifications.settings.title',
		insertUrl: null
	});

	$rootScope.laddaLoading = false;

	vm.save = function () {
		$rootScope.laddaLoading = true;


		Notification.upsert(vm.model, function () {
			$state.go('main.notification');
		}, function (response) {
			MessageService.alertError(response.data);
		});
	}
});