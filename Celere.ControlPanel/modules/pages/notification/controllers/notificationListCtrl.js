﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('notificationListCtrl', function ($scope, $rootScope, $state, $http, $filter, settings, apiSettings, list, MessageService, Notification) {
	var vm = this;

	var $translate = $filter('translate');

	angular.extend($scope.$parent.vm, {
		title: 'notifications.settings.title',
		insertUrl: 'main.notification-edit'
	});

	$rootScope.laddaLoading = false;

	// Pagination
	vm.pagination = {
		list: list,
		currentPage: 1,
		pageSize: apiSettings.defaultPageSize,
		pagesTotal: list.total
	};

	$scope.$watch('vm.keyword', function (newValue, oldValue) {
		if (newValue !== oldValue) {
			vm.refresh(vm.pagination.currentPage, newValue);
		}
	});

	$scope.$watch('vm.pagination.currentPage', function (newValue, oldValue) {
		if (newValue !== oldValue) {
			vm.refresh(newValue, vm.keyword);
		}
	});

	vm.refresh = function (page, keyword) {
		Notification.list({ page: page, keyword: keyword }).$promise.then(function (data) {
			vm.pagination.list = data;
			vm.pagination.currentPage = data.page;
			vm.pagination.pagesTotal = data.total;
		});
	}

	vm.delete = function (id, index) {
		MessageService.confirmDelete(function () {
			Notification.delete({ id: id }, function () {
				vm.pagination.list.rows.splice(index, 1);
			});
		});
	}
});