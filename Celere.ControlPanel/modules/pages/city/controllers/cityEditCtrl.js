﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('cityEditCtrl', function ($scope, $rootScope, $window, $state, $http, $filter, MessageService, model, states, City) {
	var vm = this;

	var $translate = $filter('translate');

	vm.model = model;
	vm.states = states;

	angular.extend($scope.$parent.vm, {
		title: 'cities.settings.title',
		insertUrl: null
	});

	$rootScope.laddaLoading = false;

	vm.save = function () {
		$rootScope.laddaLoading = true;


		City.upsert(vm.model, function () {
			$state.go('main.city');
		}, function (response) {
			MessageService.alertError(response.data);
		});
	}
});