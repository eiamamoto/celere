﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider.state('main.city', {
		url: 'city',
		resolve: {
			list: ['$stateParams', 'City', function ($stateParams, City) {
				return City.list().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/city/views/city.list.html',
				controller: 'cityListCtrl',
				controllerAs: 'vm'
			}
		}
	});

	$stateProvider.state('main.city-edit', {
		url: 'city/:id',
		resolve: {
			model: ['$stateParams', 'City', function ($stateParams, City) {
				if ($stateParams.id) {
					return City.get({ id: $stateParams.id }).$promise;
				}

				return {};
			}],
			states: ['State', function (State) {
				return State.listIds().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/city/views/city.edit.html',
				controller: 'cityEditCtrl',
				controllerAs: 'vm'
			}
		}
	});
});