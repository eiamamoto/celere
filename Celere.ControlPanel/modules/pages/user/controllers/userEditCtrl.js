'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('userEditCtrl', function ($scope, $rootScope, $window, $state, $http, $filter, MessageService, model, language, User) {
	var vm = this;

	var $translate = $filter('translate');

	vm.model = model;
	vm.enumLanguage = language;
	vm.model.imageAvatar = vm.model.imageAvatar || $scope.globalConfig.cdnUrl + $scope.globalConfig.storageContainer + 'default.png';
	// vm.groups = groups;

	angular.extend($scope.$parent.vm, {
		title: 'users.settings.title',
		insertUrl: null
	});

	$rootScope.laddaLoading = false;

	vm.save = function () {
		$rootScope.laddaLoading = true;

		if (vm.model.imageAvatar) vm.model.imageAvatar = vm.model.imageAvatar.split('/').pop();

		if (!vm.model.userId) {
			User.insert(vm.model, function () {
				$state.go('main.user');
			}, function (response) {
				MessageService.alertError(response.data);
			});
		} else {
			User.update(vm.model, function () {
				$state.go('main.user');
			}, function (response) {
				MessageService.alertError(response.data);
			});
		}
	}
});