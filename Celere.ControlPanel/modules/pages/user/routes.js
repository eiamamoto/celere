'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider.state('main.user', {
		url: 'user',
		resolve: {
			list: ['$stateParams', 'User', function ($stateParams, User) {
				return User.list().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/user/views/user.list.html',
				controller: 'userListCtrl',
				controllerAs: 'vm'
			}
		}
	});

	$stateProvider.state('main.user-edit', {
		url: 'user/:id',
		resolve: {
			model: ['$stateParams', 'User', function ($stateParams, User) {
				if ($stateParams.id) {
					return User.get({ id: $stateParams.id }).$promise;
				}

				return {};
			}],
			language: ['Enumerators', function (Enumerators) {
				return Enumerators.query({ name: 'language' }).$promise;
			}],
			// groups: ['Internal', function (Internal) {
			// 	return Internal.groups().$promise;
			// }]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/user/views/user.edit.html',
				controller: 'userEditCtrl',
				controllerAs: 'vm'
			}
		}
	});
});