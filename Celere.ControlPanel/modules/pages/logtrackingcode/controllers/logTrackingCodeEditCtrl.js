﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('logtrackingcodeEditCtrl', function ($scope, $rootScope, $window, $state, $http, $filter, MessageService, model, operationtypes, trackingcodes, LogTrackingCode) {
	var vm = this;

	var $translate = $filter('translate');

	vm.model = model;
	vm.operationtypes = operationtypes;
	vm.trackingcodes = trackingcodes;

	angular.extend($scope.$parent.vm, {
		title: 'logTrackingCodes.settings.title',
		insertUrl: null
	});

	$rootScope.laddaLoading = false;

	vm.save = function () {
		$rootScope.laddaLoading = true;


		LogTrackingCode.upsert(vm.model, function () {
			$state.go('main.logtrackingcode');
		}, function (response) {
			MessageService.alertError(response.data);
		});
	}
});