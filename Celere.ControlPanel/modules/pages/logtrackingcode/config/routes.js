﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider.state('main.logtrackingcode', {
		url: 'logtrackingcode',
		resolve: {
			list: ['$stateParams', 'LogTrackingCode', function ($stateParams, LogTrackingCode) {
				return LogTrackingCode.list().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/logtrackingcode/views/logtrackingcode.list.html',
				controller: 'logtrackingcodeListCtrl',
				controllerAs: 'vm'
			}
		}
	});

	$stateProvider.state('main.logtrackingcode-edit', {
		url: 'logtrackingcode/:id',
		resolve: {
			model: ['$stateParams', 'LogTrackingCode', function ($stateParams, LogTrackingCode) {
				if ($stateParams.id) {
					return LogTrackingCode.get({ id: $stateParams.id }).$promise;
				}

				return {};
			}],
			operationtypes: ['OperationType', function (OperationType) {
				return OperationType.listIds().$promise;
			}],
			trackingcodes: ['TrackingCode', function (TrackingCode) {
				return TrackingCode.listIds().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/logtrackingcode/views/logtrackingcode.edit.html',
				controller: 'logtrackingcodeEditCtrl',
				controllerAs: 'vm'
			}
		}
	});
});