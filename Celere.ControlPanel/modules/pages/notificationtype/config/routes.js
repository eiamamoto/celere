﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider.state('main.notificationtype', {
		url: 'notificationtype',
		resolve: {
			list: ['$stateParams', 'NotificationType', function ($stateParams, NotificationType) {
				return NotificationType.list().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/notificationtype/views/notificationtype.list.html',
				controller: 'notificationtypeListCtrl',
				controllerAs: 'vm'
			}
		}
	});

	$stateProvider.state('main.notificationtype-edit', {
		url: 'notificationtype/:id',
		resolve: {
			model: ['$stateParams', 'NotificationType', function ($stateParams, NotificationType) {
				if ($stateParams.id) {
					return NotificationType.get({ id: $stateParams.id }).$promise;
				}

				return {};
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/notificationtype/views/notificationtype.edit.html',
				controller: 'notificationtypeEditCtrl',
				controllerAs: 'vm'
			}
		}
	});
});