﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('notificationtypeEditCtrl', function ($scope, $rootScope, $window, $state, $http, $filter, MessageService, model, NotificationType) {
	var vm = this;

	var $translate = $filter('translate');

	vm.model = model;

	angular.extend($scope.$parent.vm, {
		title: 'notificationTypes.settings.title',
		insertUrl: null
	});

	$rootScope.laddaLoading = false;

	vm.save = function () {
		$rootScope.laddaLoading = true;


		NotificationType.upsert(vm.model, function () {
			$state.go('main.notificationtype');
		}, function (response) {
			MessageService.alertError(response.data);
		});
	}
});