﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider.state('main.operationtype', {
		url: 'operationtype',
		resolve: {
			list: ['$stateParams', 'OperationType', function ($stateParams, OperationType) {
				return OperationType.list().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/operationtype/views/operationtype.list.html',
				controller: 'operationtypeListCtrl',
				controllerAs: 'vm'
			}
		}
	});

	$stateProvider.state('main.operationtype-edit', {
		url: 'operationtype/:id',
		resolve: {
			model: ['$stateParams', 'OperationType', function ($stateParams, OperationType) {
				if ($stateParams.id) {
					return OperationType.get({ id: $stateParams.id }).$promise;
				}

				return {};
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/operationtype/views/operationtype.edit.html',
				controller: 'operationtypeEditCtrl',
				controllerAs: 'vm'
			}
		}
	});
});