﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).factory('OperationType', ['$resource', '$stateParams', 'apiSettings', function ($resource, $stateParams, apiSettings) {
	return $resource(apiSettings.baseUri + 'v1/operationtypes/:id', { id: '@id' }, {
		'list': {
			method: 'GET',
			isArray: false,
			params: {
				page: 1,
				pageSize: apiSettings.defaultPageSize,
				keyword: ''
			}
		},
		'listIds': {
			url: apiSettings.baseUri + 'v1/operationtypes/list/ids',
			method: 'GET',
			isArray: true
		},
		'upsert': {
			method: 'POST'
		},
		'delete': {
			method: 'DELETE'
		}
	});
}]);