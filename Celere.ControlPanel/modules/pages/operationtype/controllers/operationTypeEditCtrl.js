﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('operationtypeEditCtrl', function ($scope, $rootScope, $window, $state, $http, $filter, MessageService, model, OperationType) {
	var vm = this;

	var $translate = $filter('translate');

	vm.model = model;

	angular.extend($scope.$parent.vm, {
		title: 'operationTypes.settings.title',
		insertUrl: null
	});

	$rootScope.laddaLoading = false;

	vm.save = function () {
		$rootScope.laddaLoading = true;


		OperationType.upsert(vm.model, function () {
			$state.go('main.operationtype');
		}, function (response) {
			MessageService.alertError(response.data);
		});
	}
});