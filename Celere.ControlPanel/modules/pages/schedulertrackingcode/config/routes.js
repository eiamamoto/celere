﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider.state('main.schedulertrackingcode', {
		url: 'schedulertrackingcode',
		resolve: {
			list: ['$stateParams', 'SchedulerTrackingCode', function ($stateParams, SchedulerTrackingCode) {
				return SchedulerTrackingCode.list().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/schedulertrackingcode/views/schedulertrackingcode.list.html',
				controller: 'schedulertrackingcodeListCtrl',
				controllerAs: 'vm'
			}
		}
	});

	$stateProvider.state('main.schedulertrackingcode-edit', {
		url: 'schedulertrackingcode/:id',
		resolve: {
			model: ['$stateParams', 'SchedulerTrackingCode', function ($stateParams, SchedulerTrackingCode) {
				if ($stateParams.id) {
					return SchedulerTrackingCode.get({ id: $stateParams.id }).$promise;
				}

				return {};
			}],
			schedulerstatus: ['SchedulerStatu', function (SchedulerStatu) {
				return SchedulerStatu.listIds().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/schedulertrackingcode/views/schedulertrackingcode.edit.html',
				controller: 'schedulertrackingcodeEditCtrl',
				controllerAs: 'vm'
			}
		}
	});
});