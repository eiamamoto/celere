﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('schedulertrackingcodeEditCtrl', function ($scope, $rootScope, $window, $state, $http, $filter, MessageService, model, schedulerstatus, SchedulerTrackingCode) {
	var vm = this;

	var $translate = $filter('translate');

	vm.model = model;
	vm.schedulerstatus = schedulerstatus;

	angular.extend($scope.$parent.vm, {
		title: 'schedulerTrackingCodes.settings.title',
		insertUrl: null
	});

	$rootScope.laddaLoading = false;

	vm.save = function () {
		$rootScope.laddaLoading = true;


		SchedulerTrackingCode.upsert(vm.model, function () {
			$state.go('main.schedulertrackingcode');
		}, function (response) {
			MessageService.alertError(response.data);
		});
	}
});