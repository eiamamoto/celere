﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('schedulerstatuEditCtrl', function ($scope, $rootScope, $window, $state, $http, $filter, MessageService, model, SchedulerStatu) {
	var vm = this;

	var $translate = $filter('translate');

	vm.model = model;

	angular.extend($scope.$parent.vm, {
		title: 'schedulerStatus.settings.title',
		insertUrl: null
	});

	$rootScope.laddaLoading = false;

	vm.save = function () {
		$rootScope.laddaLoading = true;


		SchedulerStatu.upsert(vm.model, function () {
			$state.go('main.schedulerstatu');
		}, function (response) {
			MessageService.alertError(response.data);
		});
	}
});