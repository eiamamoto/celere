﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider.state('main.schedulerstatu', {
		url: 'schedulerstatu',
		resolve: {
			list: ['$stateParams', 'SchedulerStatu', function ($stateParams, SchedulerStatu) {
				return SchedulerStatu.list().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/schedulerstatu/views/schedulerstatu.list.html',
				controller: 'schedulerstatuListCtrl',
				controllerAs: 'vm'
			}
		}
	});

	$stateProvider.state('main.schedulerstatu-edit', {
		url: 'schedulerstatu/:id',
		resolve: {
			model: ['$stateParams', 'SchedulerStatu', function ($stateParams, SchedulerStatu) {
				if ($stateParams.id) {
					return SchedulerStatu.get({ id: $stateParams.id }).$promise;
				}

				return {};
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/schedulerstatu/views/schedulerstatu.edit.html',
				controller: 'schedulerstatuEditCtrl',
				controllerAs: 'vm'
			}
		}
	});
});