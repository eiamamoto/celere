﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider.state('main.trackingcode', {
		url: 'trackingcode',
		resolve: {
			list: ['$stateParams', 'TrackingCode', function ($stateParams, TrackingCode) {
				return TrackingCode.list().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/trackingcode/views/trackingcode.list.html',
				controller: 'trackingcodeListCtrl',
				controllerAs: 'vm'
			}
		}
	});

	$stateProvider.state('main.trackingcode-edit', {
		url: 'trackingcode/:id',
		resolve: {
			model: ['$stateParams', 'TrackingCode', function ($stateParams, TrackingCode) {
				if ($stateParams.id) {
					return TrackingCode.get({ id: $stateParams.id }).$promise;
				}

				return {};
			}],
			customers: ['Customer', function (Customer) {
				return Customer.listIds().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/trackingcode/views/trackingcode.edit.html',
				controller: 'trackingcodeEditCtrl',
				controllerAs: 'vm'
			}
		}
	});
});