﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('trackingcodeEditCtrl', function ($scope, $rootScope, $window, $state, $http, $filter, MessageService, model, customers, TrackingCode) {
	var vm = this;

	var $translate = $filter('translate');

	vm.model = model;
	vm.customers = customers;

	angular.extend($scope.$parent.vm, {
		title: 'trackingCodes.settings.title',
		insertUrl: null
	});

	$rootScope.laddaLoading = false;

	vm.save = function () {
		$rootScope.laddaLoading = true;


		TrackingCode.upsert(vm.model, function () {
			$state.go('main.trackingcode');
		}, function (response) {
			MessageService.alertError(response.data);
		});
	}
});