﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('schedulercreationEditCtrl', function ($scope, $rootScope, $window, $state, $http, $filter, MessageService, model, SchedulerCreation) {
	var vm = this;

	var $translate = $filter('translate');

	vm.model = model;

	angular.extend($scope.$parent.vm, {
		title: 'schedulerCreations.settings.title',
		insertUrl: null
	});

	$rootScope.laddaLoading = false;

	vm.save = function () {
		$rootScope.laddaLoading = true;


		SchedulerCreation.upsert(vm.model, function () {
			$state.go('main.schedulercreation');
		}, function (response) {
			MessageService.alertError(response.data);
		});
	}
});