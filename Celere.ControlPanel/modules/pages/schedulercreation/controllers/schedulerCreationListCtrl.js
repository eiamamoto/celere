﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('schedulercreationListCtrl', function ($scope, $rootScope, $state, $http, $filter, settings, apiSettings, list, MessageService, SchedulerCreation) {
	var vm = this;

	var $translate = $filter('translate');

	angular.extend($scope.$parent.vm, {
		title: 'schedulerCreations.settings.title',
		insertUrl: 'main.schedulercreation-edit'
	});

	$rootScope.laddaLoading = false;

	// Pagination
	vm.pagination = {
		list: list,
		currentPage: 1,
		pageSize: apiSettings.defaultPageSize,
		pagesTotal: list.total
	};

	$scope.$watch('vm.keyword', function (newValue, oldValue) {
		if (newValue !== oldValue) {
			vm.refresh(vm.pagination.currentPage, newValue);
		}
	});

	$scope.$watch('vm.pagination.currentPage', function (newValue, oldValue) {
		if (newValue !== oldValue) {
			vm.refresh(newValue, vm.keyword);
		}
	});

	vm.refresh = function (page, keyword) {
		SchedulerCreation.list({ page: page, keyword: keyword }).$promise.then(function (data) {
			vm.pagination.list = data;
			vm.pagination.currentPage = data.page;
			vm.pagination.pagesTotal = data.total;
		});
	}

	vm.delete = function (id, index) {
		MessageService.confirmDelete(function () {
			SchedulerCreation.delete({ id: id }, function () {
				vm.pagination.list.rows.splice(index, 1);
			});
		});
	}
});