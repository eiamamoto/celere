﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider.state('main.schedulercreation', {
		url: 'schedulercreation',
		resolve: {
			list: ['$stateParams', 'SchedulerCreation', function ($stateParams, SchedulerCreation) {
				return SchedulerCreation.list().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/schedulercreation/views/schedulercreation.list.html',
				controller: 'schedulercreationListCtrl',
				controllerAs: 'vm'
			}
		}
	});

	$stateProvider.state('main.schedulercreation-edit', {
		url: 'schedulercreation/:id',
		resolve: {
			model: ['$stateParams', 'SchedulerCreation', function ($stateParams, SchedulerCreation) {
				if ($stateParams.id) {
					return SchedulerCreation.get({ id: $stateParams.id }).$promise;
				}

				return {};
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/schedulercreation/views/schedulercreation.edit.html',
				controller: 'schedulercreationEditCtrl',
				controllerAs: 'vm'
			}
		}
	});
});