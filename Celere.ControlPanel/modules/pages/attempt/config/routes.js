﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider.state('main.attempt', {
		url: 'attempt',
		resolve: {
			list: ['$stateParams', 'Attempt', function ($stateParams, Attempt) {
				return Attempt.list().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/attempt/views/attempt.list.html',
				controller: 'attemptListCtrl',
				controllerAs: 'vm'
			}
		}
	});

	$stateProvider.state('main.attempt-edit', {
		url: 'attempt/:id',
		resolve: {
			model: ['$stateParams', 'Attempt', function ($stateParams, Attempt) {
				if ($stateParams.id) {
					return Attempt.get({ id: $stateParams.id }).$promise;
				}

				return {};
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/attempt/views/attempt.edit.html',
				controller: 'attemptEditCtrl',
				controllerAs: 'vm'
			}
		}
	});
});