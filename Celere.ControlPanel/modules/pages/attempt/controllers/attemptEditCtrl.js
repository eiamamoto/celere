﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('attemptEditCtrl', function ($scope, $rootScope, $window, $state, $http, $filter, MessageService, model, Attempt) {
	var vm = this;

	var $translate = $filter('translate');

	vm.model = model;

	angular.extend($scope.$parent.vm, {
		title: 'attempts.settings.title',
		insertUrl: null
	});

	$rootScope.laddaLoading = false;

	vm.save = function () {
		$rootScope.laddaLoading = true;


		Attempt.upsert(vm.model, function () {
			$state.go('main.attempt');
		}, function (response) {
			MessageService.alertError(response.data);
		});
	}
});