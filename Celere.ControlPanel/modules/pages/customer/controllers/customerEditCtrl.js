﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('customerEditCtrl', function ($scope, $rootScope, $window, $state, $http, $filter, MessageService, model, auth, addresses, Customer) {
	var vm = this;

	var $translate = $filter('translate');

	vm.model = model;
	vm.auth = auth;
	vm.addresses = addresses;

	angular.extend($scope.$parent.vm, {
		title: 'customers.settings.title',
		insertUrl: null
	});

	$rootScope.laddaLoading = false;

	vm.save = function () {
		$rootScope.laddaLoading = true;


		Customer.upsert(vm.model, function () {
			$state.go('main.customer');
		}, function (response) {
			MessageService.alertError(response.data);
		});
	}
});