﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider.state('main.customer', {
		url: 'customer',
		resolve: {
			list: ['$stateParams', 'Customer', function ($stateParams, Customer) {
				return Customer.list().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/customer/views/customer.list.html',
				controller: 'customerListCtrl',
				controllerAs: 'vm'
			}
		}
	});

	$stateProvider.state('main.customer-edit', {
		url: 'customer/:id',
		resolve: {
			model: ['$stateParams', 'Customer', function ($stateParams, Customer) {
				if ($stateParams.id) {
					return Customer.get({ id: $stateParams.id }).$promise;
				}

				return {};
			}],
			auth: ['Auth', function (Auth) {
				return Auth.listIds().$promise;
			}],
			addresses: ['Address', function (Address) {
				return Address.listIds().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/customer/views/customer.edit.html',
				controller: 'customerEditCtrl',
				controllerAs: 'vm'
			}
		}
	});
});