﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider.state('main.movement', {
		url: 'movement',
		resolve: {
			list: ['$stateParams', 'Movement', function ($stateParams, Movement) {
				return Movement.list().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/movement/views/movement.list.html',
				controller: 'movementListCtrl',
				controllerAs: 'vm'
			}
		}
	});

	$stateProvider.state('main.movement-edit', {
		url: 'movement/:id',
		resolve: {
			model: ['$stateParams', 'Movement', function ($stateParams, Movement) {
				if ($stateParams.id) {
					return Movement.get({ id: $stateParams.id }).$promise;
				}

				return {};
			}],
			processes: ['Process', function (Process) {
				return Process.listIds().$promise;
			}]
		},
		views: {
			'main': {
				templateUrl: '/modules/pages/movement/views/movement.edit.html',
				controller: 'movementEditCtrl',
				controllerAs: 'vm'
			}
		}
	});
});