﻿'use strict';

angular.module(ApplicationConfiguration.applicationModuleName).controller('movementEditCtrl', function ($scope, $rootScope, $window, $state, $http, $filter, MessageService, model, processes, Movement) {
	var vm = this;

	var $translate = $filter('translate');

	vm.model = model;
	vm.processes = processes;

	angular.extend($scope.$parent.vm, {
		title: 'movements.settings.title',
		insertUrl: null
	});

	$rootScope.laddaLoading = false;

	vm.save = function () {
		$rootScope.laddaLoading = true;


		Movement.upsert(vm.model, function () {
			$state.go('main.movement');
		}, function (response) {
			MessageService.alertError(response.data);
		});
	}
});