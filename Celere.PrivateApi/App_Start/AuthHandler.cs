﻿using Asteria.Core.Settings;
using Jose;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Celere.Business.Model.Internal;

namespace Celere.PrivateApi.App_Start
{
	/// <summary></summary>
	public class AuthHandler : Asteria.API.App_Start.AuthHandler
	{
		#region Support Methods

		/// <summary>Create a JWT with user information</summary>
		/// <param name="user"></param>
		/// <param name="data"></param>
		/// <returns></returns>
		public static string CreateToken(UserVM user, out object data)
		{
			var unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
			var expiry = Math.Round((DateTime.UtcNow.AddDays(60) - unixEpoch).TotalSeconds);
			var issuedAt = Math.Round((DateTime.UtcNow - unixEpoch).TotalSeconds);
			var notBefore = Math.Round((DateTime.UtcNow.AddMonths(6) - unixEpoch).TotalSeconds);

			var payload = new Dictionary<string, object>
			{
				{"email", user.Email},
				{"authId", user.AuthId},
				{"sub", user.AuthId},
				{"nbf", notBefore},
				{"iat", issuedAt},
				{"exp", expiry}
			};

			byte[] secret = Encoding.ASCII.GetBytes(GlobalSettings.JWT_PassToken);

			var token = Jose.JWT.Encode(payload, secret, JwsAlgorithm.HS256);

			data = new
			{
				authId = user.AuthId,
				email = user.Email,
				imageAvatar = user.ImageAvatar,
				permissions = user.Permissions
			};

			return token;
		}

		#endregion
	}
}