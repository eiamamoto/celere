﻿using Asteria.API.App_Start;
using Asteria.Core.Settings;
using Newtonsoft.Json.Converters;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Celere.PrivateApi
{
	/// <summary></summary>
	public static class WebApiConfig
	{
		/// <summary></summary>
		/// <param name="config"></param>
		public static void Register(HttpConfiguration config)
		{
			// Web API configuration and services
			config.Formatters.JsonFormatter.SerializerSettings = JSON.ProjectSerializerSettings;
			config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter());
			config.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;
			config.Formatters.Remove(config.Formatters.XmlFormatter);

			// JWT
			config.MessageHandlers.Add(new AuthHandler());

			// CORS
			config.EnableCors(new EnableCorsAttribute("*", "*", "*"));

			// Web API routes
			config.MapHttpAttributeRoutes();

			config.Routes.MapHttpRoute(
				name: "DefaultApi",
				routeTemplate: "api/{controller}/{id}",
				defaults: new { id = RouteParameter.Optional }
			);
		}
	}
}