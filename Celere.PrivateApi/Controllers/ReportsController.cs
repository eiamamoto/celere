﻿using Asteria.API.Controllers;
using log4net;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using Celere.Business;

namespace Celere.PrivateApi.Controllers
{
	/// <summary></summary>
	[RoutePrefix("v1/reports")]
	public class ReportsController : BaseApiController
	{
		#region Constructor

		/// <summary></summary>
		public ReportsController()
		{
			logger = LogManager.GetLogger("Celere.PrivateApi.Controllers.ReportsController");
		}

		#endregion

		/// <summary>Users Report</summary>
		/// <param name="from">Initial date</param>
		/// <param name="to">Finish date</param>
		/// <returns></returns>
		[HttpGet]
		[Route("users")]
		public async Task<IHttpActionResult> Users(DateTime from, DateTime to)
		{
			using (var bl = new ReportsBL())
			{
				return Ok(await bl.UsersAsync(from, to, ContextUserId));
			}
		}

		/// <summary>Users Report (Export to Excel)</summary>
		/// <param name="from">Initial date</param>
		/// <param name="to">Finish date</param>
		/// <returns></returns>
		[HttpGet]
		[Route("users/excel")]
		public async Task<IHttpActionResult> UsersToExcel(DateTime from, DateTime to)
		{
			using (var bl = new ReportsBL())
			{
				var data = await bl.UsersAsync(from, to, ContextUserId);

				var excel = ConvertToExcel(data);

				return ResponseMessage(excel);
			}
		}

		#region Support Methods

		private HttpResponseMessage ConvertToExcel<T>(IEnumerable<T> source)
		{
			var excel = new ExcelPackage();

			var workSheet = excel.Workbook.Worksheets.Add("Sheet1");

			workSheet.Cells[1, 1].LoadFromCollection(source, true).AutoFitColumns();

			workSheet.Row(1).Style.Font.Bold = true;

			var props = source.GetType().GetGenericArguments()[0].GetProperties();

			var i = 1;

			foreach (var prop in props)
			{
				if (prop.PropertyType == typeof(DateTime))
				{
					workSheet.Column(i).Style.Numberformat.Format = "yyyy-mm-dd";
				}

				i++;
			}

			var response = new HttpResponseMessage(HttpStatusCode.OK);

			response.Content = new ByteArrayContent(excel.GetAsByteArray());
			response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
			response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.Content.Headers.ContentDisposition.FileName = string.Format("report-{0:yyyyMMddHHmmss}.xlsx", DateTime.Now);

			return response;
		}

		#endregion
	}
}