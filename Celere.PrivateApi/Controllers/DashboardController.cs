﻿using Asteria.API.Controllers;
using log4net;
using System.Threading.Tasks;
using System.Web.Http;
using Celere.Business;

namespace Celere.PrivateApi.Controllers
{
	/// <summary></summary>
	[RoutePrefix("v1/dashboard")]
	public class DashboardController : BaseApiController
	{
		#region Constructor

		/// <summary></summary>
		public DashboardController()
		{
			logger = LogManager.GetLogger("Celere.PrivateApi.Controllers.DashboardController");
		}

		#endregion Constructor

		/// <summary>Detail</summary>
		/// <returns></returns>
		[HttpGet]
		[Route("")]
		public async Task<IHttpActionResult> Dashboard()
		{
			using (var bl = new DashboardBL())
			{
				return Ok(await bl.DetailAsync(ContextUserId));
			}
		}
	}
}