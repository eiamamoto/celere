﻿using Asteria.API.Controllers;
using Asteria.API.Models;
using Asteria.Core;
using Asteria.Core.Settings;
using Asteria.Core.Tools;
using log4net;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Celere.Business;
using Celere.Business.Model.Internal;

namespace Celere.PrivateApi.Controllers
{
	/// <summary>Internal methods</summary>
	[RoutePrefix("v1/internal")]
	public class InternalController : BaseApiController
	{
		#region Constructor

		/// <summary></summary>
		public InternalController()
		{
			logger = LogManager.GetLogger("Celere.PrivateApi.Controllers.InternalController");
		}

		#endregion

		#region Users

		/// <summary>Users List</summary>
		/// <param name="page">Page number</param>
		/// <param name="pageSize">Size of the page</param>
		/// <param name="keyword">Filter keyword</param>
		/// <returns></returns>
		[HttpGet]
		[Route("users")]
		public async Task<IHttpActionResult> UsersPage(int page, int pageSize, string keyword = null)
		{
			using (var bl = new UserBL())
			{
				return Ok(await bl.PageAsync(page, pageSize, keyword, ContextUserId));
			}
		}

		/// <summary>User Id List</summary>
		/// <returns></returns>
		[HttpGet]
		[Route("users/list/ids")]
		public async Task<IHttpActionResult> UsersListIds()
		{
			using (var bl = new UserBL())
			{
				return Ok(await bl.ListIdsAsync(ContextUserId));
			}
		}

		/// <summary>User Detail</summary>
		/// <param name="id">Record identification</param>
		/// <returns></returns>
		[HttpGet]
		[Route("users/{id}")]
		public async Task<IHttpActionResult> UserDetail(int id)
		{
			using (var bl = new UserBL())
			{
				return Ok(await bl.DetailAsync(id, ContextUserId));
			}
		}

		/// <summary>Insert or update an existing user</summary>
		/// <param name="model">Object with all the required data for updating a record</param>
		/// <returns></returns>
		[HttpPut]
		[Route("users")]
		public async Task<IHttpActionResult> UserUpsert(UserVM model)
		{
			using (var bl = new UserBL())
			{
				if (string.IsNullOrWhiteSpace(model.NewPassword) == false)
				{
					model.Password = Cryptography.CreateHash(model.NewPassword);
				}

				return Ok(await bl.UpsertAsync(model, ContextUserId));
			}
		}

		/// <summary>Delete (or inactivate) the given user</summary>
		/// <param name="id">Record identification</param>
		/// <returns></returns>
		[HttpDelete]
		[Route("users/{id}")]
		public async Task<IHttpActionResult> UserDelete(int id)
		{
			using (var bl = new UserBL())
			{
				await bl.DeleteAsync(id, ContextUserId);

				return Ok(true);
			}
		}

		/// <summary>Avatar Upload</summary>
		/// <param name="isFlow">Upload by Flow.js</param>
		[HttpPost]
		[Route("users/avatar")]
		public async Task<IHttpActionResult> UserUploadAvatar(bool isFlow = true)
		{
			var path = string.Concat(GlobalSettings.Storage_Container, "users/");

			UploadResultVM uploadedFile = (isFlow) ? await FlowUploadImageAsync(path) : await StandardUploadImageAsync(path);

			if (uploadedFile == null)
			{
				return Ok(true);
			}

			if (string.IsNullOrWhiteSpace(uploadedFile.Filename))
			{
				throw new AsteriaException(SystemMessages.Error_FileSave, HttpStatusCode.BadRequest);
			}

			var url = Flurl.Url.Combine(GlobalSettings.RootUrl_CDN, path, uploadedFile.Filename);

			return Ok(url);
		}

		#endregion

		#region Groups

		/// <summary>Internal Groups List</summary>
		/// <param name="page">Page number</param>
		/// <param name="pageSize">Size of the page</param>
		/// <param name="keyword">Filter keyword</param>
		/// <returns></returns>
		[HttpGet]
		[Route("groups")]
		public async Task<IHttpActionResult> GroupsPage(int page, int pageSize, string keyword = null)
		{
			using (var bl = new GroupsBL())
			{
				return Ok(await bl.PageAsync(page, pageSize, keyword, ContextUserId));
			}
		}

		/// <summary>Internal Groups Id List</summary>
		/// <returns></returns>
		[HttpGet]
		[Route("groups/list/ids")]
		public async Task<IHttpActionResult> GroupsListIds()
		{
			using (var bl = new GroupsBL())
			{
				return Ok(await bl.ListIdsAsync(ContextUserId));
			}
		}

		/// <summary>Internal Group Detail</summary>
		/// <param name="id">Record identification</param>
		/// <returns></returns>
		[HttpGet]
		[Route("groups/{id}")]
		public async Task<IHttpActionResult> GroupsDetail(int id)
		{
			using (var bl = new GroupsBL())
			{
				return Ok(await bl.DetailAsync(id, ContextUserId));
			}
		}

		/// <summary>Insert or update an existing Internal Group</summary>
		/// <param name="model">Object with all the required data for updating a record</param>
		/// <returns></returns>
		[HttpPost]
		[Route("groups")]
		public async Task<IHttpActionResult> GroupsUpdate(GroupVM model)
		{
			using (var bl = new GroupsBL())
			{
				return Ok(await bl.UpsertAsync(model, ContextUserId));
			}
		}

		/// <summary>Delete (or inactivate) the given Internal Group</summary>
		/// <param name="id">Record identification</param>
		/// <returns></returns>
		[HttpDelete]
		[Route("groups/{id}")]
		public async Task<IHttpActionResult> GroupsDelete(int id)
		{
			using (var bl = new GroupsBL())
			{
				await bl.DeleteAsync(id, ContextUserId);

				return Ok(true);
			}
		}

		#endregion

		#region Logs
		
		/// <summary>Logs List</summary>
		/// <param name="page">Page number</param>
		/// <param name="pageSize">Size of the page</param>
		/// <param name="keyword">Filter keyword</param>
		/// <returns></returns>
		[HttpGet]
		[Route("logs")]
		public async Task<IHttpActionResult> LogsPage(int page, int pageSize, string keyword = null)
		{
			using (var bl = new LogsBL())
			{
				return Ok(await bl.PageAsync(page, pageSize, keyword, ContextUserId));
			}
		}

		/// <summary>Log Detail</summary>
		/// <param name="id">Record identification</param>
		/// <returns></returns>
		[HttpGet]
		[Route("logs/{id}")]
		public async Task<IHttpActionResult> LogsDetail(int id)
		{
			using (var bl = new LogsBL())
			{
				return Ok(await bl.DetailAsync(id, ContextUserId));
			}
		}

		#endregion

		#region Parameters

		/// <summary>Parameters List</summary>
		/// <param name="page">Page number</param>
		/// <param name="pageSize">Size of the page</param>
		/// <param name="keyword">Filter keyword</param>
		/// <returns></returns>
		[HttpGet]
		[Route("parameters")]
		public async Task<IHttpActionResult> ParametersPage(int page, int pageSize, string keyword = null)
		{
			using (var bl = new ParametersBL())
			{
				return Ok(await bl.PageAsync(page, pageSize, keyword, ContextUserId));
			}
		}

		/// <summary>Parameter Detail</summary>
		/// <param name="id">Record identification</param>
		/// <returns></returns>
		[HttpGet]
		[Route("parameters/{id}")]
		public async Task<IHttpActionResult> ParametersDetail(int id)
		{
			using (var bl = new ParametersBL())
			{
				return Ok(await bl.DetailAsync(id, ContextUserId));
			}
		}

		/// <summary>Update an existing Parameter</summary>
		/// <param name="model">Object with all the required data for updating a record</param>
		/// <returns></returns>
		[HttpPut]
		[Route("parameters")]
		public async Task<IHttpActionResult> ParametersUpdate(ParameterVM model)
		{
			using (var bl = new ParametersBL())
			{
				return Ok(await bl.UpdateAsync(model, ContextUserId));
			}
		}

		#endregion

		#region Tasks
		
		/// <summary>Internal Tasks Id List</summary>
		/// <returns></returns>
		[HttpGet]
		[Route("tasks/list")]
		public async Task<IHttpActionResult> TasksList()
		{
			using (var bl = new TasksBL())
			{
				return Ok(await bl.ListAsync(ContextUserId));
			}
		}

		#endregion
	}
}