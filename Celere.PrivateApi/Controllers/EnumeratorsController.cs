﻿using Asteria.API.Controllers;
using Asteria.Core.Tools;
using log4net;
using System.Web.Http;
using static Celere.Business.Enumerators;

namespace Celere.PrivateApi.Controllers
{
	/// <summary></summary>
	[RoutePrefix("v1/enumerators")]
	public class EnumeratorsController : BaseApiController
	{
		#region Constructor

		/// <summary></summary>
		public EnumeratorsController()
		{
			logger = LogManager.GetLogger("Celere.PrivateApi.Controllers.EnumeratorsController");
		}

		#endregion
		
		/// <summary>Log Source</summary>
		[HttpGet]
		[Route("logsource")]
		public IHttpActionResult LogSource()
		{
			return Ok(Utilities.GetDescriptionList<EnumLogSource>());
		}

		/// <summary>Supported Languages</summary>
		[HttpGet]
		[Route("language")]
		public IHttpActionResult Language()
		{
			return Ok(Utilities.GetDescriptionList<EnumLanguage>());
		}
	}
}