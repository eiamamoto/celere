﻿using Asteria.API.Controllers;
using Asteria.Business.Model.Auth;
using log4net;
using System.Threading.Tasks;
using System.Web.Http;
using Celere.Business;
using Celere.PrivateApi.App_Start;

namespace Celere.PrivateApi.Controllers
{
	/// <summary></summary>
	[RoutePrefix("v1/auth")]
	public class AuthController : BaseApiController
	{
		#region Constructor

		/// <summary></summary>
		public AuthController()
		{
			logger = LogManager.GetLogger("Celere.PrivateApi.Controllers.AuthController");
		}

		#endregion

		#region Public Methods

		/// <summary>General authentication method for the internal users</summary>
		/// <param name="model">Object containing the intended email and password</param>
		/// <returns>Token containing the credentials or a http error in case of failure to authenticate</returns>
		[AllowAnonymous]
		[HttpPost]
		[Route("signin")]
		public async Task<IHttpActionResult> SignIn(SignInVM model)
		{
			using (var bl = new UserBL())
			{
				var data = await bl.AuthenticateAsync(model.Email, model.Password);

				object auth;

				var token = AuthHandler.CreateToken(data, out auth);

				return Ok(new { auth, token });
			}
		}

		/// <summary></summary>
		/// <param name="model"></param>
		/// <returns></returns>
		[AllowAnonymous]
		[HttpPost]
		[Route("forgot-password")]
		public async Task<IHttpActionResult> ForgotPassword(ForgotPasswordVM model)
		{
			using (var bl = new UserBL())
			{
				return Ok(await bl.ForgotPasswordAsync(model.Email));
			}
		}

		/// <summary></summary>
		/// <param name="model"></param>
		/// <returns></returns>
		[AllowAnonymous]
		[HttpPost]
		[Route("validate-password-reset-token")]
		public async Task<IHttpActionResult> ValidatePasswordResetToken(PasswordResetValidationVM model)
		{
			using (var bl = new UserBL())
			{
				await bl.ValidatePasswordResetTokenAsync(model);

				return Ok(true);
			}
		}

		/// <summary></summary>
		/// <param name="model"></param>
		/// <returns></returns>
		[AllowAnonymous]
		[HttpPost]
		[Route("password-reset")]
		public async Task<IHttpActionResult> PasswordReset(PasswordResetVM model)
		{
			using (var bl = new UserBL())
			{
				await bl.PasswordResetAsync(model);

				return Ok(true);
			}
		}

		#endregion
	}
}