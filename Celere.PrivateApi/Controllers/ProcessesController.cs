﻿using Asteria.API.Controllers;
using Asteria.API.Models;
using Asteria.Core;
using Asteria.Core.Settings;
using log4net;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Celere.Business;
using Celere.Business.Model;
using Celere.Business.Model.Profiles;
using Celere.Business.Parser;

namespace Celere.PrivateApi.Controllers
{
	/// <summary></summary>
	[RoutePrefix("v1/processes")]
	public class ProcessesController : BaseApiController
	{
        #region Constructor

        /// <summary></summary>
        public ProcessesController()
        {
            logger = LogManager.GetLogger("Celere.PrivateApi.Controllers.ProcessesController");
        }

        #endregion Constructor

		/// <summary>Processes List</summary>
		/// <param name="page">Page number</param>
		/// <param name="pageSize">Size of the page</param>
		/// <param name="keyword">Filter keyword</param>
		/// <returns></returns>
		[HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> Page(int page, int pageSize, string keyword = null)
        {
            using (var bl = new ProcessesBL())
            {
                return Ok(await bl.PageAsync(page, pageSize, keyword, ContextUserId));
            }
        }

		/// <summary>Processes Id List</summary>
		/// <returns></returns>
		[HttpGet]
		[Route("list/ids")]
		public async Task<IHttpActionResult> ListIds()
		{
			using (var bl = new ProcessesBL())
			{
				return Ok(await bl.ListIdsAsync(ContextUserId));
			}
		}


        /// <summary>List of processes of an CPF/CNPJ</summary>
        /// <returns></returns>
        [HttpGet]
        [Route("list/cpfcnpj")]
        public async Task<IHttpActionResult> ListOfCpfCnpj(string CpfCnpj)
        {
            using (var bl = new ProcessesBL())
            {
                return Ok(await bl.ListOfCpfCnpjAsync(CpfCnpj, ContextUserId));
            }
        }



        /// <summary>Process Detail</summary>
        /// <param name="id">Record identification</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        public async Task<IHttpActionResult> Detail(int id)
        {
            using (var bl = new ProcessesBL())
            {
				return Ok(await bl.DetailAsync(id, ContextUserId));
            }
        }

        /// <summary>Insert or update an existing Process</summary>
        /// <param name="model">Object with all the required data for updating a record</param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        public async Task<IHttpActionResult> Upsert(ProcessVM model)
        {
        	using (var bl = new ProcessesBL())
			{
				return Ok(await bl.UpsertAsync(model, ContextUserId));
			}
		}

		/// <summary>Delete (or inactivate) the given Process</summary>
		/// <param name="id">Record identification</param>
		/// <returns></returns>
		[HttpDelete]
		[Route("{id}")]
		public async Task<IHttpActionResult> Delete(int id)
		{
			using (var bl = new ProcessesBL())
			{
				await bl.DeleteAsync(id, ContextUserId);

				return Ok(true);
			}
		}


        [Route("parser/processeslist")]
        [HttpGet]
        public async Task<IHttpActionResult> ProcessesList(string cpf)
        {
            using (var bl = new ParserBL())
            {
                return Ok(await bl.SearchAsync<TJSPProcessItemVM>(new TJSPSearchFilterVM()
                {
                    Cpf = cpf
                }));
            }
        }

        /// <summary>Detail</summary>
        /// <param name="model">Search Parameters</param>
        [Route("parser/detail")]
        [HttpPost]
        public async Task<IHttpActionResult> Detail(TJSPProcessItemVM model)
        {
            using (var bl = new ParserBL())
            {
                return Ok(await bl.DetailAsync<TJSPDetailVM>(model));
            }
        }




    }
}