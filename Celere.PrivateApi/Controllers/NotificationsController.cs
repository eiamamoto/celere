﻿using Asteria.API.Controllers;
using Asteria.API.Models;
using Asteria.Core;
using Asteria.Core.Settings;
using log4net;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Celere.Business;
using Celere.Business.Model;

namespace Celere.PrivateApi.Controllers
{
	/// <summary></summary>
	[RoutePrefix("v1/notifications")]
	public class NotificationsController : BaseApiController
	{
        #region Constructor

        /// <summary></summary>
        public NotificationsController()
        {
            logger = LogManager.GetLogger("Celere.PrivateApi.Controllers.NotificationsController");
        }

        #endregion Constructor

		/// <summary>Notifications List</summary>
		/// <param name="page">Page number</param>
		/// <param name="pageSize">Size of the page</param>
		/// <param name="keyword">Filter keyword</param>
		/// <returns></returns>
		[HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> Page(int page, int pageSize, string keyword = null)
        {
            using (var bl = new NotificationsBL())
            {
                return Ok(await bl.PageAsync(page, pageSize, keyword, ContextUserId));
            }
        }

		/// <summary>Notifications Id List</summary>
		/// <returns></returns>
		[HttpGet]
		[Route("list/ids")]
		public async Task<IHttpActionResult> ListIds()
		{
			using (var bl = new NotificationsBL())
			{
				return Ok(await bl.ListIdsAsync(ContextUserId));
			}
		}

        /// <summary>Notification Detail</summary>
        /// <param name="id">Record identification</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        public async Task<IHttpActionResult> Detail(int id)
        {
            using (var bl = new NotificationsBL())
            {
				return Ok(await bl.DetailAsync(id, ContextUserId));
            }
        }

        /// <summary>Insert or update an existing Notification</summary>
        /// <param name="model">Object with all the required data for updating a record</param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        public async Task<IHttpActionResult> Upsert(NotificationVM model)
        {
        	using (var bl = new NotificationsBL())
			{
				return Ok(await bl.UpsertAsync(model, ContextUserId));
			}
		}

		/// <summary>Delete (or inactivate) the given Notification</summary>
		/// <param name="id">Record identification</param>
		/// <returns></returns>
		[HttpDelete]
		[Route("{id}")]
		public async Task<IHttpActionResult> Delete(int id)
		{
			using (var bl = new NotificationsBL())
			{
				await bl.DeleteAsync(id, ContextUserId);

				return Ok(true);
			}
		}

        /// <summary>Create notifications that were not created yet</summary>
        [HttpGet]
        [Route("Check")]
        public async Task<IHttpActionResult> Check()
        {
            using (var bl = new NotificationsBL())
            {
                await bl.CheckAsync(ContextUserId);

                return Ok(true);
            }
        }

    }
}