﻿using Asteria.API.Controllers;
using Asteria.API.Models;
using Asteria.Core;
using Asteria.Core.Settings;
using log4net;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Celere.Business;
using Celere.Business.Model;

namespace Celere.PrivateApi.Controllers
{
	/// <summary></summary>
	[RoutePrefix("v1/schedulercreations")]
	public class SchedulerCreationsController : BaseApiController
	{
        #region Constructor

        /// <summary></summary>
        public SchedulerCreationsController()
        {
            logger = LogManager.GetLogger("Celere.PrivateApi.Controllers.SchedulerCreationsController");
        }

        #endregion Constructor

		/// <summary>SchedulerCreations List</summary>
		/// <param name="page">Page number</param>
		/// <param name="pageSize">Size of the page</param>
		/// <param name="keyword">Filter keyword</param>
		/// <returns></returns>
		[HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> Page(int page, int pageSize, string keyword = null)
        {
            using (var bl = new SchedulerCreationsBL())
            {
                return Ok(await bl.PageAsync(page, pageSize, keyword, ContextUserId));
            }
        }

		/// <summary>SchedulerCreations Id List</summary>
		/// <returns></returns>
		[HttpGet]
		[Route("list/ids")]
		public async Task<IHttpActionResult> ListIds()
		{
			using (var bl = new SchedulerCreationsBL())
			{
				return Ok(await bl.ListIdsAsync(ContextUserId));
			}
		}

        /// <summary>Generates the list of CPF/CNPJs to be monitorated at the present day</summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Generate")]
        public async Task<IHttpActionResult> Generate()
        {
            using (var bl = new SchedulerCreationsBL())
            {
                await bl.GenerateAsync(ContextUserId);

                return Ok(true);
            }
        }



        /// <summary>SchedulerCreation Detail</summary>
        /// <param name="id">Record identification</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        public async Task<IHttpActionResult> Detail(int id)
        {
            using (var bl = new SchedulerCreationsBL())
            {
				return Ok(await bl.DetailAsync(id, ContextUserId));
            }
        }

        /// <summary>Insert or update an existing SchedulerCreation</summary>
        /// <param name="model">Object with all the required data for updating a record</param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        public async Task<IHttpActionResult> Upsert(SchedulerCreationVM model)
        {
        	using (var bl = new SchedulerCreationsBL())
			{
				return Ok(await bl.UpsertAsync(model, ContextUserId));
			}
		}

		/// <summary>Delete (or inactivate) the given SchedulerCreation</summary>
		/// <param name="id">Record identification</param>
		/// <returns></returns>
		[HttpDelete]
		[Route("{id}")]
		public async Task<IHttpActionResult> Delete(int id)
		{
			using (var bl = new SchedulerCreationsBL())
			{
				await bl.DeleteAsync(id, ContextUserId);

				return Ok(true);
			}
		}

	}
}