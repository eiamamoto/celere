﻿
using System.Web.Mvc;

namespace Celere.PrivateApi.Controllers
{
	/// <summary></summary>
	public class DefaultController : Controller
	{
		/// <summary></summary>
		/// <returns></returns>
		public ActionResult Index()
		{
			return Redirect("~/swagger");
		}
	}
}