﻿(function () {
	$(function () {
		if ($('#input_email').length && $('#input_password').length) {
			return;
		}

		var basicAuthUI =
            '<div class="input"><input placeholder="email" id="input_email" name="email" type="email" size="10"></div>' +
            '<div class="input"><input placeholder="password" id="input_password" name="password" type="password" size="10"></div>';

		$(basicAuthUI).insertBefore('#api_selector div.input:last-child');

		$("#input_apiKey").hide();

		$('#input_email').change(addAuthorization);
		$('#input_password').change(addAuthorization);
	});

	function addAuthorization() {
		var email = $('#input_email').val();
		var password = $('#input_password').val();

		if (email && email.trim() != "" && password && password.trim() != "") {

			$.ajax('/v1/auth/signin', {
				type: 'POST',
				data: {
					email: email,
					password: password
				},
				success: function (data) {
					var apiKeyAuth = new SwaggerClient.ApiKeyAuthorization('Authorization', 'Bearer ' + data.token, 'header');

					window.swaggerUi.api.clientAuthorizations.add('bearer', apiKeyAuth);

					console.log('Set bearer token: ' + data.token);
				},
				error: function (xhr, textStatus, error) {
					alert(xhr.statusText);
				}
			});
		}
	}
})();