﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;

namespace Celere.PrivateApi
{
	/// <summary></summary>
	public class Global : HttpApplication
    {
        private void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            log4net.Config.XmlConfigurator.Configure();
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
		}

		private void Application_BeginRequest(object sender, EventArgs e)
		{
			var application = sender as HttpApplication;

			if (application != null && application.Context != null)
			{
				application.Context.Response.Headers.Remove("Server");
			}
		}
	}
}