﻿
namespace Celere.Business.Tools
{
	public static class Utilities
	{

		#region String


		public static string RemoveLineEndings(this string value)
		{
			if (string.IsNullOrEmpty(value))
			{
				return value;
			}
			string lineSeparator = ((char)0x2028).ToString();
			string paragraphSeparator = ((char)0x2029).ToString();

			return value.Replace("\r\n", string.Empty)
						.Replace("\n", string.Empty)
						.Replace("\r", string.Empty)
						.Replace("\t", string.Empty)
						.Replace(lineSeparator, string.Empty)
						.Replace(paragraphSeparator, string.Empty);
		}

		#endregion
	}
}