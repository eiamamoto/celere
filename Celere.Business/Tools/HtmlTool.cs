﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using Asteria.Core;
//using Asteria.Core.Settings;
using HtmlAgilityPack;
using System.Configuration;
using System.Collections.Specialized;

namespace Celere.Business.Tools
{
	public static class HtmlTool
	{
		#region WebClients

		public class CustomWebClient : WebClient
		{
			Uri responseUri;

			public Uri ResponseUri
			{
				get { return responseUri; }
			}

			protected override WebResponse GetWebResponse(WebRequest request)
			{
				WebResponse response = null;
				response = base.GetWebResponse(request);
				responseUri = response.ResponseUri;
				return response;
			}
		}

		public class GZipWebClient : CustomWebClient
		{
			protected override WebRequest GetWebRequest(Uri address)
			{
				HttpWebRequest request = (HttpWebRequest)base.GetWebRequest(address);
				request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
				return request;
			}
		}

		#endregion

		#region Public

		public static HtmlDocument DownloadHtml(string targetUrl,
			out string htmlStringResult, out string ResponsetargetUrl)
		{
			return DownloadHtml(targetUrl, decodeUrl: false, queryStringParameters: null,
				headers: null, gzipCompression: false,
				htmlStringResult: out htmlStringResult, ResponsetargetUrl: out ResponsetargetUrl);
		}

		public static HtmlDocument DownloadHtml(string targetUrl, Dictionary<string, string> queryStringParameters,
			out string htmlStringResult, out string ResponsetargetUrl)
		{
			return DownloadHtml(targetUrl, decodeUrl: false, queryStringParameters: queryStringParameters,
				headers: null, gzipCompression: false,
				htmlStringResult: out htmlStringResult, ResponsetargetUrl: out ResponsetargetUrl);
		}

		#endregion

		#region HTML Get

		private static HtmlDocument DownloadHtml(string targetUrl, bool decodeUrl,
			Dictionary<string, string> queryStringParameters,
			Dictionary<HttpRequestHeader, string> headers, bool gzipCompression,
			out string htmlStringResult, out string ResponsetargetUrl)
		{
			using (var client = gzipCompression ? new GZipWebClient() : new CustomWebClient())
			{
                NameValueCollection globalSettings = (NameValueCollection)ConfigurationManager.GetSection("globalSettings");

                client.Encoding = Encoding.UTF8;
				client.Headers.Add(HttpRequestHeader.UserAgent, Convert.ToString(globalSettings["HtmlTool_UserAgent"]));
				
				if (headers != null && headers.Count > 0)
				{
					foreach (var kv in headers)
					{
						client.Headers.Add(kv.Key, kv.Value);
					}
				}

				try
				{
					if (queryStringParameters != null && queryStringParameters.Count > 0)
					{
						targetUrl = 
							string.Format("{0}?{1}",
								targetUrl,
								string.Join("&", queryStringParameters.Select(kvp =>
									string.Format("{0}={1}", kvp.Key, HttpUtility.UrlEncode(kvp.Value)))));
					}


					htmlStringResult = decodeUrl ?
						HttpUtility.HtmlDecode(client.DownloadString(targetUrl)) :
						client.DownloadString(targetUrl);

					var doc = new HtmlDocument();
					doc.OptionCheckSyntax = false;
					doc.LoadHtml(htmlStringResult);

					ResponsetargetUrl = client.ResponseUri.ToString();

					return doc;
				}
				catch (Exception EX)
				{
					if (EX.Message == "The server committed a protocol violation. Section=ResponseHeader Detail=CR must be followed by LF")
					{
						SetAllowUnsafeHeaderParsing20();
						return DownloadHtml(targetUrl, decodeUrl, queryStringParameters, headers, gzipCompression,
							out htmlStringResult, out ResponsetargetUrl);
					}
					else
					{

						throw EX;

					}
				}
			}
		}

		#endregion

		//https://social.msdn.microsoft.com/Forums/en-US/ff098248-551c-4da9-8ba5-358a9f8ccc57/how-do-i-enable-useunsafeheaderparsing-from-code-net-20
		private static bool SetAllowUnsafeHeaderParsing20()
		{
			//Get the assembly that contains the internal class
			Assembly aNetAssembly = Assembly.GetAssembly(typeof(System.Net.Configuration.SettingsSection));
			if (aNetAssembly != null)
			{
				//Use the assembly in order to get the internal type for the internal class
				Type aSettingsType = aNetAssembly.GetType("System.Net.Configuration.SettingsSectionInternal");
				if (aSettingsType != null)
				{
					//Use the internal static property to get an instance of the internal settings class.
					//If the static instance isn't created allready the property will create it for us.
					object anInstance = aSettingsType.InvokeMember("Section",
					  BindingFlags.Static | BindingFlags.GetProperty | BindingFlags.NonPublic, null, null, new object[] { });

					if (anInstance != null)
					{
						//Locate the private bool field that tells the framework is unsafe header parsing should be allowed or not
						FieldInfo aUseUnsafeHeaderParsing = aSettingsType.GetField("useUnsafeHeaderParsing", BindingFlags.NonPublic | BindingFlags.Instance);
						if (aUseUnsafeHeaderParsing != null)
						{
							aUseUnsafeHeaderParsing.SetValue(anInstance, true);
							return true;
						}
					}
				}
			}
			return false;
		}
	}
}