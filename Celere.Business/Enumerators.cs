﻿using System.ComponentModel;

namespace Celere.Business
{
	public static class Enumerators
	{
		public enum EnumTask
		{
			[Description("Acesso ao Painel de Controle")]
			ControlPanelAccess = 1,

			[Description("Gerenciar Usuários")]
			ManageUsers = 2,

			[Description("Gerenciar Grupos de Usuários")]
			ManageGroups = 3,

			[Description("Gerenciar Logs")]
			ManageLogs = 4,

			[Description("Gerenciar Parâmetros")]
			ManageParameters = 5,

			[Description("Gerenciar Tarefas")]
			ManageTasks = 6,

			[Description("Gerenciar Addresses")]
			ManageAddresses = 7,

			[Description("Gerenciar Attempts")]
			ManageAttempts = 8,

			[Description("Gerenciar Cities")]
			ManageCities = 9,

			[Description("Gerenciar Customers")]
			ManageCustomers = 10,

			[Description("Gerenciar LogTrackingCodes")]
			ManageLogTrackingCodes = 11,

			[Description("Gerenciar Movements")]
			ManageMovements = 12,

			[Description("Gerenciar Notifications")]
			ManageNotifications = 13,

			[Description("Gerenciar NotificationTypes")]
			ManageNotificationTypes = 14,

			[Description("Gerenciar OperationTypes")]
			ManageOperationTypes = 15,

			[Description("Gerenciar Parts")]
			ManageParts = 16,

			[Description("Gerenciar Plans")]
			ManagePlans = 17,

			[Description("Gerenciar Processes")]
			ManageProcesses = 18,

			[Description("Gerenciar SchedulerCreations")]
			ManageSchedulerCreations = 19,

			[Description("Gerenciar SchedulerStatus")]
			ManageSchedulerStatus = 20,

			[Description("Gerenciar SchedulerTrackingCodes")]
			ManageSchedulerTrackingCodes = 21,

			[Description("Gerenciar States")]
			ManageStates = 22,

			[Description("Gerenciar TrackingCodes")]
			ManageTrackingCodes = 23

		}

		public enum EnumLogSource
		{
			[Description("Private Api")]
			PrivateApi = 1,

			[Description("Public Api")]
			PublicApi = 2,

			[Description("Control Panel")]
			ControlPanel = 3,

			[Description("Site")]
			Site = 4
		}

		public enum EnumLanguage
		{
			[Description("pt-BR")]
			ptBR = 1
		}
	}
}