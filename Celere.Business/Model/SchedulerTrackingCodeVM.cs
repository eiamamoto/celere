﻿using Newtonsoft.Json;
using System;

namespace Celere.Business.Model
{
    public class SchedulerTrackingCodeVM
    {
		/// <summary>SchedulerId</summary>
        [JsonProperty("schedulerId")]
		public int SchedulerId { get; set; }

		/// <summary>DtReg</summary>
        [JsonProperty("dtReg")]
		public DateTime? DtReg { get; set; }

		/// <summary>CpfCnpj</summary>
        [JsonProperty("cpfCnpj")]
		public string CpfCnpj { get; set; }

		/// <summary>SchedulerStatusId</summary>
        [JsonProperty("schedulerStatusId")]
		public int? SchedulerStatusId { get; set; }

		/// <summary>DtProcessed</summary>
        [JsonProperty("dtProcessed")]
		public DateTime? DtProcessed { get; set; }

		/// <summary>ServerChecking</summary>
        [JsonProperty("serverChecking")]
		public string ServerChecking { get; set; }

		/// <summary>DtStart</summary>
        [JsonProperty("dtStart")]
		public DateTime? DtStart { get; set; }

    }
}