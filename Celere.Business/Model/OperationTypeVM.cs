﻿using Newtonsoft.Json;
using System;

namespace Celere.Business.Model
{
    public class OperationTypeVM
    {
		/// <summary>OperationTypeId</summary>
        [JsonProperty("operationTypeId")]
		public int OperationTypeId { get; set; }

		/// <summary>Name</summary>
        [JsonProperty("name")]
		public string Name { get; set; }

    }
}