﻿using Newtonsoft.Json;
using System;
using static Celere.Business.Enumerators;

namespace Celere.Business.Model.Internal
{
	public class LogVM
	{
		/// <summary>LogId</summary>
        [JsonProperty("logId")]
		public int LogId { get; set; }

		/// <summary>Date</summary>
        [JsonProperty("date")]
		public DateTime Date { get; set; }

		/// <summary>Thread</summary>
        [JsonProperty("thread")]
		public string Thread { get; set; }

		/// <summary>Level</summary>
        [JsonProperty("level")]
		public string Level { get; set; }

		/// <summary>Logger</summary>
        [JsonProperty("logger")]
		public string Logger { get; set; }

		/// <summary>Message</summary>
        [JsonProperty("message")]
		public string Message { get; set; }

		/// <summary>Exception</summary>
        [JsonProperty("exception")]
		public string Exception { get; set; }

		/// <summary>LogSource</summary>
        [JsonProperty("enumLogSource")]
		public EnumLogSource EnumLogSource { get; set; }

    }
}