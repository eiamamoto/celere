﻿using Newtonsoft.Json;
using System;

namespace Celere.Business.Model
{
    public class DashboardVM
	{
		/// <summary>Addresses</summary>
		[JsonProperty("addresses")]
		public int Addresses { get; set; }

		/// <summary>Attempts</summary>
		[JsonProperty("attempts")]
		public int Attempts { get; set; }

		/// <summary>Cities</summary>
		[JsonProperty("cities")]
		public int Cities { get; set; }

		/// <summary>Customers</summary>
		[JsonProperty("customers")]
		public int Customers { get; set; }

		/// <summary>LogTrackingCodes</summary>
		[JsonProperty("logtrackingcodes")]
		public int LogTrackingCodes { get; set; }

		/// <summary>Movements</summary>
		[JsonProperty("movements")]
		public int Movements { get; set; }

		/// <summary>Notifications</summary>
		[JsonProperty("notifications")]
		public int Notifications { get; set; }

		/// <summary>NotificationTypes</summary>
		[JsonProperty("notificationtypes")]
		public int NotificationTypes { get; set; }

		/// <summary>OperationTypes</summary>
		[JsonProperty("operationtypes")]
		public int OperationTypes { get; set; }

		/// <summary>Parts</summary>
		[JsonProperty("parts")]
		public int Parts { get; set; }

		/// <summary>Plans</summary>
		[JsonProperty("plans")]
		public int Plans { get; set; }

		/// <summary>Processes</summary>
		[JsonProperty("processes")]
		public int Processes { get; set; }

		/// <summary>SchedulerCreations</summary>
		[JsonProperty("schedulercreations")]
		public int SchedulerCreations { get; set; }

		/// <summary>SchedulerStatus</summary>
		[JsonProperty("schedulerstatus")]
		public int SchedulerStatus { get; set; }

		/// <summary>SchedulerTrackingCodes</summary>
		[JsonProperty("schedulertrackingcodes")]
		public int SchedulerTrackingCodes { get; set; }

		/// <summary>States</summary>
		[JsonProperty("states")]
		public int States { get; set; }

		/// <summary>TrackingCodes</summary>
		[JsonProperty("trackingcodes")]
		public int TrackingCodes { get; set; }

	}
}