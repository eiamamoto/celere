﻿using Newtonsoft.Json;

namespace Celere.Business.Model.Internal
{
	public class ParameterVM
	{
		/// <summary>ParameterId</summary>
        [JsonProperty("parameterId")]
		public int ParameterId { get; set; }

		/// <summary>Key</summary>
        [JsonProperty("key")]
		public string Key { get; set; }

		/// <summary>Value</summary>
        [JsonProperty("value")]
		public string Value { get; set; }

		/// <summary>ValueDev</summary>
        [JsonProperty("valueDev")]
		public string ValueDev { get; set; }

		/// <summary>ValueQA</summary>
        [JsonProperty("valueQA")]
		public string ValueQA { get; set; }

		/// <summary>Description</summary>
        [JsonProperty("description")]
		public string Description { get; set; }

		/// <summary>ReadOnly</summary>
        [JsonProperty("isReadOnly")]
		public bool IsReadOnly { get; set; }

    }
}