﻿using Newtonsoft.Json;
using static Celere.Business.Enumerators;

namespace Celere.Business.Model.Internal
{
    public class TaskVM
	{
		/// <summary>TaskId</summary>
        [JsonProperty("taskId")]
		public int TaskId { get; set; }

		/// <summary>Name</summary>
        [JsonProperty("name")]
		public string Name { get; set; }

		/// <summary>Description</summary>
        [JsonProperty("description")]
		public string Description { get; set; }

		/// <summary>Task</summary>
        [JsonProperty("enumTask")]
		public EnumTask EnumTask { get; set; }

    }
}