﻿using Newtonsoft.Json;

namespace Celere.Business.Model.Internal
{
	public class GroupVM
	{
		/// <summary>GroupId</summary>
        [JsonProperty("groupId")]
		public int GroupId { get; set; }

		/// <summary>Name</summary>
        [JsonProperty("name")]
		public string Name { get; set; }

		/// <summary>Active</summary>
        [JsonProperty("isActive")]
		public bool IsActive { get; set; }

    }
}