﻿using Asteria.Business.Model.Auth;
using Newtonsoft.Json;
using System.Collections.Generic;
using static Celere.Business.Enumerators;

namespace Celere.Business.Model.Internal
{
	public class UserVM : AuthVM
	{
		/// <summary>Name</summary>
		[JsonProperty("name")]
		public string Name { get; set; }

		/// <summary>New Password</summary>
		[JsonProperty("newPassword")]
		public string NewPassword { get; set; }

		/// <summary>Password Confirmation</summary>
		[JsonProperty("passwordConfirmation")]
		public string PasswordConfirmation { get; set; }

		/// <summary>Avatar</summary>
		[JsonProperty("imageAvatar")]
		public string ImageAvatar { get; set; }

		/// <summary>Permissions</summary>
		[JsonProperty("permissions")]
		public IEnumerable<EnumTask> Permissions { get; set; }

		/// <summary>Language</summary>
		[JsonProperty("enumLanguage")]
		public EnumLanguage EnumLanguage { get; set; }

		/// <summary>Is User Active?</summary>
		[JsonProperty("isUserActive")]
		public bool IsUserActive { get; set; }
	}
}