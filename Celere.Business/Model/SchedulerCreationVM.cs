﻿using Newtonsoft.Json;
using System;

namespace Celere.Business.Model
{
    public class SchedulerCreationVM
    {
		/// <summary>SchedulerCreationId</summary>
        [JsonProperty("schedulerCreationId")]
		public int SchedulerCreationId { get; set; }

		/// <summary>DtReg</summary>
        [JsonProperty("dtReg")]
		public DateTime? DtReg { get; set; }

    }
}