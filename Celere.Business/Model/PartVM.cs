﻿using Newtonsoft.Json;
using System;

namespace Celere.Business.Model
{
    public class PartVM
    {
		/// <summary>PartId</summary>
        [JsonProperty("partId")]
		public int PartId { get; set; }

		/// <summary>DtReg</summary>
        [JsonProperty("dtReg")]
		public DateTime? DtReg { get; set; }

		/// <summary>ProcessId</summary>
        [JsonProperty("processId")]
		public string ProcessId { get; set; }

		/// <summary>Name</summary>
        [JsonProperty("name")]
		public string Name { get; set; }

    }
}