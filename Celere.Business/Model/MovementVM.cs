﻿using Newtonsoft.Json;
using System;

namespace Celere.Business.Model
{
    public class MovementVM
    {
		/// <summary>MovementId</summary>
        [JsonProperty("movementId")]
		public int MovementId { get; set; }

		/// <summary>ProcessId</summary>
        [JsonProperty("processId")]
		public string ProcessId { get; set; }

		/// <summary>DtReg</summary>
        [JsonProperty("dtReg")]
		public DateTime? DtReg { get; set; }

		/// <summary>DtMovement</summary>
        [JsonProperty("dtMovement")]
		public DateTime? DtMovement { get; set; }

		/// <summary>Vara</summary>
        [JsonProperty("vara")]
		public string Vara { get; set; }

    }
}