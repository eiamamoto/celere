﻿using Newtonsoft.Json;
using System;

namespace Celere.Business.Model
{
    public class AddressVM
    {
		/// <summary>AddressId</summary>
        [JsonProperty("addressId")]
		public int AddressId { get; set; }

		/// <summary>Address1</summary>
        [JsonProperty("address1")]
		public string Address1 { get; set; }

		/// <summary>Address2</summary>
        [JsonProperty("address2")]
		public string Address2 { get; set; }

		/// <summary>Neighborhood</summary>
        [JsonProperty("neighborhood")]
		public string Neighborhood { get; set; }

		/// <summary>ZipCode</summary>
        [JsonProperty("zipCode")]
		public string ZipCode { get; set; }

		/// <summary>CityId</summary>
        [JsonProperty("cityId")]
		public int CityId { get; set; }

		/// <summary>Lat</summary>
        [JsonProperty("lat")]
		public decimal Lat { get; set; }

		/// <summary>Lng</summary>
        [JsonProperty("lng")]
		public decimal Lng { get; set; }

		/// <summary>Active</summary>
        [JsonProperty("isActive")]
		public bool IsActive { get; set; }

		/// <summary>Number</summary>
        [JsonProperty("number")]
		public string Number { get; set; }

    }
}