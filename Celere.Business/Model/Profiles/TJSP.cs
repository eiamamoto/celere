﻿using Newtonsoft.Json;
using Celere.Business.Model.Custom;

namespace Celere.Business.Model.Profiles
{
	public class TJSPSearchFilterVM : ISearchFilter
	{
		[JsonIgnore]
		public string ProfileName { get; set; }

		[JsonProperty("cpf")]
		public string Cpf { get; set; }

		public TJSPSearchFilterVM()
		{
			this.ProfileName = "TJSP";
		}
	}

	public class TJSPProcessItemVM : ISearchItem
	{
		[JsonIgnore]
		public string ProfileName { get; set; }

		[JsonProperty("processNumber")]
		public string ProcessNumber { get; set; }

		[JsonProperty("processUrl")]
		public string ProcessUrl { get; set; }

		public TJSPProcessItemVM()
		{
			this.ProfileName = "TJSP";
		}
	}

	public class TJSPDetailVM
    {
		[JsonProperty("processNumber")]
		public string ProcessNumber { get; set; }

		[JsonProperty("processData")]
		public string ProcessData { get; set; }

        [JsonProperty("processValue")]
        public string ProcessValue { get; set; }

        [JsonProperty("vara")]
        public string Vara { get; set; }

        [JsonProperty("distribution")]
        public string Distribution { get; set; }

        [JsonProperty("peopleInvolved")]
		public string PeopleInvolved { get; set; }




	}
}