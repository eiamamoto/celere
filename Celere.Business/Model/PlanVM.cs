﻿using Newtonsoft.Json;
using System;

namespace Celere.Business.Model
{
    public class PlanVM
    {
		/// <summary>PlanId</summary>
        [JsonProperty("planId")]
		public int PlanId { get; set; }

		/// <summary>DtReg</summary>
        [JsonProperty("dtReg")]
		public DateTime? DtReg { get; set; }

		/// <summary>Quantity</summary>
        [JsonProperty("quantity")]
		public int? Quantity { get; set; }

		/// <summary>PlanName</summary>
        [JsonProperty("planName")]
		public string PlanName { get; set; }

		/// <summary>Active</summary>
        [JsonProperty("isActive")]
		public bool? IsActive { get; set; }

		/// <summary>Cost</summary>
        [JsonProperty("cost")]
		public decimal? Cost { get; set; }

		/// <summary>CostExtra</summary>
        [JsonProperty("costExtra")]
		public decimal? CostExtra { get; set; }

    }
}