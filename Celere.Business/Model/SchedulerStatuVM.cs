﻿using Newtonsoft.Json;
using System;

namespace Celere.Business.Model
{
    public class SchedulerStatuVM
    {
		/// <summary>SchedulerStatusId</summary>
        [JsonProperty("schedulerStatusId")]
		public int SchedulerStatusId { get; set; }

		/// <summary>Status</summary>
        [JsonProperty("status")]
		public string Status { get; set; }

    }
}