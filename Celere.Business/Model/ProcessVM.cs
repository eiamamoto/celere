﻿using Newtonsoft.Json;
using System;

namespace Celere.Business.Model
{
    public class ProcessVM
    {
		/// <summary>ProcessId</summary>
        [JsonProperty("processId")]
		public string ProcessId { get; set; }

		/// <summary>DtReg</summary>
        [JsonProperty("dtReg")]
		public DateTime? DtReg { get; set; }

		/// <summary>DtDistribution</summary>
        [JsonProperty("dtDistribution")]
		public DateTime? DtDistribution { get; set; }

		/// <summary>Value</summary>
        [JsonProperty("value")]
		public decimal? Value { get; set; }

		/// <summary>Vara</summary>
        [JsonProperty("vara")]
		public string Vara { get; set; }

		/// <summary>CpfCnpj</summary>
        [JsonProperty("cpfCnpj")]
		public string CpfCnpj { get; set; }

		/// <summary>HasNotifications</summary>
        [JsonProperty("hasNotifications")]
		public bool? HasNotifications { get; set; }

    }
}