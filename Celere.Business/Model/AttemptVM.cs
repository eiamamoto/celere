﻿using Newtonsoft.Json;
using System;

namespace Celere.Business.Model
{
    public class AttemptVM
    {
		/// <summary>AttemptId</summary>
        [JsonProperty("attemptId")]
		public int AttemptId { get; set; }

		/// <summary>NotificationId</summary>
        [JsonProperty("notificationId")]
		public int? NotificationId { get; set; }

		/// <summary>DtReg</summary>
        [JsonProperty("dtReg")]
		public DateTime? DtReg { get; set; }

		/// <summary>Succeded</summary>
        [JsonProperty("succeded")]
		public bool? Succeded { get; set; }

		/// <summary>Message</summary>
        [JsonProperty("message")]
		public string Message { get; set; }

    }
}