﻿using Newtonsoft.Json;
using System;

namespace Celere.Business.Model
{
    public class LogTrackingCodeVM
    {
		/// <summary>LogId</summary>
        [JsonProperty("logId")]
		public int LogId { get; set; }

		/// <summary>DtReg</summary>
        [JsonProperty("dtReg")]
		public DateTime? DtReg { get; set; }

		/// <summary>OperationTypeId</summary>
        [JsonProperty("operationTypeId")]
		public int? OperationTypeId { get; set; }

		/// <summary>TrackingCodeId</summary>
        [JsonProperty("trackingCodeId")]
		public int? TrackingCodeId { get; set; }

    }
}