﻿using Newtonsoft.Json;
using System;

namespace Celere.Business.Model
{
    public class NotificationVM
    {
		/// <summary>NotificationId</summary>
        [JsonProperty("notificationId")]
		public int NotificationId { get; set; }

		/// <summary>DtReg</summary>
        [JsonProperty("dtReg")]
		public DateTime? DtReg { get; set; }

		/// <summary>Succeded</summary>
        [JsonProperty("succeded")]
		public bool? Succeded { get; set; }

		/// <summary>DtSucceded</summary>
        [JsonProperty("dtSucceded")]
		public DateTime? DtSucceded { get; set; }

		/// <summary>ProcessId</summary>
        [JsonProperty("processId")]
		public string ProcessId { get; set; }

		/// <summary>CustomerId</summary>
        [JsonProperty("customerId")]
		public int? CustomerId { get; set; }

		/// <summary>NotificationTypeId</summary>
        [JsonProperty("notificationTypeId")]
		public int? NotificationTypeId { get; set; }

    }
}