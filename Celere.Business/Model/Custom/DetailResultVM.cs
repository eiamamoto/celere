﻿using Newtonsoft.Json;

namespace Celere.Business.Model.Custom
{
	public class DetailResultVM<T>
	{
		[JsonProperty("url")]
		public string Url { get; set; }

		[JsonProperty("process")]
		public T Process { get; set; }
	}
}