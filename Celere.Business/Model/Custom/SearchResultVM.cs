﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Celere.Business.Model.Custom
{
	public class SearchResultVM<T>
	{
		[JsonProperty("total")]
		public int Total { get; set; }

		[JsonProperty("url")]
		public string Url { get; set; }

		[JsonProperty("items")]
		public IEnumerable<T> Items { get; set; }
	}
}