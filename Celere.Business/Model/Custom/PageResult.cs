﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Celere.Business.Model.Custom
{
	public class PageResult<T>
	{
		/// <summary></summary>
		public PageResult() { }

		/// <summary>P�gina atual</summary>
		[JsonProperty("page")]
		public int Page { get; set; }

		/// <summary>Total de p�ginas</summary>
		[JsonProperty("total")]
		public int Total { get; set; }

		/// <summary>Total de registros</summary>
		[JsonProperty("rows")]
		public IEnumerable<T> Rows { get; set; }
	}
}