﻿using Newtonsoft.Json;
using System;

namespace Celere.Business.Model.Reports
{
	public class UserVM
	{
		/// <summary>Name</summary>
		[JsonProperty("name")]
		public string Name { get; set; }

		/// <summary>Email</summary>
		[JsonProperty("email")]
		public string Email { get; set; }

		/// <summary>CreatedOn</summary>
		[JsonProperty("createdOn")]
		public DateTime CreatedOn { get; set; }
	}
}