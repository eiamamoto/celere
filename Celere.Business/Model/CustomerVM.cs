﻿using Newtonsoft.Json;
using System;

namespace Celere.Business.Model
{
    public class CustomerVM
    {
		/// <summary>CustomerId</summary>
        [JsonProperty("customerId")]
		public int CustomerId { get; set; }

		/// <summary>AuthId</summary>
        [JsonProperty("authId")]
		public int AuthId { get; set; }

		/// <summary>DtReg</summary>
        [JsonProperty("dtReg")]
		public DateTime? DtReg { get; set; }

		/// <summary>Name</summary>
        [JsonProperty("name")]
		public string Name { get; set; }

		/// <summary>AddressId</summary>
        [JsonProperty("addressId")]
		public int? AddressId { get; set; }

		/// <summary>Phone</summary>
        [JsonProperty("phone")]
		public string Phone { get; set; }

		/// <summary>Cel</summary>
        [JsonProperty("cel")]
		public string Cel { get; set; }

		/// <summary>CpfCnpj</summary>
        [JsonProperty("cpfCnpj")]
		public string CpfCnpj { get; set; }

    }
}