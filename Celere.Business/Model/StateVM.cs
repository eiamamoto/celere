﻿using Newtonsoft.Json;
using System;

namespace Celere.Business.Model
{
    public class StateVM
    {
		/// <summary>StateId</summary>
        [JsonProperty("stateId")]
		public int StateId { get; set; }

		/// <summary>Name</summary>
        [JsonProperty("name")]
		public string Name { get; set; }

		/// <summary>Abbreviation</summary>
        [JsonProperty("abbreviation")]
		public string Abbreviation { get; set; }

    }
}