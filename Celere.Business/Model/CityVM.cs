﻿using Newtonsoft.Json;
using System;

namespace Celere.Business.Model
{
    public class CityVM
    {
		/// <summary>CityId</summary>
        [JsonProperty("cityId")]
		public int CityId { get; set; }

		/// <summary>Name</summary>
        [JsonProperty("name")]
		public string Name { get; set; }

		/// <summary>StateId</summary>
        [JsonProperty("stateId")]
		public int StateId { get; set; }

		/// <summary>Capital</summary>
        [JsonProperty("isCapital")]
		public bool IsCapital { get; set; }

    }
}