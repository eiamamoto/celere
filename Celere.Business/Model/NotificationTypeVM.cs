﻿using Newtonsoft.Json;
using System;

namespace Celere.Business.Model
{
    public class NotificationTypeVM
    {
		/// <summary>NotificationTypeId</summary>
        [JsonProperty("notificationTypeId")]
		public int NotificationTypeId { get; set; }

		/// <summary>NotificationType</summary>
        [JsonProperty("notificationType")]
		public string NotificationType { get; set; }

    }
}