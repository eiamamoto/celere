﻿using Newtonsoft.Json;
using System;

namespace Celere.Business.Model
{
    public class TrackingCodeVM
    {
		/// <summary>TrackingCodeId</summary>
        [JsonProperty("trackingCodeId")]
		public int TrackingCodeId { get; set; }

		/// <summary>DtReg</summary>
        [JsonProperty("dtReg")]
		public DateTime? DtReg { get; set; }

		/// <summary>DtActivation</summary>
        [JsonProperty("dtActivation")]
		public DateTime? DtActivation { get; set; }

		/// <summary>CpfCnpj</summary>
        [JsonProperty("cpfCnpj")]
		public string CpfCnpj { get; set; }

		/// <summary>Active</summary>
        [JsonProperty("isActive")]
		public bool? IsActive { get; set; }

		/// <summary>CustomerId</summary>
        [JsonProperty("customerId")]
		public int? CustomerId { get; set; }

    }
}