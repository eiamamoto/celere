﻿using Asteria.Business;
using Asteria.Business.Model;
using Asteria.Core.Tools;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;
using Celere.Business.Model;
using static Celere.Business.Enumerators;

namespace Celere.Business
{
	public class PlansBL : BaseBL
	{
		#region Constructor

		private string loggerName = "Celere.Business.PlansBL";
		
		/// <summary></summary>
		public PlansBL() : base()
		{
			ChangeLogger(loggerName);
		}
		
		/// <summary></summary>
		public PlansBL(SqlConnection conn, SqlTransaction transaction) : base(conn, transaction)
		{
			ChangeLogger(loggerName);
		}

		#endregion

		#region Selects

		/// <summary></summary>
		public async Task<PageResult<PlanVM>> PageAsync(int page, int pageSize, string keyword, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManagePlans);

			return await PageAsync<PlanVM>("_spPlansPage", page, new
			{
				Page = (page - 1),
				PageRows = pageSize,
				Keyword = keyword
			});
		}		

		/// <summary></summary>
		public async Task<IEnumerable<KeyValuePair<int, string>>> ListIdsAsync(int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManagePlans);

			return await ListAsync<KeyValuePair<int, string>>("_spPlansListIds");
		}

		/// <summary></summary>
		public async Task<PlanVM> DetailAsync(int id, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManagePlans);

			var model = await SingleAsync<PlanVM>("_spPlansGet", new
			{
				PlanId = id
			});


			return model;
		}

		#endregion

		#region Handles

		/// <summary></summary>
		public async Task<PlanVM> UpsertAsync(PlanVM model, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManagePlans);

				model.PlanId = await SingleAsync<int>("_spPlanUpsert", parameters: new
				{
					PlanId = model.PlanId,
					DtReg = model.DtReg,
					Quantity = model.Quantity,
					PlanName = model.PlanName,
					IsActive = model.IsActive,
					Cost = model.Cost,
					CostExtra = model.CostExtra,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});

				return model;
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		/// <summary></summary>
		public async Task DeleteAsync(int planId, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManagePlans);
			
				await RunAsync("_spPlanDelete", parameters: new {
					PlanId = planId,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		#endregion
	}
}