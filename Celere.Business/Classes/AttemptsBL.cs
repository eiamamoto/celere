﻿using Asteria.Business;
using Asteria.Business.Model;
using Asteria.Core.Tools;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;
using Celere.Business.Model;
using static Celere.Business.Enumerators;

namespace Celere.Business
{
	public class AttemptsBL : BaseBL
	{
		#region Constructor

		private string loggerName = "Celere.Business.AttemptsBL";
		
		/// <summary></summary>
		public AttemptsBL() : base()
		{
			ChangeLogger(loggerName);
		}
		
		/// <summary></summary>
		public AttemptsBL(SqlConnection conn, SqlTransaction transaction) : base(conn, transaction)
		{
			ChangeLogger(loggerName);
		}

		#endregion

		#region Selects

		/// <summary></summary>
		public async Task<PageResult<AttemptVM>> PageAsync(int page, int pageSize, string keyword, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageAttempts);

			return await PageAsync<AttemptVM>("_spAttemptsPage", page, new
			{
				Page = (page - 1),
				PageRows = pageSize,
				Keyword = keyword
			});
		}		

		/// <summary></summary>
		public async Task<IEnumerable<KeyValuePair<int, string>>> ListIdsAsync(int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageAttempts);

			return await ListAsync<KeyValuePair<int, string>>("_spAttemptsListIds");
		}

		/// <summary></summary>
		public async Task<AttemptVM> DetailAsync(int id, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageAttempts);

			var model = await SingleAsync<AttemptVM>("_spAttemptsGet", new
			{
				AttemptId = id
			});


			return model;
		}

		#endregion

		#region Handles

		/// <summary></summary>
		public async Task<AttemptVM> UpsertAsync(AttemptVM model, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageAttempts);

				model.AttemptId = await SingleAsync<int>("_spAttemptUpsert", parameters: new
				{
					AttemptId = model.AttemptId,
					NotificationId = model.NotificationId,
					DtReg = model.DtReg,
					Succeded = model.Succeded,
					Message = model.Message,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});

				return model;
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		/// <summary></summary>
		public async Task DeleteAsync(int attemptId, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageAttempts);
			
				await RunAsync("_spAttemptDelete", parameters: new {
					AttemptId = attemptId,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		#endregion
	}
}