﻿using Asteria.Business;
using Asteria.Business.Model;
using Asteria.Core.Tools;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;
using Celere.Business.Model;
using static Celere.Business.Enumerators;

namespace Celere.Business
{
	public class StatesBL : BaseBL
	{
		#region Constructor

		private string loggerName = "Celere.Business.StatesBL";
		
		/// <summary></summary>
		public StatesBL() : base()
		{
			ChangeLogger(loggerName);
		}
		
		/// <summary></summary>
		public StatesBL(SqlConnection conn, SqlTransaction transaction) : base(conn, transaction)
		{
			ChangeLogger(loggerName);
		}

		#endregion

		#region Selects

		/// <summary></summary>
		public async Task<PageResult<StateVM>> PageAsync(int page, int pageSize, string keyword, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageStates);

			return await PageAsync<StateVM>("_spStatesPage", page, new
			{
				Page = (page - 1),
				PageRows = pageSize,
				Keyword = keyword
			});
		}		

		/// <summary></summary>
		public async Task<IEnumerable<KeyValuePair<int, string>>> ListIdsAsync(int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageStates);

			return await ListAsync<KeyValuePair<int, string>>("_spStatesListIds");
		}

		/// <summary></summary>
		public async Task<StateVM> DetailAsync(int id, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageStates);

			var model = await SingleAsync<StateVM>("_spStatesGet", new
			{
				StateId = id
			});


			return model;
		}

		#endregion

		#region Handles

		/// <summary></summary>
		public async Task<StateVM> UpsertAsync(StateVM model, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageStates);

				model.StateId = await SingleAsync<int>("_spStateUpsert", parameters: new
				{
					StateId = model.StateId,
					Name = model.Name,
					Abbreviation = model.Abbreviation,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});

				return model;
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		/// <summary></summary>
		public async Task DeleteAsync(int stateId, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageStates);
			
				await RunAsync("_spStateDelete", parameters: new {
					StateId = stateId,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		#endregion
	}
}