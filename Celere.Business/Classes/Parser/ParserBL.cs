﻿using Celere.Business.Model.Custom;
using System;
using System.Reflection;
using System.Threading.Tasks;
using Celere.Business.Model.Profiles;

namespace Celere.Business.Parser
{
	public class ParserBL : BaseBL
	{
		#region Constructor

		private string loggerName = "Celere.Business.ParserBL";

		public ParserBL() : base()
		{
			ChangeLogger(loggerName);
		}

		#endregion Constructor

		public async Task<SearchResultVM<T>> SearchAsync<T>(ISearchFilter model)
		{
			Type type;
			MethodInfo method;

            // Pega o Profile
            type = Type.GetType(string.Format("Celere.Business.Classes.Parser.Profiles.{0}", model.ProfileName));

            // Cria uma referência ao método Search da classe
            method = type.GetMethod("Search", BindingFlags.Static | BindingFlags.Public);

			var taskResult = (Task<SearchResultVM<T>>)method.Invoke(null, new object[] { model });

			return await taskResult.ContinueWith(res =>
			{
				if (res.IsFaulted) throw res.Exception;
				return res.Result;
			});
		}

		public async Task<DetailResultVM<T>> DetailAsync<T>(ISearchItem model)
		{
			Type type;
			MethodInfo method;

            // Pega o Profile
            type = Type.GetType(string.Format("Celere.Business.Classes.Parser.Profiles.{0}", model.ProfileName));


            // Cria uma referência ao método Search da classe
            method = type.GetMethod("Detail", BindingFlags.Static | BindingFlags.Public);

			var taskResult = (Task<DetailResultVM<T>>)method.Invoke(null, new object[] { model });

			return await taskResult.ContinueWith(res =>
			{
				if (res.IsFaulted) throw res.Exception;
				return res.Result;
			});
		}
	}
}