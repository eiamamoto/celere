﻿using Asteria.Business;
using Asteria.Business.Model;
using Asteria.Core.Tools;

//using Celere.Core.Tools;
using Celere.Business.Tools;
using Celere.Business.Model.Custom;
using Celere.Business.Model.Profiles;
using HtmlAgilityPack;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Celere.Business.Classes.Parser.Profiles
{
	public class TJSP
	{
		private static readonly string TJSP_URL = "https://esaj.tjsp.jus.br";
		private static readonly string SEARCH_URL = "/cpopg/search.do";
		private static readonly Dictionary<string, string> DEFAULT_QUERYSTRINGS = new Dictionary<string, string>()
		{
			{ "dadosConsulta.localPesquisa.cdLocal", "-1" },
			{ "cbPesquisa", "DOCPARTE" },
			{ "dadosConsulta.tipoNuProcesso", "UNIFICADO" }
		};

		public static async Task<SearchResultVM<TJSPProcessItemVM>> Search(TJSPSearchFilterVM model)
		{
			var result = new SearchResultVM<TJSPProcessItemVM>();

			string htmlStringResult = string.Empty;
			string targetUrl = string.Empty;

			var queryString = new Dictionary<string, string>(DEFAULT_QUERYSTRINGS);
			queryString.Add("dadosConsulta.valorConsulta", model.Cpf);

			var doc = await Task.Run(() =>
				HtmlTool.DownloadHtml(string.Concat(TJSP_URL, SEARCH_URL), queryString, out htmlStringResult, out targetUrl));

			result.Url = targetUrl;

			var resultNodes = doc.GetElementbyId("listagemDeProcessos");
			if (resultNodes != null && resultNodes.ChildNodes != null && resultNodes.ChildNodes.Count > 0)
			{
				var resultItems = resultNodes.ChildNodes.Where(c => c.Attributes["id"] != null && c.Attributes["id"].Value.StartsWith("divProcesso"));

				if (resultItems != null && resultItems.Count() > 0)
				{
					result.Total = resultItems.Count();

					result.Items = resultItems.Select(c =>
					{
						HtmlNode node = null;

						node = c.ChildNodes.FirstOrDefault(d =>
							d.Name == "div"
							&& d.HasAttributes
							&& d.HasChildNodes
							&& d.Attributes["class"] != null
							&& (d.Attributes["class"].Value == "fundoClaro" || d.Attributes["class"].Value == "fundoEscuro"));

						if (node != null)
						{
							node = node.ChildNodes.FirstOrDefault(d =>
								d.Name == "div"
								&& d.HasAttributes
								&& d.HasChildNodes
								&& d.Attributes["class"] != null
								&& d.Attributes["class"].Value == "nuProcesso");

							if (node != null)
							{
								node = node.ChildNodes.FirstOrDefault(d =>
									d.Name == "a"
									&& d.HasAttributes
									&& d.Attributes["class"] != null
									&& d.Attributes["class"].Value == "linkProcesso");
							}
						}

						if (node != null)
						{
							return new TJSPProcessItemVM()
							{

								ProcessNumber = node.InnerText.Trim(),
								ProcessUrl = string.Concat(TJSP_URL, node.Attributes["href"].Value.Trim())
							};
						}
						return null;
					}).Where(r => r != null);
				}
			}

			return result;
		}

		public static async Task<DetailResultVM<TJSPDetailVM>> Detail(TJSPProcessItemVM model)
		{
			var result = new DetailResultVM<TJSPDetailVM>();
			result.Url = model.ProcessUrl;

			string htmlStringResult = string.Empty;
			string targetUrl = string.Empty;

			var doc = await Task.Run(() =>
				HtmlTool.DownloadHtml(model.ProcessUrl, out htmlStringResult, out targetUrl));

			result.Url = targetUrl;
			result.Process = new TJSPDetailVM()
			{
				ProcessNumber = model.ProcessNumber
			};


            //var processDataNodes = doc.DocumentNode.SelectSingleNode("/html[1]/body[1]/div[1]/table[4]/tr[1]/td[1]/div[1]/table[2]");
            //if (processDataNodes != null)
            //{
            //	string resultData = string.Join(" ",
            //		processDataNodes.ChildNodes.Where(c => c.Name == "tr").Select(c => c.InnerText))
            //		.RemoveLineEndings()
            //		.Replace(":&nbsp;", ": ")
            //		.Replace("&nbsp;", ".<br />");

            //	result.Process.ProcessData = string.Join(" ", resultData.Split().Where(x => x != ""));
            //}

            var processDataNodes = doc.DocumentNode.SelectSingleNode("/html[1]/body[1]/div[1]/table[4]/tr[1]/td[1]/div[1]/table[2]");
            if (processDataNodes != null)
            {
                string resultData = string.Join(" ",
                    processDataNodes.ChildNodes.Where(c => c.Name == "tr").Select(c => c.InnerText))
                    .RemoveLineEndings()
                    .Replace(":&nbsp;", ": ")
                    .Replace("&nbsp;", ".<br />");

                result.Process.ProcessData = string.Join(" ", resultData.Split().Where(x => x != ""));
            }

            

            if (processDataNodes != null) {
                var vara = processDataNodes.SelectNodes("//td/label[contains(., 'Valor da ação')]").First();
                //result.Process.Vara = vara.ParentNode.ParentNode.NextSibling.InnerHtml.ToString();
                
                result.Process.Vara = processDataNodes.SelectNodes("//td/label[contains(., 'Valor da ação')]").First().ParentNode.ChildNodes[3].InnerHtml;
            }


            result.Process.Distribution = "01/01/2018";
            
            result.Process.ProcessValue = "R$ 123,45";


            HtmlNode peopleInvolvedNodes = doc.GetElementbyId("tableTodasPartes");
			if (peopleInvolvedNodes == null)
			{
				peopleInvolvedNodes = doc.GetElementbyId("tablePartesPrincipais");
			}

			if (peopleInvolvedNodes != null)
			{
				string resultPeople = string.Join(" ",
					peopleInvolvedNodes.ChildNodes.Where(c => c.Name == "tr").Select(c => c.InnerText))
					.RemoveLineEndings()
					.Replace(":&nbsp;", ": ")
					.Replace("&nbsp;", ".<br />");

				result.Process.PeopleInvolved = string.Join(" ", resultPeople.Split().Where(x => x != ""));
			}

			return result;
		}
	}
}