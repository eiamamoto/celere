﻿using Asteria.Business;
using Asteria.Business.Model;
using Asteria.Core.Tools;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;
using Celere.Business.Model;
using static Celere.Business.Enumerators;

namespace Celere.Business
{
	public class LogTrackingCodesBL : BaseBL
	{
		#region Constructor

		private string loggerName = "Celere.Business.LogTrackingCodesBL";
		
		/// <summary></summary>
		public LogTrackingCodesBL() : base()
		{
			ChangeLogger(loggerName);
		}
		
		/// <summary></summary>
		public LogTrackingCodesBL(SqlConnection conn, SqlTransaction transaction) : base(conn, transaction)
		{
			ChangeLogger(loggerName);
		}

		#endregion

		#region Selects

		/// <summary></summary>
		public async Task<PageResult<LogTrackingCodeVM>> PageAsync(int page, int pageSize, string keyword, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageLogTrackingCodes);

			return await PageAsync<LogTrackingCodeVM>("_spLogTrackingCodesPage", page, new
			{
				Page = (page - 1),
				PageRows = pageSize,
				Keyword = keyword
			});
		}		

		/// <summary></summary>
		public async Task<IEnumerable<KeyValuePair<int, string>>> ListIdsAsync(int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageLogTrackingCodes);

			return await ListAsync<KeyValuePair<int, string>>("_spLogTrackingCodesListIds");
		}

		/// <summary></summary>
		public async Task<LogTrackingCodeVM> DetailAsync(int id, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageLogTrackingCodes);

			var model = await SingleAsync<LogTrackingCodeVM>("_spLogTrackingCodesGet", new
			{
				LogId = id
			});


			return model;
		}

		#endregion

		#region Handles

		/// <summary></summary>
		public async Task<LogTrackingCodeVM> UpsertAsync(LogTrackingCodeVM model, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageLogTrackingCodes);

				model.LogId = await SingleAsync<int>("_spLogTrackingCodeUpsert", parameters: new
				{
					LogId = model.LogId,
					DtReg = model.DtReg,
					OperationTypeId = model.OperationTypeId,
					TrackingCodeId = model.TrackingCodeId,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});

				return model;
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		/// <summary></summary>
		public async Task DeleteAsync(int logId, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageLogTrackingCodes);
			
				await RunAsync("_spLogTrackingCodeDelete", parameters: new {
					LogId = logId,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		#endregion
	}
}