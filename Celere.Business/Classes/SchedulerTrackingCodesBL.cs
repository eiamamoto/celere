﻿using Asteria.Business;
using Asteria.Business.Model;
using Asteria.Core.Tools;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;
using Celere.Business.Model;
using static Celere.Business.Enumerators;

namespace Celere.Business
{
	public class SchedulerTrackingCodesBL : BaseBL
	{
		#region Constructor

		private string loggerName = "Celere.Business.SchedulerTrackingCodesBL";
		
		/// <summary></summary>
		public SchedulerTrackingCodesBL() : base()
		{
			ChangeLogger(loggerName);
		}
		
		/// <summary></summary>
		public SchedulerTrackingCodesBL(SqlConnection conn, SqlTransaction transaction) : base(conn, transaction)
		{
			ChangeLogger(loggerName);
		}

		#endregion

		#region Selects

		/// <summary></summary>
		public async Task<PageResult<SchedulerTrackingCodeVM>> PageAsync(int page, int pageSize, string keyword, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageSchedulerTrackingCodes);

			return await PageAsync<SchedulerTrackingCodeVM>("_spSchedulerTrackingCodesPage", page, new
			{
				Page = (page - 1),
				PageRows = pageSize,
				Keyword = keyword
			});
		}		

		/// <summary></summary>
		public async Task<IEnumerable<KeyValuePair<int, string>>> ListIdsAsync(int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageSchedulerTrackingCodes);

			return await ListAsync<KeyValuePair<int, string>>("_spSchedulerTrackingCodesListIds");
		}

		/// <summary></summary>
		public async Task<SchedulerTrackingCodeVM> DetailAsync(int id, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageSchedulerTrackingCodes);

			var model = await SingleAsync<SchedulerTrackingCodeVM>("_spSchedulerTrackingCodesGet", new
			{
				SchedulerId = id
			});


			return model;
		}

		#endregion

		#region Handles

		/// <summary></summary>
		public async Task<SchedulerTrackingCodeVM> UpsertAsync(SchedulerTrackingCodeVM model, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageSchedulerTrackingCodes);

				model.SchedulerId = await SingleAsync<int>("_spSchedulerTrackingCodeUpsert", parameters: new
				{
					SchedulerId = model.SchedulerId,
					DtReg = model.DtReg,
					CpfCnpj = model.CpfCnpj,
					SchedulerStatusId = model.SchedulerStatusId,
					DtProcessed = model.DtProcessed,
					ServerChecking = model.ServerChecking,
					DtStart = model.DtStart,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});

				return model;
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		/// <summary></summary>
		public async Task DeleteAsync(int schedulerId, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageSchedulerTrackingCodes);
			
				await RunAsync("_spSchedulerTrackingCodeDelete", parameters: new {
					SchedulerId = schedulerId,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		#endregion
	}
}