﻿using Asteria.Business;
using Asteria.Business.Model;
using Asteria.Core.Tools;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;
using Celere.Business.Model;
using static Celere.Business.Enumerators;

namespace Celere.Business
{
	public class CitiesBL : BaseBL
	{
		#region Constructor

		private string loggerName = "Celere.Business.CitiesBL";
		
		/// <summary></summary>
		public CitiesBL() : base()
		{
			ChangeLogger(loggerName);
		}
		
		/// <summary></summary>
		public CitiesBL(SqlConnection conn, SqlTransaction transaction) : base(conn, transaction)
		{
			ChangeLogger(loggerName);
		}

		#endregion

		#region Selects

		/// <summary></summary>
		public async Task<PageResult<CityVM>> PageAsync(int page, int pageSize, string keyword, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageCities);

			return await PageAsync<CityVM>("_spCitiesPage", page, new
			{
				Page = (page - 1),
				PageRows = pageSize,
				Keyword = keyword
			});
		}		

		/// <summary></summary>
		public async Task<IEnumerable<KeyValuePair<int, string>>> ListIdsAsync(int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageCities);

			return await ListAsync<KeyValuePair<int, string>>("_spCitiesListIds");
		}

		/// <summary></summary>
		public async Task<CityVM> DetailAsync(int id, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageCities);

			var model = await SingleAsync<CityVM>("_spCitiesGet", new
			{
				CityId = id
			});


			return model;
		}

		#endregion

		#region Handles

		/// <summary></summary>
		public async Task<CityVM> UpsertAsync(CityVM model, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageCities);

				model.CityId = await SingleAsync<int>("_spCityUpsert", parameters: new
				{
					CityId = model.CityId,
					Name = model.Name,
					StateId = model.StateId,
					IsCapital = model.IsCapital,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});

				return model;
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		/// <summary></summary>
		public async Task DeleteAsync(int cityId, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageCities);
			
				await RunAsync("_spCityDelete", parameters: new {
					CityId = cityId,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		#endregion
	}
}