﻿using Asteria.Business;
using Asteria.Business.Model;
using Asteria.Core.Tools;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;
using Celere.Business.Model;
using static Celere.Business.Enumerators;

namespace Celere.Business
{
	public class CustomersBL : BaseBL
	{
		#region Constructor

		private string loggerName = "Celere.Business.CustomersBL";
		
		/// <summary></summary>
		public CustomersBL() : base()
		{
			ChangeLogger(loggerName);
		}
		
		/// <summary></summary>
		public CustomersBL(SqlConnection conn, SqlTransaction transaction) : base(conn, transaction)
		{
			ChangeLogger(loggerName);
		}

		#endregion

		#region Selects

		/// <summary></summary>
		public async Task<PageResult<CustomerVM>> PageAsync(int page, int pageSize, string keyword, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageCustomers);

			return await PageAsync<CustomerVM>("_spCustomersPage", page, new
			{
				Page = (page - 1),
				PageRows = pageSize,
				Keyword = keyword
			});
		}		

		/// <summary></summary>
		public async Task<IEnumerable<KeyValuePair<int, string>>> ListIdsAsync(int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageCustomers);

			return await ListAsync<KeyValuePair<int, string>>("_spCustomersListIds");
		}

		/// <summary></summary>
		public async Task<CustomerVM> DetailAsync(int id, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageCustomers);

			var model = await SingleAsync<CustomerVM>("_spCustomersGet", new
			{
				CustomerId = id
			});


			return model;
		}

		#endregion

		#region Handles

		/// <summary></summary>
		public async Task<CustomerVM> UpsertAsync(CustomerVM model, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageCustomers);

				model.CustomerId = await SingleAsync<int>("_spCustomerUpsert", parameters: new
				{
					CustomerId = model.CustomerId,
					AuthId = model.AuthId,
					DtReg = model.DtReg,
					Name = model.Name,
					AddressId = model.AddressId,
					Phone = model.Phone,
					Cel = model.Cel,
					CpfCnpj = model.CpfCnpj,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});

				return model;
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		/// <summary></summary>
		public async Task DeleteAsync(int customerId, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageCustomers);
			
				await RunAsync("_spCustomerDelete", parameters: new {
					CustomerId = customerId,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		#endregion
	}
}