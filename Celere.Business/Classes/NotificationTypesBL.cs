﻿using Asteria.Business;
using Asteria.Business.Model;
using Asteria.Core.Tools;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;
using Celere.Business.Model;
using static Celere.Business.Enumerators;

namespace Celere.Business
{
	public class NotificationTypesBL : BaseBL
	{
		#region Constructor

		private string loggerName = "Celere.Business.NotificationTypesBL";
		
		/// <summary></summary>
		public NotificationTypesBL() : base()
		{
			ChangeLogger(loggerName);
		}
		
		/// <summary></summary>
		public NotificationTypesBL(SqlConnection conn, SqlTransaction transaction) : base(conn, transaction)
		{
			ChangeLogger(loggerName);
		}

		#endregion

		#region Selects

		/// <summary></summary>
		public async Task<PageResult<NotificationTypeVM>> PageAsync(int page, int pageSize, string keyword, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageNotificationTypes);

			return await PageAsync<NotificationTypeVM>("_spNotificationTypesPage", page, new
			{
				Page = (page - 1),
				PageRows = pageSize,
				Keyword = keyword
			});
		}		

		/// <summary></summary>
		public async Task<IEnumerable<KeyValuePair<int, string>>> ListIdsAsync(int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageNotificationTypes);

			return await ListAsync<KeyValuePair<int, string>>("_spNotificationTypesListIds");
		}

		/// <summary></summary>
		public async Task<NotificationTypeVM> DetailAsync(int id, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageNotificationTypes);

			var model = await SingleAsync<NotificationTypeVM>("_spNotificationTypesGet", new
			{
				NotificationTypeId = id
			});


			return model;
		}

		#endregion

		#region Handles

		/// <summary></summary>
		public async Task<NotificationTypeVM> UpsertAsync(NotificationTypeVM model, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageNotificationTypes);

				model.NotificationTypeId = await SingleAsync<int>("_spNotificationTypeUpsert", parameters: new
				{
					NotificationTypeId = model.NotificationTypeId,
					NotificationType = model.NotificationType,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});

				return model;
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		/// <summary></summary>
		public async Task DeleteAsync(int notificationTypeId, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageNotificationTypes);
			
				await RunAsync("_spNotificationTypeDelete", parameters: new {
					NotificationTypeId = notificationTypeId,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		#endregion
	}
}