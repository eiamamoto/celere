﻿using Asteria.Business;
using Asteria.Business.Model;
using Asteria.Core.Tools;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;
using Celere.Business.Model;
using static Celere.Business.Enumerators;


using System.Reflection;
using Celere.Business.Model.Custom;


namespace Celere.Business
{
	public class ProcessesBL : BaseBL
	{
		#region Constructor

		private string loggerName = "Celere.Business.ProcessesBL";
		
		/// <summary></summary>
		public ProcessesBL() : base()
		{
			ChangeLogger(loggerName);
		}
		
		/// <summary></summary>
		public ProcessesBL(SqlConnection conn, SqlTransaction transaction) : base(conn, transaction)
		{
			ChangeLogger(loggerName);
		}

		#endregion

		#region Selects

		/// <summary></summary>
		public async Task<PageResult<ProcessVM>> PageAsync(int page, int pageSize, string keyword, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageProcesses);

			return await PageAsync<ProcessVM>("_spProcessesPage", page, new
			{
				Page = (page - 1),
				PageRows = pageSize,
				Keyword = keyword
			});
		}		




		/// <summary></summary>
		public async Task<IEnumerable<KeyValuePair<int, string>>> ListIdsAsync(int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageProcesses);

			return await ListAsync<KeyValuePair<int, string>>("_spProcessesListIds");
		}


        /// <summary>List of processes of an CPF/CNPJ</summary>
        public async Task<IEnumerable<ProcessVM>> ListOfCpfCnpjAsync(string cpfCnpj,  int currentAuthId)
        {
            await CheckPermissionAsync(currentAuthId, EnumTask.ManageProcesses);

            return await ListAsync<ProcessVM>("spProcessesListOfTrackingCode", new
            {
                CpfCnpj = cpfCnpj
            });
        }


        /// <summary></summary>
        public async Task<ProcessVM> DetailAsync(int id, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageProcesses);

			var model = await SingleAsync<ProcessVM>("_spProcessesGet", new
			{
				ProcessId = id
			});


			return model;
		}

		#endregion

		#region Handles

		/// <summary></summary>
		public async Task<ProcessVM> UpsertAsync(ProcessVM model, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageProcesses);

				model.ProcessId = await SingleAsync<string>("_spProcessUpsert", parameters: new
				{
					ProcessId = model.ProcessId,
					DtReg = model.DtReg,
					DtDistribution = model.DtDistribution,
					Value = model.Value,
					Vara = model.Vara,
					CpfCnpj = model.CpfCnpj,
					HasNotifications = model.HasNotifications,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});

				return model;
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		/// <summary></summary>
		public async Task DeleteAsync(int processId, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageProcesses);
			
				await RunAsync("_spProcessDelete", parameters: new {
					ProcessId = processId,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

        #endregion


        public async Task<SearchResultVM<T>> SearchAsync<T>(ISearchFilter model)
        {
            Type type;
            MethodInfo method;

            // Pega o Profile
            type = Type.GetType(string.Format("Celere.Business.Profiles.{0},Celere.Business", model.ProfileName));

            // Cria uma referência ao método Search da classe
            method = type.GetMethod("Search", BindingFlags.Static | BindingFlags.Public);

            var taskResult = (Task<SearchResultVM<T>>)method.Invoke(null, new object[] { model });

            return await taskResult.ContinueWith(res =>
            {
                if (res.IsFaulted) throw res.Exception;
                return res.Result;
            });
        }

        public async Task<DetailResultVM<T>> DetailAsync<T>(ISearchItem model)
        {
            Type type;
            MethodInfo method;

            // Pega o Profile
            type = Type.GetType(string.Format("Celere.Business.Profiles.{0},Celere.Business", model.ProfileName));

            // Cria uma referência ao método Search da classe
            method = type.GetMethod("Detail", BindingFlags.Static | BindingFlags.Public);

            var taskResult = (Task<DetailResultVM<T>>)method.Invoke(null, new object[] { model });

            return await taskResult.ContinueWith(res =>
            {
                if (res.IsFaulted) throw res.Exception;
                return res.Result;
            });
        }




    }
}