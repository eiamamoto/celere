﻿using Asteria.Business;
using Asteria.Business.Model;
using Asteria.Core.Tools;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;
using Celere.Business.Model;
using static Celere.Business.Enumerators;

namespace Celere.Business
{
	public class OperationTypesBL : BaseBL
	{
		#region Constructor

		private string loggerName = "Celere.Business.OperationTypesBL";
		
		/// <summary></summary>
		public OperationTypesBL() : base()
		{
			ChangeLogger(loggerName);
		}
		
		/// <summary></summary>
		public OperationTypesBL(SqlConnection conn, SqlTransaction transaction) : base(conn, transaction)
		{
			ChangeLogger(loggerName);
		}

		#endregion

		#region Selects

		/// <summary></summary>
		public async Task<PageResult<OperationTypeVM>> PageAsync(int page, int pageSize, string keyword, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageOperationTypes);

			return await PageAsync<OperationTypeVM>("_spOperationTypesPage", page, new
			{
				Page = (page - 1),
				PageRows = pageSize,
				Keyword = keyword
			});
		}		

		/// <summary></summary>
		public async Task<IEnumerable<KeyValuePair<int, string>>> ListIdsAsync(int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageOperationTypes);

			return await ListAsync<KeyValuePair<int, string>>("_spOperationTypesListIds");
		}

		/// <summary></summary>
		public async Task<OperationTypeVM> DetailAsync(int id, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageOperationTypes);

			var model = await SingleAsync<OperationTypeVM>("_spOperationTypesGet", new
			{
				OperationTypeId = id
			});


			return model;
		}

		#endregion

		#region Handles

		/// <summary></summary>
		public async Task<OperationTypeVM> UpsertAsync(OperationTypeVM model, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageOperationTypes);

				model.OperationTypeId = await SingleAsync<int>("_spOperationTypeUpsert", parameters: new
				{
					OperationTypeId = model.OperationTypeId,
					Name = model.Name,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});

				return model;
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		/// <summary></summary>
		public async Task DeleteAsync(int operationTypeId, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageOperationTypes);
			
				await RunAsync("_spOperationTypeDelete", parameters: new {
					OperationTypeId = operationTypeId,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		#endregion
	}
}