﻿using Asteria.Business;
using Asteria.Business.Model;
using Asteria.Core.Tools;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;
using Celere.Business.Model;
using static Celere.Business.Enumerators;

namespace Celere.Business
{
	public class TrackingCodesBL : BaseBL
	{
		#region Constructor

		private string loggerName = "Celere.Business.TrackingCodesBL";
		
		/// <summary></summary>
		public TrackingCodesBL() : base()
		{
			ChangeLogger(loggerName);
		}
		
		/// <summary></summary>
		public TrackingCodesBL(SqlConnection conn, SqlTransaction transaction) : base(conn, transaction)
		{
			ChangeLogger(loggerName);
		}

		#endregion

		#region Selects

		/// <summary></summary>
		public async Task<PageResult<TrackingCodeVM>> PageAsync(int page, int pageSize, string keyword, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageTrackingCodes);

			return await PageAsync<TrackingCodeVM>("_spTrackingCodesPage", page, new
			{
				Page = (page - 1),
				PageRows = pageSize,
				Keyword = keyword
			});
		}		

		/// <summary></summary>
		public async Task<IEnumerable<KeyValuePair<int, string>>> ListIdsAsync(int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageTrackingCodes);

			return await ListAsync<KeyValuePair<int, string>>("_spTrackingCodesListIds");
		}

		/// <summary></summary>
		public async Task<TrackingCodeVM> DetailAsync(int id, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageTrackingCodes);

			var model = await SingleAsync<TrackingCodeVM>("_spTrackingCodesGet", new
			{
				TrackingCodeId = id
			});


			return model;
		}

		#endregion

		#region Handles

		/// <summary></summary>
		public async Task<TrackingCodeVM> UpsertAsync(TrackingCodeVM model, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageTrackingCodes);

				model.TrackingCodeId = await SingleAsync<int>("_spTrackingCodeUpsert", parameters: new
				{
					TrackingCodeId = model.TrackingCodeId,
					DtReg = model.DtReg,
					DtActivation = model.DtActivation,
					CpfCnpj = model.CpfCnpj,
					IsActive = model.IsActive,
					CustomerId = model.CustomerId,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});

				return model;
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		/// <summary></summary>
		public async Task DeleteAsync(int trackingCodeId, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageTrackingCodes);
			
				await RunAsync("_spTrackingCodeDelete", parameters: new {
					TrackingCodeId = trackingCodeId,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		#endregion
	}
}