﻿using Asteria.Business;
using Asteria.Business.Model;
using Asteria.Core.Tools;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;
using Celere.Business.Model;
using static Celere.Business.Enumerators;

namespace Celere.Business
{
	public class PartsBL : BaseBL
	{
		#region Constructor

		private string loggerName = "Celere.Business.PartsBL";
		
		/// <summary></summary>
		public PartsBL() : base()
		{
			ChangeLogger(loggerName);
		}
		
		/// <summary></summary>
		public PartsBL(SqlConnection conn, SqlTransaction transaction) : base(conn, transaction)
		{
			ChangeLogger(loggerName);
		}

		#endregion

		#region Selects

		/// <summary></summary>
		public async Task<PageResult<PartVM>> PageAsync(int page, int pageSize, string keyword, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageParts);

			return await PageAsync<PartVM>("_spPartsPage", page, new
			{
				Page = (page - 1),
				PageRows = pageSize,
				Keyword = keyword
			});
		}		

		/// <summary></summary>
		public async Task<IEnumerable<KeyValuePair<int, string>>> ListIdsAsync(int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageParts);

			return await ListAsync<KeyValuePair<int, string>>("_spPartsListIds");
		}

		/// <summary></summary>
		public async Task<PartVM> DetailAsync(int id, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageParts);

			var model = await SingleAsync<PartVM>("_spPartsGet", new
			{
				PartId = id
			});


			return model;
		}

		#endregion

		#region Handles

		/// <summary></summary>
		public async Task<PartVM> UpsertAsync(PartVM model, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageParts);

				model.PartId = await SingleAsync<int>("_spPartUpsert", parameters: new
				{
					PartId = model.PartId,
					DtReg = model.DtReg,
					ProcessId = model.ProcessId,
					Name = model.Name,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});

				return model;
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		/// <summary></summary>
		public async Task DeleteAsync(int partId, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageParts);
			
				await RunAsync("_spPartDelete", parameters: new {
					PartId = partId,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		#endregion
	}
}