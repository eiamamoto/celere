﻿using Asteria.Core;
using Asteria.Core.Settings;
using System.Data.SqlClient;
using System.Net;
using System.Threading.Tasks;
using static Celere.Business.Enumerators;

namespace Celere.Business
{
	public class BaseBL : Asteria.Business.BaseBL
	{
		#region Constructor

		public BaseBL() : base()
		{
		}

		public BaseBL(SqlConnection conn, SqlTransaction transaction) : base(conn, transaction)
		{
		}

		#endregion

		#region Permissions

		public async Task<bool> CheckPermissionAsync(int authId, EnumTask task)
		{
			var allowed = await ScalarAsync<bool>("__spCheckPermission", parameters: new
			{
				AuthId = authId,
				EnumTask = task
			});

			if (allowed == false)
			{
				throw new AsteriaException(SystemMessages.Error_UserNotAuthorized, HttpStatusCode.Forbidden);
			}

			return true;
		}

		#endregion
	}
}
