﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Celere.Business.Model.Internal;
using static Celere.Business.Enumerators;

namespace Celere.Business
{
	public class TasksBL : BaseBL
	{
		#region Constructor

		private string loggerName = "Celere.Business.TasksBL";
		
		/// <summary></summary>
		public TasksBL() : base()
		{
			ChangeLogger(loggerName);
		}
		
		/// <summary></summary>
		public TasksBL(SqlConnection conn, SqlTransaction transaction) : base(conn, transaction)
		{
			ChangeLogger(loggerName);
		}

		#endregion

		#region Selects	

		/// <summary></summary>
		public async Task<IEnumerable<KeyValuePair<int, string>>> ListAsync(int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageTasks);

			return await ListAsync<KeyValuePair<int, string>>("__spTasksList");
		}

		#endregion
	}
}