﻿using Asteria.Business;
using Asteria.Business.Model;
using Asteria.Business.Model.Auth;
using Asteria.Core;
using Asteria.Core.Settings;
using Asteria.Core.Tools;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using Celere.Business.Model;
using Celere.Business.Model.Internal;
using static Celere.Business.Enumerators;

namespace Celere.Business
{
	public class UserBL : BaseBL
	{
		#region Constructor

		private string loggerName = "Celere.Business.UserBL";

		/// <summary></summary>
		public UserBL() : base()
		{
			ChangeLogger(loggerName);
		}

		/// <summary></summary>
		public UserBL(SqlConnection conn, SqlTransaction transaction) : base(conn, transaction)
		{
			ChangeLogger(loggerName);
		}

		#endregion

		#region Auth

		public async Task<UserVM> AuthenticateAsync(string email, string password)
		{
			UserVM user = null;

			try
			{
				var auth = new AuthBL();

				user = await auth.AuthAsync<UserVM>(email);

				auth.ChallengeCredentials(user, password);
			}
			catch (AsteriaException EX)
			{
				throw EX;
			}
			catch (Exception EX)
			{
				throw new AsteriaException(EX.Message, EX, HttpStatusCode.Forbidden);
			}

			user.Permissions = await PermissionsAsync(user.AuthId);

			//if (!string.IsNullOrWhiteSpace(deviceToken))
			//{
			//	var blDevices = new DevicesBL(_db, dbTransaction);

			//	await blDevices.AddOrRemoveDevice(model.AuthId, deviceToken, true);
			//}

			return user;
		}

		public async Task PasswordResetAsync(PasswordResetVM model)
		{
			// Check if the two parameters were filled
			if (string.IsNullOrWhiteSpace(model.NewPassword) || string.IsNullOrWhiteSpace(model.PasswordConfirmation))
			{
				throw new AsteriaException(SystemMessages.Error_FieldsRequired, HttpStatusCode.BadRequest);
			}

			// Check if the password and the confirmation are the same
			if (model.NewPassword != model.PasswordConfirmation)
			{
				throw new AsteriaException(SystemMessages.Error_PasswordMismatch, HttpStatusCode.BadRequest);
			}

			// As a last security measure we recheck the given token
			await ValidatePasswordResetTokenAsync(new PasswordResetValidationVM()
			{
				email = model.Email,
				token = model.Token
			});

			// Check the new password against security rules
			ValidatePassword(model.NewPassword);

			var auth = new AuthBL();

			// Store the new password and clean the confirmation token
			var data = await auth.AuthAsync<UserVM>(model.Email);

			data.Password = Cryptography.CreateHash(model.NewPassword);
			data.EmailConfirmationToken = null;
			data.EmailConfirmationExpiryDate = null;

			await auth.UpdatePasswordAsync(data);
		}

		public async Task<string> ForgotPasswordAsync(string email)
		{
			try
			{
				var auth = new AuthBL();

				// User lookup
				AuthVM model = await auth.AuthAsync<AuthVM>(email);

				// Create and store a new confirmation token
				string newToken = Utilities.RandomString(12);

				model.EmailConfirmationToken = Cryptography.CreateHash(newToken);
				model.EmailConfirmationExpiryDate = DateTime.Now.AddHours(2);

				await auth.RefreshTokenAsync(model);

				// Send an email containing the new confirmation token
				string link = string.Format(
					"{0}#!/passwordreset?email={1}&token={2}",
					GlobalSettings.RootUrl_ControlPanel,
					HttpUtility.UrlEncode(model.Email),
					HttpUtility.UrlEncode(newToken));

				var subject = SystemMessages.Message_ForgotPasswordMailSubject;

				var body = SystemMessages.Message_ForgotPasswordMailMessage
						.Replace("[BREAKLINE]", "<br />")
						.Replace("[LINK]", string.Format("<a href=\"{0}\">Recuperação de senha</a>", link));

				await SendMail.SendAsync(subject, body, model.Email);

				return SystemMessages.Message_ForgotPasswordSubmitted;
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		public async Task ValidatePasswordResetTokenAsync(PasswordResetValidationVM model)
		{
			var auth = new AuthBL();

			var target = await auth.AuthAsync<UserVM>(model.email);

			auth.ChallengeConfirmationToken(target, model.token);
		}

		#endregion

		#region Internal Methods

		/// <summary>Popula um enum com as permissões correspondentes ao usuário</summary>
		/// <param name="authId">Id do usuário para o qual deseja-se consultar as permissões</param>
		/// <returns>Lista de enumeradores contendo as permissões do usuário</returns>
		private async Task<IEnumerable<EnumTask>> PermissionsAsync(int authId)
		{
			return await ListAsync<EnumTask>("__spUserPermissionsGet", new { AuthId = authId });
		}

		private void ValidatePassword(string password)
		{
			if (password.Length < 6)
			{
				throw new AsteriaException(SystemMessages.Error_PasswordLength, HttpStatusCode.BadRequest);
			}
		}

		#endregion

		#region Selects

		/// <summary></summary>
		public async Task<PageResult<UserVM>> PageAsync(int page, int pageSize, string keyword, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageUsers);

			return await PageAsync<UserVM>("__spUsersPage", page, new
			{
				Page = (page - 1),
				PageRows = pageSize,
				Keyword = keyword
			});
		}

		/// <summary></summary>
		public async Task<IEnumerable<KeyValuePair<int, string>>> ListIdsAsync(int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageUsers);

			return await ListAsync<KeyValuePair<int, string>>("__spUsersListIds");
		}

		/// <summary></summary>
		public async Task<UserVM> DetailAsync(int id, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageUsers);

			var model = await SingleAsync<UserVM>("__spUserGet", new
			{
				AuthId = id
			});

			model.ImageAvatar = ImageUrl(model.ImageAvatar, "auth");

			return model;
		}

		#endregion

		#region Handles

		/// <summary></summary>
		public async Task<UserVM> UpsertAsync(UserVM model, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageUsers);

				model.AuthId = await SingleAsync<int>("__spUserUpsert", parameters: new
				{
					AuthId = model.AuthId,
					Name = model.Name,
					Email = model.Email,
					Password = model.Password,
					// Reminder = model.Reminder,
					ImageAvatar = model.ImageAvatar,
					ConfirmationToken = model.EmailConfirmationToken,
					ConfirmationTokenExpiryDate = model.EmailConfirmationExpiryDate,
					EnumLanguage = model.EnumLanguage,
					IsActive = model.IsUserActive,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});

				return model;
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		/// <summary></summary>
		public async Task DeleteAsync(int authId, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageUsers);

				await RunAsync("__spUserDelete", parameters: new
				{
					AuthId = authId,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		#endregion
	}
}