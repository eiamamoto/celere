﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Celere.Business.Model.Reports;
using static Celere.Business.Enumerators;

namespace Celere.Business
{
	public class ReportsBL : BaseBL
    {
        #region Constructor

        private string loggerName = "Celere.Business.ReportsBL";

        /// <summary></summary>
        public ReportsBL() : base()
        {
            ChangeLogger(loggerName);
        }

        /// <summary></summary>
        public ReportsBL(SqlConnection conn, SqlTransaction transaction) : base(conn, transaction)
        {
            ChangeLogger(loggerName);
        }

        #endregion

        #region Selects

        /// <summary></summary>
        public async Task<IEnumerable<UserVM>> UsersAsync(DateTime from, DateTime to, int currentAuthId)
        {
            await CheckPermissionAsync(currentAuthId, EnumTask.ManageUsers);

            var model = await ListAsync<UserVM>("__spReportUsers", new
            {
                From = from,
                To = to
            });

            return model;
        }

        #endregion
    }
}