﻿using Asteria.Business.Model;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Celere.Business.Model;
using Celere.Business.Model.Internal;
using static Celere.Business.Enumerators;

namespace Celere.Business
{
	public class LogsBL : BaseBL
	{
		#region Constructor

		private string loggerName = "Celere.Business.LogsBL";
		
		/// <summary></summary>
		public LogsBL() : base()
		{
			ChangeLogger(loggerName);
		}
		
		/// <summary></summary>
		public LogsBL(SqlConnection conn, SqlTransaction transaction) : base(conn, transaction)
		{
			ChangeLogger(loggerName);
		}

		#endregion

		#region Selects

		/// <summary></summary>
		public async Task<PageResult<LogVM>> PageAsync(int page, int pageSize, string keyword, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageLogs);

			return await PageAsync<LogVM>("__spLogsPage", page, new
			{
				Page = (page - 1),
				PageRows = pageSize,
				Keyword = keyword
			});
		}

		/// <summary></summary>
		public async Task<LogVM> DetailAsync(int id, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageLogs);

			var model = await SingleAsync<LogVM>("__spLogsGet", new
			{
				LogId = id
			});
			
			return model;
		}

		#endregion
	}
}