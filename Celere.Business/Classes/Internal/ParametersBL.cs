﻿using Asteria.Business.Model;
using Asteria.Core.Tools;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;
using Celere.Business.Model;
using Celere.Business.Model.Internal;
using static Celere.Business.Enumerators;

namespace Celere.Business
{
	public class ParametersBL : BaseBL
	{
		#region Constructor

		private string loggerName = "Celere.Business.ParametersBL";
		
		/// <summary></summary>
		public ParametersBL() : base()
		{
			ChangeLogger(loggerName);
		}
		
		/// <summary></summary>
		public ParametersBL(SqlConnection conn, SqlTransaction transaction) : base(conn, transaction)
		{
			ChangeLogger(loggerName);
		}

		#endregion

		#region Selects

		/// <summary></summary>
		public async Task<PageResult<ParameterVM>> PageAsync(int page, int pageSize, string keyword, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageParameters);

			return await PageAsync<ParameterVM>("__spParametersPage", page, new
			{
				Page = (page - 1),
				PageRows = pageSize,
				Keyword = keyword
			});
		}

		/// <summary></summary>
		public async Task<ParameterVM> DetailAsync(int id, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageParameters);

			var model = await SingleAsync<ParameterVM>("__spParametersGet", new
			{
				ParameterId = id
			});


			return model;
		}

		#endregion

		#region Handles

		/// <summary></summary>
		public async Task<ParameterVM> UpdateAsync(ParameterVM model, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageParameters);
								
				await SingleAsync<int>("__spParameterUpdate", parameters: new
				{
					ParameterId = model.ParameterId,
					Key = model.Key,
					Value = model.Value,
					ValueDev = model.ValueDev,
					ValueQA = model.ValueQA,
					Description = model.Description,
					IsReadOnly = model.IsReadOnly,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});

				return model;
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		#endregion
	}
}