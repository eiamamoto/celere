﻿using Asteria.Business.Model;
using Asteria.Core.Tools;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;
using Celere.Business.Model;
using Celere.Business.Model.Internal;
using static Celere.Business.Enumerators;

namespace Celere.Business
{
	public class GroupsBL : BaseBL
	{
		#region Constructor

		private string loggerName = "Celere.Business.GroupsBL";

		/// <summary></summary>
		public GroupsBL() : base()
		{
			ChangeLogger(loggerName);
		}

		/// <summary></summary>
		public GroupsBL(SqlConnection conn, SqlTransaction transaction) : base(conn, transaction)
		{
			ChangeLogger(loggerName);
		}

		#endregion

		#region Selects

		/// <summary></summary>
		public async Task<PageResult<GroupVM>> PageAsync(int page, int pageSize, string keyword, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageGroups);

			return await PageAsync<GroupVM>("__spGroupsPage", page, new
			{
				Page = (page - 1),
				PageRows = pageSize,
				Keyword = keyword
			});
		}

		/// <summary></summary>
		public async Task<IEnumerable<KeyValuePair<int, string>>> ListIdsAsync(int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageGroups);

			return await ListAsync<KeyValuePair<int, string>>("__spGroupsListIds");
		}

		/// <summary></summary>
		public async Task<GroupVM> DetailAsync(int id, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageGroups);

			var model = await SingleAsync<GroupVM>("__spGroupsGet", new
			{
				GroupId = id
			});

			return model;
		}

		#endregion

		#region Handles

		/// <summary></summary>
		public async Task<GroupVM> UpsertAsync(GroupVM model, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageGroups);

				model.GroupId = await SingleAsync<int>("__spGroupUpsert", parameters: new
				{
					GroupId = model.GroupId,
					Name = model.Name,
					IsActive = model.IsActive,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});

				return model;
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		/// <summary></summary>
		public async Task DeleteAsync(int groupId, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageGroups);

				await RunAsync("__spGroupDelete", parameters: new
				{
					GroupId = groupId,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		#endregion
	}
}