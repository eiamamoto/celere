﻿using Asteria.Business;
using Asteria.Business.Model;
using Asteria.Core.Tools;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;
using Celere.Business.Model;
using static Celere.Business.Enumerators;

namespace Celere.Business
{
	public class NotificationsBL : BaseBL
	{
		#region Constructor

		private string loggerName = "Celere.Business.NotificationsBL";
		
		/// <summary></summary>
		public NotificationsBL() : base()
		{
			ChangeLogger(loggerName);
		}
		
		/// <summary></summary>
		public NotificationsBL(SqlConnection conn, SqlTransaction transaction) : base(conn, transaction)
		{
			ChangeLogger(loggerName);
		}

		#endregion

		#region Selects

		/// <summary></summary>
		public async Task<PageResult<NotificationVM>> PageAsync(int page, int pageSize, string keyword, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageNotifications);

			return await PageAsync<NotificationVM>("_spNotificationsPage", page, new
			{
				Page = (page - 1),
				PageRows = pageSize,
				Keyword = keyword
			});
		}		

		/// <summary></summary>
		public async Task<IEnumerable<KeyValuePair<int, string>>> ListIdsAsync(int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageNotifications);

			return await ListAsync<KeyValuePair<int, string>>("_spNotificationsListIds");
		}

		/// <summary></summary>
		public async Task<NotificationVM> DetailAsync(int id, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageNotifications);

			var model = await SingleAsync<NotificationVM>("_spNotificationsGet", new
			{
				NotificationId = id
			});


			return model;
		}

		#endregion

		#region Handles

		/// <summary></summary>
		public async Task<NotificationVM> UpsertAsync(NotificationVM model, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageNotifications);

				model.NotificationId = await SingleAsync<int>("_spNotificationUpsert", parameters: new
				{
					NotificationId = model.NotificationId,
					DtReg = model.DtReg,
					Succeded = model.Succeded,
					DtSucceded = model.DtSucceded,
					ProcessId = model.ProcessId,
					CustomerId = model.CustomerId,
					NotificationTypeId = model.NotificationTypeId,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});

				return model;
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		/// <summary></summary>
		public async Task DeleteAsync(int notificationId, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageNotifications);
			
				await RunAsync("_spNotificationDelete", parameters: new {
					NotificationId = notificationId,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}


        /// <summary>Create notifications that were not created yet</summary>
        public async Task CheckAsync(int currentAuthId)
        {
            try
            {
                await CheckPermissionAsync(currentAuthId, EnumTask.ManageSchedulerCreations);

                await RunAsync("spVerifyNotifications");
            }
            catch (Exception EX)
            {
                throw EX;
            }
        }


        #endregion
    }
}