﻿using Asteria.Business;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Celere.Business.Model;
using static Celere.Business.Enumerators;

namespace Celere.Business
{
	public class DashboardBL : BaseBL
	{
		#region Constructor

		private string loggerName = "Celere.Business.DashboardBL";

		/// <summary></summary>
		public DashboardBL() : base()
		{
			ChangeLogger(loggerName);
		}

		/// <summary></summary>
		public DashboardBL(SqlConnection conn, SqlTransaction transaction) : base(conn, transaction)
		{
			ChangeLogger(loggerName);
		}

		#endregion

		/// <summary></summary>
		public async Task<DashboardVM> DetailAsync(int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ControlPanelAccess);

			var result = new DashboardVM();

			using (var multi = await MultiAsync("__spDashboardGet"))
			{
				// Addresses
				result.Addresses = await multi.ReadSingleOrDefaultAsync<int>();

				// Attempts
				result.Attempts = await multi.ReadSingleOrDefaultAsync<int>();

				// Cities
				result.Cities = await multi.ReadSingleOrDefaultAsync<int>();

				// Customers
				result.Customers = await multi.ReadSingleOrDefaultAsync<int>();

				// LogTrackingCodes
				result.LogTrackingCodes = await multi.ReadSingleOrDefaultAsync<int>();

				// Movements
				result.Movements = await multi.ReadSingleOrDefaultAsync<int>();

				// Notifications
				result.Notifications = await multi.ReadSingleOrDefaultAsync<int>();

				// NotificationTypes
				result.NotificationTypes = await multi.ReadSingleOrDefaultAsync<int>();

				// OperationTypes
				result.OperationTypes = await multi.ReadSingleOrDefaultAsync<int>();

				// Parts
				result.Parts = await multi.ReadSingleOrDefaultAsync<int>();

				// Plans
				result.Plans = await multi.ReadSingleOrDefaultAsync<int>();

				// Processes
				result.Processes = await multi.ReadSingleOrDefaultAsync<int>();

				// SchedulerCreations
				result.SchedulerCreations = await multi.ReadSingleOrDefaultAsync<int>();

				// SchedulerStatus
				result.SchedulerStatus = await multi.ReadSingleOrDefaultAsync<int>();

				// SchedulerTrackingCodes
				result.SchedulerTrackingCodes = await multi.ReadSingleOrDefaultAsync<int>();

				// States
				result.States = await multi.ReadSingleOrDefaultAsync<int>();

				// TrackingCodes
				result.TrackingCodes = await multi.ReadSingleOrDefaultAsync<int>();

			}

			return result;
		}
	}
}