﻿using Asteria.Business;
using Asteria.Business.Model;
using Asteria.Core.Tools;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;
using Celere.Business.Model;
using static Celere.Business.Enumerators;

namespace Celere.Business
{
	public class AddressesBL : BaseBL
	{
		#region Constructor

		private string loggerName = "Celere.Business.AddressesBL";
		
		/// <summary></summary>
		public AddressesBL() : base()
		{
			ChangeLogger(loggerName);
		}
		
		/// <summary></summary>
		public AddressesBL(SqlConnection conn, SqlTransaction transaction) : base(conn, transaction)
		{
			ChangeLogger(loggerName);
		}

		#endregion

		#region Selects

		/// <summary></summary>
		public async Task<PageResult<AddressVM>> PageAsync(int page, int pageSize, string keyword, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageAddresses);

			return await PageAsync<AddressVM>("_spAddressesPage", page, new
			{
				Page = (page - 1),
				PageRows = pageSize,
				Keyword = keyword
			});
		}		

		/// <summary></summary>
		public async Task<IEnumerable<KeyValuePair<int, string>>> ListIdsAsync(int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageAddresses);

			return await ListAsync<KeyValuePair<int, string>>("_spAddressesListIds");
		}

		/// <summary></summary>
		public async Task<AddressVM> DetailAsync(int id, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageAddresses);

			var model = await SingleAsync<AddressVM>("_spAddressesGet", new
			{
				AddressId = id
			});


			return model;
		}

		#endregion

		#region Handles

		/// <summary></summary>
		public async Task<AddressVM> UpsertAsync(AddressVM model, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageAddresses);

				model.AddressId = await SingleAsync<int>("_spAddressUpsert", parameters: new
				{
					AddressId = model.AddressId,
					Address1 = model.Address1,
					Address2 = model.Address2,
					Neighborhood = model.Neighborhood,
					ZipCode = model.ZipCode,
					CityId = model.CityId,
					Lat = model.Lat,
					Lng = model.Lng,
					IsActive = model.IsActive,
					Number = model.Number,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});

				return model;
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		/// <summary></summary>
		public async Task DeleteAsync(int addressId, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageAddresses);
			
				await RunAsync("_spAddressDelete", parameters: new {
					AddressId = addressId,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		#endregion
	}
}