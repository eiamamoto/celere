﻿using Asteria.Business;
using Asteria.Business.Model;
using Asteria.Core.Tools;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;
using Celere.Business.Model;
using static Celere.Business.Enumerators;

namespace Celere.Business
{
	public class SchedulerStatusBL : BaseBL
	{
		#region Constructor

		private string loggerName = "Celere.Business.SchedulerStatusBL";
		
		/// <summary></summary>
		public SchedulerStatusBL() : base()
		{
			ChangeLogger(loggerName);
		}
		
		/// <summary></summary>
		public SchedulerStatusBL(SqlConnection conn, SqlTransaction transaction) : base(conn, transaction)
		{
			ChangeLogger(loggerName);
		}

		#endregion

		#region Selects

		/// <summary></summary>
		public async Task<PageResult<SchedulerStatuVM>> PageAsync(int page, int pageSize, string keyword, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageSchedulerStatus);

			return await PageAsync<SchedulerStatuVM>("_spSchedulerStatusPage", page, new
			{
				Page = (page - 1),
				PageRows = pageSize,
				Keyword = keyword
			});
		}		

		/// <summary></summary>
		public async Task<IEnumerable<KeyValuePair<int, string>>> ListIdsAsync(int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageSchedulerStatus);

			return await ListAsync<KeyValuePair<int, string>>("_spSchedulerStatusListIds");
		}

		/// <summary></summary>
		public async Task<SchedulerStatuVM> DetailAsync(int id, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageSchedulerStatus);

			var model = await SingleAsync<SchedulerStatuVM>("_spSchedulerStatusGet", new
			{
				SchedulerStatusId = id
			});


			return model;
		}

		#endregion

		#region Handles

		/// <summary></summary>
		public async Task<SchedulerStatuVM> UpsertAsync(SchedulerStatuVM model, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageSchedulerStatus);

				model.SchedulerStatusId = await SingleAsync<int>("_spSchedulerStatuUpsert", parameters: new
				{
					SchedulerStatusId = model.SchedulerStatusId,
					Status = model.Status,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});

				return model;
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		/// <summary></summary>
		public async Task DeleteAsync(int schedulerStatusId, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageSchedulerStatus);
			
				await RunAsync("_spSchedulerStatuDelete", parameters: new {
					SchedulerStatusId = schedulerStatusId,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		#endregion
	}
}