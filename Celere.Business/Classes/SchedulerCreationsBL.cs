﻿using Asteria.Business;
using Asteria.Business.Model;
using Asteria.Core.Tools;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;
using Celere.Business.Model;
using static Celere.Business.Enumerators;

namespace Celere.Business
{
	public class SchedulerCreationsBL : BaseBL
	{
		#region Constructor

		private string loggerName = "Celere.Business.SchedulerCreationsBL";
		
		/// <summary></summary>
		public SchedulerCreationsBL() : base()
		{
			ChangeLogger(loggerName);
		}
		
		/// <summary></summary>
		public SchedulerCreationsBL(SqlConnection conn, SqlTransaction transaction) : base(conn, transaction)
		{
			ChangeLogger(loggerName);
		}

		#endregion

		#region Selects

		/// <summary></summary>
		public async Task<PageResult<SchedulerCreationVM>> PageAsync(int page, int pageSize, string keyword, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageSchedulerCreations);

			return await PageAsync<SchedulerCreationVM>("_spSchedulerCreationsPage", page, new
			{
				Page = (page - 1),
				PageRows = pageSize,
				Keyword = keyword
			});
		}		

		/// <summary></summary>
		public async Task<IEnumerable<KeyValuePair<int, string>>> ListIdsAsync(int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageSchedulerCreations);

			return await ListAsync<KeyValuePair<int, string>>("_spSchedulerCreationsListIds");
		}

		/// <summary></summary>
		public async Task<SchedulerCreationVM> DetailAsync(int id, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageSchedulerCreations);

			var model = await SingleAsync<SchedulerCreationVM>("_spSchedulerCreationsGet", new
			{
				SchedulerCreationId = id
			});


			return model;
		}

		#endregion

		#region Handles

		/// <summary></summary>
		public async Task<SchedulerCreationVM> UpsertAsync(SchedulerCreationVM model, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageSchedulerCreations);

				model.SchedulerCreationId = await SingleAsync<int>("_spSchedulerCreationUpsert", parameters: new
				{
					SchedulerCreationId = model.SchedulerCreationId,
					DtReg = model.DtReg,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});

				return model;
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		/// <summary></summary>
		public async Task DeleteAsync(int schedulerCreationId, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageSchedulerCreations);
			
				await RunAsync("_spSchedulerCreationDelete", parameters: new {
					SchedulerCreationId = schedulerCreationId,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

        /// <summary>Generates the list of CPF/CNPJs to be monitorated at the present day</summary>
        public async Task GenerateAsync(int currentAuthId)
        {
            try
            {
                await CheckPermissionAsync(currentAuthId, EnumTask.ManageSchedulerCreations);

                await RunAsync("spCreateSchedulingForCodes");
            }
            catch (Exception EX)
            {
                throw EX;
            }
        }



        #endregion
    }
}