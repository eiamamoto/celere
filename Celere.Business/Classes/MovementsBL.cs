﻿using Asteria.Business;
using Asteria.Business.Model;
using Asteria.Core.Tools;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;
using Celere.Business.Model;
using static Celere.Business.Enumerators;

namespace Celere.Business
{
	public class MovementsBL : BaseBL
	{
		#region Constructor

		private string loggerName = "Celere.Business.MovementsBL";
		
		/// <summary></summary>
		public MovementsBL() : base()
		{
			ChangeLogger(loggerName);
		}
		
		/// <summary></summary>
		public MovementsBL(SqlConnection conn, SqlTransaction transaction) : base(conn, transaction)
		{
			ChangeLogger(loggerName);
		}

		#endregion

		#region Selects

		/// <summary></summary>
		public async Task<PageResult<MovementVM>> PageAsync(int page, int pageSize, string keyword, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageMovements);

			return await PageAsync<MovementVM>("_spMovementsPage", page, new
			{
				Page = (page - 1),
				PageRows = pageSize,
				Keyword = keyword
			});
		}		

		/// <summary></summary>
		public async Task<IEnumerable<KeyValuePair<int, string>>> ListIdsAsync(int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageMovements);

			return await ListAsync<KeyValuePair<int, string>>("_spMovementsListIds");
		}

		/// <summary></summary>
		public async Task<MovementVM> DetailAsync(int id, int currentAuthId)
		{
			await CheckPermissionAsync(currentAuthId, EnumTask.ManageMovements);

			var model = await SingleAsync<MovementVM>("_spMovementsGet", new
			{
				MovementId = id
			});


			return model;
		}

		#endregion

		#region Handles

		/// <summary></summary>
		public async Task<MovementVM> UpsertAsync(MovementVM model, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageMovements);

				model.MovementId = await SingleAsync<int>("_spMovementUpsert", parameters: new
				{
					MovementId = model.MovementId,
					ProcessId = model.ProcessId,
					DtReg = model.DtReg,
					DtMovement = model.DtMovement,
					Vara = model.Vara,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});

				return model;
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		/// <summary></summary>
		public async Task DeleteAsync(int movementId, int currentAuthId)
		{
			try
			{
				await CheckPermissionAsync(currentAuthId, EnumTask.ManageMovements);
			
				await RunAsync("_spMovementDelete", parameters: new {
					MovementId = movementId,
					__AuthId = currentAuthId,
					__AuthIP = HttpContext.Current.GetClientIPAddress()
				});
			}
			catch (Exception EX)
			{
				throw EX;
			}
		}

		#endregion
	}
}