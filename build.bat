﻿
@ECHO OFF
CLS

ECHO Configuring IIS...
ECHO(
ECHO Deletes existing website
"C:\Windows\System32\inetsrv\appcmd" delete site "public.api.celere.localhost"
"C:\Windows\System32\inetsrv\appcmd" delete site "private.api.celere.localhost"
"C:\Windows\System32\inetsrv\appcmd" delete site "controlpanel.celere.localhost"

ECHO(
ECHO Creates new websites for API and Control Panel
"C:\Windows\System32\inetsrv\appcmd" add site /name:"public.api.celere.localhost" /physicalPath:"%CD%\Celere.PublicApi" /bindings:"http://public.api.celere.localhost:80"
"C:\Windows\System32\inetsrv\appcmd" add site /name:"private.api.celere.localhost" /physicalPath:"%CD%\Celere.PrivateApi" /bindings:"http://private.api.celere.localhost:80"
"C:\Windows\System32\inetsrv\appcmd" add site /name:"controlpanel.celere.localhost" /physicalPath:"%CD%\Celere.ControlPanel" /bindings:"http://controlpanel.celere.localhost:80"

ECHO(
ECHO Restoring Nuget packages...
nuget restore

ECHO(
ECHO Compiling solution...
msbuild.exe Celere.sln /t:rebuild /verbosity:minimal /p:nowarn=1591

ECHO(
ECHO Restoring Control Panel NPM packages...
CD Celere.ControlPanel
CALL npm install


ECHO(
ECHO Building project...
CALL gulp build

ECHO(
ECHO Open hosts file using Notepad with administrator access and add the following lines to the file...
ECHO(
ECHO # Celere
ECHO 127.0.0.1	controlpanel.celere.localhost
ECHO 127.0.0.1	public.api.celere.localhost
ECHO 127.0.0.1	private.api.celere.localhost

%SystemRoot%\explorer.exe "C:\Windows\System32\drivers\etc"