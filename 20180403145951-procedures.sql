﻿USE Celere
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spAddressesPage'))
	DROP PROCEDURE _spAddressesPage;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spAddressesGet'))
	DROP PROCEDURE _spAddressesGet;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spAddressesListIds'))
	DROP PROCEDURE _spAddressesListIds;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spAddressUpsert'))
	DROP PROCEDURE _spAddressUpsert;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spAddressDelete'))
	DROP PROCEDURE _spAddressDelete;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spAttemptsPage'))
	DROP PROCEDURE _spAttemptsPage;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spAttemptsGet'))
	DROP PROCEDURE _spAttemptsGet;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spAttemptsListIds'))
	DROP PROCEDURE _spAttemptsListIds;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spAttemptUpsert'))
	DROP PROCEDURE _spAttemptUpsert;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spAttemptDelete'))
	DROP PROCEDURE _spAttemptDelete;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spCitiesPage'))
	DROP PROCEDURE _spCitiesPage;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spCitiesGet'))
	DROP PROCEDURE _spCitiesGet;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spCitiesListIds'))
	DROP PROCEDURE _spCitiesListIds;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spCityUpsert'))
	DROP PROCEDURE _spCityUpsert;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spCityDelete'))
	DROP PROCEDURE _spCityDelete;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spCustomersPage'))
	DROP PROCEDURE _spCustomersPage;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spCustomersGet'))
	DROP PROCEDURE _spCustomersGet;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spCustomersListIds'))
	DROP PROCEDURE _spCustomersListIds;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spCustomerUpsert'))
	DROP PROCEDURE _spCustomerUpsert;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spCustomerDelete'))
	DROP PROCEDURE _spCustomerDelete;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spLogTrackingCodesPage'))
	DROP PROCEDURE _spLogTrackingCodesPage;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spLogTrackingCodesGet'))
	DROP PROCEDURE _spLogTrackingCodesGet;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spLogTrackingCodesListIds'))
	DROP PROCEDURE _spLogTrackingCodesListIds;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spLogTrackingCodeUpsert'))
	DROP PROCEDURE _spLogTrackingCodeUpsert;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spLogTrackingCodeDelete'))
	DROP PROCEDURE _spLogTrackingCodeDelete;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spMovementsPage'))
	DROP PROCEDURE _spMovementsPage;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spMovementsGet'))
	DROP PROCEDURE _spMovementsGet;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spMovementsListIds'))
	DROP PROCEDURE _spMovementsListIds;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spMovementUpsert'))
	DROP PROCEDURE _spMovementUpsert;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spMovementDelete'))
	DROP PROCEDURE _spMovementDelete;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spNotificationsPage'))
	DROP PROCEDURE _spNotificationsPage;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spNotificationsGet'))
	DROP PROCEDURE _spNotificationsGet;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spNotificationsListIds'))
	DROP PROCEDURE _spNotificationsListIds;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spNotificationUpsert'))
	DROP PROCEDURE _spNotificationUpsert;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spNotificationDelete'))
	DROP PROCEDURE _spNotificationDelete;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spNotificationTypesPage'))
	DROP PROCEDURE _spNotificationTypesPage;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spNotificationTypesGet'))
	DROP PROCEDURE _spNotificationTypesGet;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spNotificationTypesListIds'))
	DROP PROCEDURE _spNotificationTypesListIds;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spNotificationTypeUpsert'))
	DROP PROCEDURE _spNotificationTypeUpsert;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spNotificationTypeDelete'))
	DROP PROCEDURE _spNotificationTypeDelete;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spOperationTypesPage'))
	DROP PROCEDURE _spOperationTypesPage;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spOperationTypesGet'))
	DROP PROCEDURE _spOperationTypesGet;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spOperationTypesListIds'))
	DROP PROCEDURE _spOperationTypesListIds;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spOperationTypeUpsert'))
	DROP PROCEDURE _spOperationTypeUpsert;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spOperationTypeDelete'))
	DROP PROCEDURE _spOperationTypeDelete;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spPartsPage'))
	DROP PROCEDURE _spPartsPage;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spPartsGet'))
	DROP PROCEDURE _spPartsGet;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spPartsListIds'))
	DROP PROCEDURE _spPartsListIds;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spPartUpsert'))
	DROP PROCEDURE _spPartUpsert;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spPartDelete'))
	DROP PROCEDURE _spPartDelete;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spPlansPage'))
	DROP PROCEDURE _spPlansPage;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spPlansGet'))
	DROP PROCEDURE _spPlansGet;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spPlansListIds'))
	DROP PROCEDURE _spPlansListIds;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spPlanUpsert'))
	DROP PROCEDURE _spPlanUpsert;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spPlanDelete'))
	DROP PROCEDURE _spPlanDelete;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spProcessesPage'))
	DROP PROCEDURE _spProcessesPage;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spProcessesGet'))
	DROP PROCEDURE _spProcessesGet;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spProcessesListIds'))
	DROP PROCEDURE _spProcessesListIds;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spProcessUpsert'))
	DROP PROCEDURE _spProcessUpsert;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spProcessDelete'))
	DROP PROCEDURE _spProcessDelete;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spSchedulerCreationsPage'))
	DROP PROCEDURE _spSchedulerCreationsPage;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spSchedulerCreationsGet'))
	DROP PROCEDURE _spSchedulerCreationsGet;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spSchedulerCreationsListIds'))
	DROP PROCEDURE _spSchedulerCreationsListIds;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spSchedulerCreationUpsert'))
	DROP PROCEDURE _spSchedulerCreationUpsert;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spSchedulerCreationDelete'))
	DROP PROCEDURE _spSchedulerCreationDelete;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spSchedulerStatusPage'))
	DROP PROCEDURE _spSchedulerStatusPage;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spSchedulerStatusGet'))
	DROP PROCEDURE _spSchedulerStatusGet;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spSchedulerStatusListIds'))
	DROP PROCEDURE _spSchedulerStatusListIds;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spSchedulerStatuUpsert'))
	DROP PROCEDURE _spSchedulerStatuUpsert;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spSchedulerStatuDelete'))
	DROP PROCEDURE _spSchedulerStatuDelete;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spSchedulerTrackingCodesPage'))
	DROP PROCEDURE _spSchedulerTrackingCodesPage;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spSchedulerTrackingCodesGet'))
	DROP PROCEDURE _spSchedulerTrackingCodesGet;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spSchedulerTrackingCodesListIds'))
	DROP PROCEDURE _spSchedulerTrackingCodesListIds;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spSchedulerTrackingCodeUpsert'))
	DROP PROCEDURE _spSchedulerTrackingCodeUpsert;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spSchedulerTrackingCodeDelete'))
	DROP PROCEDURE _spSchedulerTrackingCodeDelete;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spStatesPage'))
	DROP PROCEDURE _spStatesPage;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spStatesGet'))
	DROP PROCEDURE _spStatesGet;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spStatesListIds'))
	DROP PROCEDURE _spStatesListIds;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spStateUpsert'))
	DROP PROCEDURE _spStateUpsert;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spStateDelete'))
	DROP PROCEDURE _spStateDelete;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spTrackingCodesPage'))
	DROP PROCEDURE _spTrackingCodesPage;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spTrackingCodesGet'))
	DROP PROCEDURE _spTrackingCodesGet;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spTrackingCodesListIds'))
	DROP PROCEDURE _spTrackingCodesListIds;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spTrackingCodeUpsert'))
	DROP PROCEDURE _spTrackingCodeUpsert;
GO

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('_spTrackingCodeDelete'))
	DROP PROCEDURE _spTrackingCodeDelete;
GO


CREATE PROCEDURE _spAddressesPage

	@Page int = 0,
	@PageRows int = 1,
	@Keyword nvarchar(64) = NULL,
    @OrderBy nvarchar(64) = NULL,

    @AddressId INT = NULL,
    @CityId INT = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL DROP TABLE #TEMP;

	WITH TempResult AS (
		SELECT
			AddressId,
			Address1,
			Address2,
			Neighborhood,
			ZipCode,
			CityId,
			Lat,
			Lng,
			IsActive,
			Number
		FROM
			Addresses
		WHERE
			(@AddressId IS NULL OR AddressId = @AddressId)
		AND
			(@CityId IS NULL OR CityId = @CityId)
		AND
			(
				@Keyword IS NULL
			OR
				Address1 LIKE '%' + @Keyword + '%'
			OR
				Address2 LIKE '%' + @Keyword + '%'
			OR
				Neighborhood LIKE '%' + @Keyword + '%'
			OR
				ZipCode LIKE '%' + @Keyword + '%'
			OR
				Number LIKE '%' + @Keyword + '%'
			)
		AND
			IsActive = 1
	), TempCount AS (
		SELECT COUNT(*) AS MaxRows FROM TempResult
	)
	SELECT
		*
	INTO
		#TEMP
	FROM
		TempResult,
		TempCount
	ORDER BY
		TempResult.Address1 ASC
	OFFSET
		@Page * @PageRows ROWS
	FETCH NEXT
		@PageRows ROWS ONLY;

	SELECT * FROM #TEMP;

	IF EXISTS(SELECT TOP 1 MaxRows FROM #TEMP)
		SELECT TOP 1 MaxRows FROM #TEMP
	ELSE
		SELECT 0 AS MaxRows;

END

GO
CREATE PROCEDURE _spAddressesGet

    @AddressId INT

AS
BEGIN

	SELECT
		AddressId,
		Address1,
		Address2,
		Neighborhood,
		ZipCode,
		CityId,
		Lat,
		Lng,
		IsActive,
		Number
	FROM
		Addresses
	WHERE
		AddressId = @AddressId
		AND
			IsActive = 1

END

GO
CREATE PROCEDURE _spAddressesListIds

    @CityId INT = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		AddressId AS [Key],
		Address1 AS Value
	FROM
		Addresses
	WHERE
		(@CityId IS NULL OR CityId = @CityId)
	AND
		IsActive = 1
	ORDER BY
		Address1 ASC;

END
GO

CREATE PROCEDURE _spAddressUpsert

	@AddressId INT = 0,
	@Address1 NVARCHAR(64),
	@Address2 NVARCHAR(64) = NULL,
	@Neighborhood NVARCHAR(64) = NULL,
	@ZipCode NVARCHAR(16),
	@CityId INT,
	@Lat DECIMAL(10,7),
	@Lng DECIMAL(10,7),
	@IsActive BIT,
	@Number NVARCHAR(32) = NULL,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY
		IF @AddressId = 0
		BEGIN
			INSERT INTO Addresses
			(
				Address1,
				Address2,
				Neighborhood,
				ZipCode,
				CityId,
				Lat,
				Lng,
				IsActive,
				Number
			)
			VALUES
			(
				@Address1,
				@Address2,
				@Neighborhood,
				@ZipCode,
				@CityId,
				@Lat,
				@Lng,
				@IsActive,
				@Number
			);
		
			SET @AddressId = SCOPE_IDENTITY();

			EXEC __spTransactionInsert 'Addresses', @AddressId, 'INSERT', @__AuthId, @__AuthIP;
		END
		ELSE
		BEGIN
			UPDATE
				Addresses
			SET
				Address1 = @Address1,
				Address2 = @Address2,
				Neighborhood = @Neighborhood,
				ZipCode = @ZipCode,
				CityId = @CityId,
				Lat = @Lat,
				Lng = @Lng,
				IsActive = @IsActive,
				Number = @Number
			WHERE
				AddressId = @AddressId;

			EXEC __spTransactionInsert 'Addresses', @AddressId, 'UPDATE', @__AuthId, @__AuthIP;
		END

		SELECT @AddressId;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spAddressDelete

	@AddressId INT,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY

		UPDATE Addresses SET IsActive = 0 WHERE AddressId = @AddressId;

		EXEC __spTransactionInsert 'Addresses', @AddressId, 'DELETE', @__AuthId, @__AuthIP;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spAttemptsPage

	@Page int = 0,
	@PageRows int = 1,
	@Keyword nvarchar(64) = NULL,
    @OrderBy nvarchar(64) = NULL,

    @AttemptId INT = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL DROP TABLE #TEMP;

	WITH TempResult AS (
		SELECT
			AttemptId,
			NotificationId,
			DtReg,
			Succeded,
			Message
		FROM
			Attempts
		WHERE
			(@AttemptId IS NULL OR AttemptId = @AttemptId)
		AND
			(
				@Keyword IS NULL
			OR
				Message LIKE '%' + @Keyword + '%'
			)
	), TempCount AS (
		SELECT COUNT(*) AS MaxRows FROM TempResult
	)
	SELECT
		*
	INTO
		#TEMP
	FROM
		TempResult,
		TempCount
	ORDER BY
		TempResult.Message ASC
	OFFSET
		@Page * @PageRows ROWS
	FETCH NEXT
		@PageRows ROWS ONLY;

	SELECT * FROM #TEMP;

	IF EXISTS(SELECT TOP 1 MaxRows FROM #TEMP)
		SELECT TOP 1 MaxRows FROM #TEMP
	ELSE
		SELECT 0 AS MaxRows;

END

GO
CREATE PROCEDURE _spAttemptsGet

    @AttemptId INT

AS
BEGIN

	SELECT
		AttemptId,
		NotificationId,
		DtReg,
		Succeded,
		Message
	FROM
		Attempts
	WHERE
		AttemptId = @AttemptId

END

GO
CREATE PROCEDURE _spAttemptsListIds


AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		AttemptId AS [Key],
		Message AS Value
	FROM
		Attempts
	ORDER BY
		Message ASC;

END
GO

CREATE PROCEDURE _spAttemptUpsert

	@AttemptId INT = 0,
	@NotificationId INT = NULL,
	@DtReg DATETIME = NULL,
	@Succeded BIT = NULL,
	@Message NVARCHAR(1024) = NULL,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY
		IF @AttemptId = 0
		BEGIN
			INSERT INTO Attempts
			(
				NotificationId,
				DtReg,
				Succeded,
				Message
			)
			VALUES
			(
				@NotificationId,
				@DtReg,
				@Succeded,
				@Message
			);
		
			SET @AttemptId = SCOPE_IDENTITY();

			EXEC __spTransactionInsert 'Attempts', @AttemptId, 'INSERT', @__AuthId, @__AuthIP;
		END
		ELSE
		BEGIN
			UPDATE
				Attempts
			SET
				NotificationId = @NotificationId,
				DtReg = @DtReg,
				Succeded = @Succeded,
				Message = @Message
			WHERE
				AttemptId = @AttemptId;

			EXEC __spTransactionInsert 'Attempts', @AttemptId, 'UPDATE', @__AuthId, @__AuthIP;
		END

		SELECT @AttemptId;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spAttemptDelete

	@AttemptId INT,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY

		DELETE FROM Attempts WHERE AttemptId = @AttemptId;

		EXEC __spTransactionInsert 'Attempts', @AttemptId, 'DELETE', @__AuthId, @__AuthIP;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spCitiesPage

	@Page int = 0,
	@PageRows int = 1,
	@Keyword nvarchar(64) = NULL,
    @OrderBy nvarchar(64) = NULL,

    @CityId INT = NULL,
    @StateId INT = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL DROP TABLE #TEMP;

	WITH TempResult AS (
		SELECT
			CityId,
			Name,
			StateId,
			IsCapital
		FROM
			Cities
		WHERE
			(@CityId IS NULL OR CityId = @CityId)
		AND
			(@StateId IS NULL OR StateId = @StateId)
		AND
			(
				@Keyword IS NULL
			OR
				Name LIKE '%' + @Keyword + '%'
			)
	), TempCount AS (
		SELECT COUNT(*) AS MaxRows FROM TempResult
	)
	SELECT
		*
	INTO
		#TEMP
	FROM
		TempResult,
		TempCount
	ORDER BY
		TempResult.Name ASC
	OFFSET
		@Page * @PageRows ROWS
	FETCH NEXT
		@PageRows ROWS ONLY;

	SELECT * FROM #TEMP;

	IF EXISTS(SELECT TOP 1 MaxRows FROM #TEMP)
		SELECT TOP 1 MaxRows FROM #TEMP
	ELSE
		SELECT 0 AS MaxRows;

END

GO
CREATE PROCEDURE _spCitiesGet

    @CityId INT

AS
BEGIN

	SELECT
		CityId,
		Name,
		StateId,
		IsCapital
	FROM
		Cities
	WHERE
		CityId = @CityId

END

GO
CREATE PROCEDURE _spCitiesListIds

    @StateId INT = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		CityId AS [Key],
		Name AS Value
	FROM
		Cities
	WHERE
		(@StateId IS NULL OR StateId = @StateId)
	ORDER BY
		Name ASC;

END
GO

CREATE PROCEDURE _spCityUpsert

	@CityId INT = 0,
	@Name NVARCHAR(64),
	@StateId INT,
	@IsCapital BIT,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY
		IF @CityId = 0
		BEGIN
			INSERT INTO Cities
			(
				Name,
				StateId,
				IsCapital
			)
			VALUES
			(
				@Name,
				@StateId,
				@IsCapital
			);
		
			SET @CityId = SCOPE_IDENTITY();

			EXEC __spTransactionInsert 'Cities', @CityId, 'INSERT', @__AuthId, @__AuthIP;
		END
		ELSE
		BEGIN
			UPDATE
				Cities
			SET
				Name = @Name,
				StateId = @StateId,
				IsCapital = @IsCapital
			WHERE
				CityId = @CityId;

			EXEC __spTransactionInsert 'Cities', @CityId, 'UPDATE', @__AuthId, @__AuthIP;
		END

		SELECT @CityId;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spCityDelete

	@CityId INT,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY

		DELETE FROM Cities WHERE CityId = @CityId;

		EXEC __spTransactionInsert 'Cities', @CityId, 'DELETE', @__AuthId, @__AuthIP;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spCustomersPage

	@Page int = 0,
	@PageRows int = 1,
	@Keyword nvarchar(64) = NULL,
    @OrderBy nvarchar(64) = NULL,

    @CustomerId INT = NULL,
    @AuthId INT = NULL,
    @AddressId INT = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL DROP TABLE #TEMP;

	WITH TempResult AS (
		SELECT
			CustomerId,
			AuthId,
			DtReg,
			Name,
			AddressId,
			Phone,
			Cel,
			CpfCnpj
		FROM
			Customers
		WHERE
			(@CustomerId IS NULL OR CustomerId = @CustomerId)
		AND
			(@AuthId IS NULL OR AuthId = @AuthId)
		AND
			(@AddressId IS NULL OR AddressId = @AddressId)
		AND
			(
				@Keyword IS NULL
			OR
				Name LIKE '%' + @Keyword + '%'
			OR
				Cel LIKE '%' + @Keyword + '%'
			)
	), TempCount AS (
		SELECT COUNT(*) AS MaxRows FROM TempResult
	)
	SELECT
		*
	INTO
		#TEMP
	FROM
		TempResult,
		TempCount
	ORDER BY
		TempResult.Name ASC
	OFFSET
		@Page * @PageRows ROWS
	FETCH NEXT
		@PageRows ROWS ONLY;

	SELECT * FROM #TEMP;

	IF EXISTS(SELECT TOP 1 MaxRows FROM #TEMP)
		SELECT TOP 1 MaxRows FROM #TEMP
	ELSE
		SELECT 0 AS MaxRows;

END

GO
CREATE PROCEDURE _spCustomersGet

    @CustomerId INT

AS
BEGIN

	SELECT
		CustomerId,
		AuthId,
		DtReg,
		Name,
		AddressId,
		Phone,
		Cel,
		CpfCnpj
	FROM
		Customers
	WHERE
		CustomerId = @CustomerId

END

GO
CREATE PROCEDURE _spCustomersListIds

    @AuthId INT = NULL,
    @AddressId INT = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		CustomerId AS [Key],
		Name AS Value
	FROM
		Customers
	WHERE
		(@AuthId IS NULL OR AuthId = @AuthId)
	AND
		(@AddressId IS NULL OR AddressId = @AddressId)
	ORDER BY
		Name ASC;

END
GO

CREATE PROCEDURE _spCustomerUpsert

	@CustomerId INT = 0,
	@AuthId INT,
	@DtReg DATETIME = NULL,
	@Name NVARCHAR(128) = NULL,
	@AddressId INT = NULL,
	@Phone NVARCHAR(20) = NULL,
	@Cel NVARCHAR(20) = NULL,
	@CpfCnpj NVARCHAR(16) = NULL,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY
		IF @CustomerId = 0
		BEGIN
			INSERT INTO Customers
			(
				AuthId,
				DtReg,
				Name,
				AddressId,
				Phone,
				Cel,
				CpfCnpj
			)
			VALUES
			(
				@AuthId,
				@DtReg,
				@Name,
				@AddressId,
				@Phone,
				@Cel,
				@CpfCnpj
			);
		
			SET @CustomerId = SCOPE_IDENTITY();

			EXEC __spTransactionInsert 'Customers', @CustomerId, 'INSERT', @__AuthId, @__AuthIP;
		END
		ELSE
		BEGIN
			UPDATE
				Customers
			SET
				AuthId = @AuthId,
				DtReg = @DtReg,
				Name = @Name,
				AddressId = @AddressId,
				Phone = @Phone,
				Cel = @Cel,
				CpfCnpj = @CpfCnpj
			WHERE
				CustomerId = @CustomerId;

			EXEC __spTransactionInsert 'Customers', @CustomerId, 'UPDATE', @__AuthId, @__AuthIP;
		END

		SELECT @CustomerId;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spCustomerDelete

	@CustomerId INT,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY

		DELETE FROM Customers WHERE CustomerId = @CustomerId;

		EXEC __spTransactionInsert 'Customers', @CustomerId, 'DELETE', @__AuthId, @__AuthIP;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spLogTrackingCodesPage

	@Page int = 0,
	@PageRows int = 1,
	@Keyword nvarchar(64) = NULL,
    @OrderBy nvarchar(64) = NULL,

    @LogId INT = NULL,
    @OperationTypeId INT = NULL,
    @TrackingCodeId INT = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL DROP TABLE #TEMP;

	WITH TempResult AS (
		SELECT
			LogId,
			DtReg,
			OperationTypeId,
			TrackingCodeId
		FROM
			LogTrackingCodes
		WHERE
			(@LogId IS NULL OR LogId = @LogId)
		AND
			(@OperationTypeId IS NULL OR OperationTypeId = @OperationTypeId)
		AND
			(@TrackingCodeId IS NULL OR TrackingCodeId = @TrackingCodeId)
	), TempCount AS (
		SELECT COUNT(*) AS MaxRows FROM TempResult
	)
	SELECT
		*
	INTO
		#TEMP
	FROM
		TempResult,
		TempCount
	ORDER BY
		TempResult.LogId ASC
	OFFSET
		@Page * @PageRows ROWS
	FETCH NEXT
		@PageRows ROWS ONLY;

	SELECT * FROM #TEMP;

	IF EXISTS(SELECT TOP 1 MaxRows FROM #TEMP)
		SELECT TOP 1 MaxRows FROM #TEMP
	ELSE
		SELECT 0 AS MaxRows;

END

GO
CREATE PROCEDURE _spLogTrackingCodesGet

    @LogId INT

AS
BEGIN

	SELECT
		LogId,
		DtReg,
		OperationTypeId,
		TrackingCodeId
	FROM
		LogTrackingCodes
	WHERE
		LogId = @LogId

END

GO
CREATE PROCEDURE _spLogTrackingCodesListIds

    @OperationTypeId INT = NULL,
    @TrackingCodeId INT = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		LogId AS [Key],
		LogId AS Value
	FROM
		LogTrackingCodes
	WHERE
		(@OperationTypeId IS NULL OR OperationTypeId = @OperationTypeId)
	AND
		(@TrackingCodeId IS NULL OR TrackingCodeId = @TrackingCodeId)
	ORDER BY
		LogId ASC;

END
GO

CREATE PROCEDURE _spLogTrackingCodeUpsert

	@LogId INT = 0,
	@DtReg DATETIME = NULL,
	@OperationTypeId INT = NULL,
	@TrackingCodeId INT = NULL,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY
		IF @LogId = 0
		BEGIN
			INSERT INTO LogTrackingCodes
			(
				DtReg,
				OperationTypeId,
				TrackingCodeId
			)
			VALUES
			(
				@DtReg,
				@OperationTypeId,
				@TrackingCodeId
			);
		
			SET @LogId = SCOPE_IDENTITY();

			EXEC __spTransactionInsert 'LogTrackingCodes', @LogId, 'INSERT', @__AuthId, @__AuthIP;
		END
		ELSE
		BEGIN
			UPDATE
				LogTrackingCodes
			SET
				DtReg = @DtReg,
				OperationTypeId = @OperationTypeId,
				TrackingCodeId = @TrackingCodeId
			WHERE
				LogId = @LogId;

			EXEC __spTransactionInsert 'LogTrackingCodes', @LogId, 'UPDATE', @__AuthId, @__AuthIP;
		END

		SELECT @LogId;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spLogTrackingCodeDelete

	@LogId INT,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY

		DELETE FROM LogTrackingCodes WHERE LogId = @LogId;

		EXEC __spTransactionInsert 'LogTrackingCodes', @LogId, 'DELETE', @__AuthId, @__AuthIP;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spMovementsPage

	@Page int = 0,
	@PageRows int = 1,
	@Keyword nvarchar(64) = NULL,
    @OrderBy nvarchar(64) = NULL,

    @MovementId INT = NULL,
    @ProcessId VARCHAR(20) = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL DROP TABLE #TEMP;

	WITH TempResult AS (
		SELECT
			MovementId,
			ProcessId,
			DtReg,
			DtMovement,
			Vara
		FROM
			Movements
		WHERE
			(@MovementId IS NULL OR MovementId = @MovementId)
		AND
			(@ProcessId IS NULL OR ProcessId = @ProcessId)
		AND
			(
				@Keyword IS NULL
			OR
				Vara LIKE '%' + @Keyword + '%'
			)
	), TempCount AS (
		SELECT COUNT(*) AS MaxRows FROM TempResult
	)
	SELECT
		*
	INTO
		#TEMP
	FROM
		TempResult,
		TempCount
	ORDER BY
		TempResult.ProcessId ASC
	OFFSET
		@Page * @PageRows ROWS
	FETCH NEXT
		@PageRows ROWS ONLY;

	SELECT * FROM #TEMP;

	IF EXISTS(SELECT TOP 1 MaxRows FROM #TEMP)
		SELECT TOP 1 MaxRows FROM #TEMP
	ELSE
		SELECT 0 AS MaxRows;

END

GO
CREATE PROCEDURE _spMovementsGet

    @MovementId INT

AS
BEGIN

	SELECT
		MovementId,
		ProcessId,
		DtReg,
		DtMovement,
		Vara
	FROM
		Movements
	WHERE
		MovementId = @MovementId

END

GO
CREATE PROCEDURE _spMovementsListIds

    @ProcessId VARCHAR(20) = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		MovementId AS [Key],
		ProcessId AS Value
	FROM
		Movements
	WHERE
		(@ProcessId IS NULL OR ProcessId = @ProcessId)
	ORDER BY
		ProcessId ASC;

END
GO

CREATE PROCEDURE _spMovementUpsert

	@MovementId INT = 0,
	@ProcessId VARCHAR(20) = NULL,
	@DtReg DATETIME = NULL,
	@DtMovement DATETIME = NULL,
	@Vara NVARCHAR(128) = NULL,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY
		IF @MovementId = 0
		BEGIN
			INSERT INTO Movements
			(
				ProcessId,
				DtReg,
				DtMovement,
				Vara
			)
			VALUES
			(
				@ProcessId,
				@DtReg,
				@DtMovement,
				@Vara
			);
		
			SET @MovementId = SCOPE_IDENTITY();

			EXEC __spTransactionInsert 'Movements', @MovementId, 'INSERT', @__AuthId, @__AuthIP;
		END
		ELSE
		BEGIN
			UPDATE
				Movements
			SET
				ProcessId = @ProcessId,
				DtReg = @DtReg,
				DtMovement = @DtMovement,
				Vara = @Vara
			WHERE
				MovementId = @MovementId;

			EXEC __spTransactionInsert 'Movements', @MovementId, 'UPDATE', @__AuthId, @__AuthIP;
		END

		SELECT @MovementId;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spMovementDelete

	@MovementId INT,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY

		DELETE FROM Movements WHERE MovementId = @MovementId;

		EXEC __spTransactionInsert 'Movements', @MovementId, 'DELETE', @__AuthId, @__AuthIP;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spNotificationsPage

	@Page int = 0,
	@PageRows int = 1,
	@Keyword nvarchar(64) = NULL,
    @OrderBy nvarchar(64) = NULL,

    @NotificationId INT = NULL,
    @ProcessId VARCHAR(20) = NULL,
    @CustomerId INT = NULL,
    @NotificationTypeId INT = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL DROP TABLE #TEMP;

	WITH TempResult AS (
		SELECT
			NotificationId,
			DtReg,
			Succeded,
			DtSucceded,
			ProcessId,
			CustomerId,
			NotificationTypeId
		FROM
			Notifications
		WHERE
			(@NotificationId IS NULL OR NotificationId = @NotificationId)
		AND
			(@ProcessId IS NULL OR ProcessId = @ProcessId)
		AND
			(@CustomerId IS NULL OR CustomerId = @CustomerId)
		AND
			(@NotificationTypeId IS NULL OR NotificationTypeId = @NotificationTypeId)
	), TempCount AS (
		SELECT COUNT(*) AS MaxRows FROM TempResult
	)
	SELECT
		*
	INTO
		#TEMP
	FROM
		TempResult,
		TempCount
	ORDER BY
		TempResult.ProcessId ASC
	OFFSET
		@Page * @PageRows ROWS
	FETCH NEXT
		@PageRows ROWS ONLY;

	SELECT * FROM #TEMP;

	IF EXISTS(SELECT TOP 1 MaxRows FROM #TEMP)
		SELECT TOP 1 MaxRows FROM #TEMP
	ELSE
		SELECT 0 AS MaxRows;

END

GO
CREATE PROCEDURE _spNotificationsGet

    @NotificationId INT

AS
BEGIN

	SELECT
		NotificationId,
		DtReg,
		Succeded,
		DtSucceded,
		ProcessId,
		CustomerId,
		NotificationTypeId
	FROM
		Notifications
	WHERE
		NotificationId = @NotificationId

END

GO
CREATE PROCEDURE _spNotificationsListIds

    @ProcessId VARCHAR(20) = NULL,
    @CustomerId INT = NULL,
    @NotificationTypeId INT = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		NotificationId AS [Key],
		ProcessId AS Value
	FROM
		Notifications
	WHERE
		(@ProcessId IS NULL OR ProcessId = @ProcessId)
	AND
		(@CustomerId IS NULL OR CustomerId = @CustomerId)
	AND
		(@NotificationTypeId IS NULL OR NotificationTypeId = @NotificationTypeId)
	ORDER BY
		ProcessId ASC;

END
GO

CREATE PROCEDURE _spNotificationUpsert

	@NotificationId INT = 0,
	@DtReg DATETIME = NULL,
	@Succeded BIT = NULL,
	@DtSucceded DATETIME = NULL,
	@ProcessId VARCHAR(20) = NULL,
	@CustomerId INT = NULL,
	@NotificationTypeId INT = NULL,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY
		IF @NotificationId = 0
		BEGIN
			INSERT INTO Notifications
			(
				DtReg,
				Succeded,
				DtSucceded,
				ProcessId,
				CustomerId,
				NotificationTypeId
			)
			VALUES
			(
				@DtReg,
				@Succeded,
				@DtSucceded,
				@ProcessId,
				@CustomerId,
				@NotificationTypeId
			);
		
			SET @NotificationId = SCOPE_IDENTITY();

			EXEC __spTransactionInsert 'Notifications', @NotificationId, 'INSERT', @__AuthId, @__AuthIP;
		END
		ELSE
		BEGIN
			UPDATE
				Notifications
			SET
				DtReg = @DtReg,
				Succeded = @Succeded,
				DtSucceded = @DtSucceded,
				ProcessId = @ProcessId,
				CustomerId = @CustomerId,
				NotificationTypeId = @NotificationTypeId
			WHERE
				NotificationId = @NotificationId;

			EXEC __spTransactionInsert 'Notifications', @NotificationId, 'UPDATE', @__AuthId, @__AuthIP;
		END

		SELECT @NotificationId;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spNotificationDelete

	@NotificationId INT,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY

		DELETE FROM Notifications WHERE NotificationId = @NotificationId;

		EXEC __spTransactionInsert 'Notifications', @NotificationId, 'DELETE', @__AuthId, @__AuthIP;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spNotificationTypesPage

	@Page int = 0,
	@PageRows int = 1,
	@Keyword nvarchar(64) = NULL,
    @OrderBy nvarchar(64) = NULL,

    @NotificationTypeId INT = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL DROP TABLE #TEMP;

	WITH TempResult AS (
		SELECT
			NotificationTypeId,
			NotificationType
		FROM
			NotificationTypes
		WHERE
			(@NotificationTypeId IS NULL OR NotificationTypeId = @NotificationTypeId)
		AND
			(
				@Keyword IS NULL
			OR
				NotificationType LIKE '%' + @Keyword + '%'
			)
	), TempCount AS (
		SELECT COUNT(*) AS MaxRows FROM TempResult
	)
	SELECT
		*
	INTO
		#TEMP
	FROM
		TempResult,
		TempCount
	ORDER BY
		TempResult.NotificationType ASC
	OFFSET
		@Page * @PageRows ROWS
	FETCH NEXT
		@PageRows ROWS ONLY;

	SELECT * FROM #TEMP;

	IF EXISTS(SELECT TOP 1 MaxRows FROM #TEMP)
		SELECT TOP 1 MaxRows FROM #TEMP
	ELSE
		SELECT 0 AS MaxRows;

END

GO
CREATE PROCEDURE _spNotificationTypesGet

    @NotificationTypeId INT

AS
BEGIN

	SELECT
		NotificationTypeId,
		NotificationType
	FROM
		NotificationTypes
	WHERE
		NotificationTypeId = @NotificationTypeId

END

GO
CREATE PROCEDURE _spNotificationTypesListIds


AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		NotificationTypeId AS [Key],
		NotificationType AS Value
	FROM
		NotificationTypes
	ORDER BY
		NotificationType ASC;

END
GO

CREATE PROCEDURE _spNotificationTypeUpsert

	@NotificationTypeId INT = 0,
	@NotificationType VARCHAR(32) = NULL,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY
		IF @NotificationTypeId = 0
		BEGIN
			INSERT INTO NotificationTypes
			(
				NotificationType
			)
			VALUES
			(
				@NotificationType
			);
		
			SET @NotificationTypeId = SCOPE_IDENTITY();

			EXEC __spTransactionInsert 'NotificationTypes', @NotificationTypeId, 'INSERT', @__AuthId, @__AuthIP;
		END
		ELSE
		BEGIN
			UPDATE
				NotificationTypes
			SET
				NotificationType = @NotificationType
			WHERE
				NotificationTypeId = @NotificationTypeId;

			EXEC __spTransactionInsert 'NotificationTypes', @NotificationTypeId, 'UPDATE', @__AuthId, @__AuthIP;
		END

		SELECT @NotificationTypeId;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spNotificationTypeDelete

	@NotificationTypeId INT,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY

		DELETE FROM NotificationTypes WHERE NotificationTypeId = @NotificationTypeId;

		EXEC __spTransactionInsert 'NotificationTypes', @NotificationTypeId, 'DELETE', @__AuthId, @__AuthIP;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spOperationTypesPage

	@Page int = 0,
	@PageRows int = 1,
	@Keyword nvarchar(64) = NULL,
    @OrderBy nvarchar(64) = NULL,

    @OperationTypeId INT = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL DROP TABLE #TEMP;

	WITH TempResult AS (
		SELECT
			OperationTypeId,
			Name
		FROM
			OperationTypes
		WHERE
			(@OperationTypeId IS NULL OR OperationTypeId = @OperationTypeId)
		AND
			(
				@Keyword IS NULL
			OR
				Name LIKE '%' + @Keyword + '%'
			)
	), TempCount AS (
		SELECT COUNT(*) AS MaxRows FROM TempResult
	)
	SELECT
		*
	INTO
		#TEMP
	FROM
		TempResult,
		TempCount
	ORDER BY
		TempResult.Name ASC
	OFFSET
		@Page * @PageRows ROWS
	FETCH NEXT
		@PageRows ROWS ONLY;

	SELECT * FROM #TEMP;

	IF EXISTS(SELECT TOP 1 MaxRows FROM #TEMP)
		SELECT TOP 1 MaxRows FROM #TEMP
	ELSE
		SELECT 0 AS MaxRows;

END

GO
CREATE PROCEDURE _spOperationTypesGet

    @OperationTypeId INT

AS
BEGIN

	SELECT
		OperationTypeId,
		Name
	FROM
		OperationTypes
	WHERE
		OperationTypeId = @OperationTypeId

END

GO
CREATE PROCEDURE _spOperationTypesListIds


AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		OperationTypeId AS [Key],
		Name AS Value
	FROM
		OperationTypes
	ORDER BY
		Name ASC;

END
GO

CREATE PROCEDURE _spOperationTypeUpsert

	@OperationTypeId INT = 0,
	@Name VARCHAR(32) = NULL,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY
		IF @OperationTypeId = 0
		BEGIN
			INSERT INTO OperationTypes
			(
				Name
			)
			VALUES
			(
				@Name
			);
		
			SET @OperationTypeId = SCOPE_IDENTITY();

			EXEC __spTransactionInsert 'OperationTypes', @OperationTypeId, 'INSERT', @__AuthId, @__AuthIP;
		END
		ELSE
		BEGIN
			UPDATE
				OperationTypes
			SET
				Name = @Name
			WHERE
				OperationTypeId = @OperationTypeId;

			EXEC __spTransactionInsert 'OperationTypes', @OperationTypeId, 'UPDATE', @__AuthId, @__AuthIP;
		END

		SELECT @OperationTypeId;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spOperationTypeDelete

	@OperationTypeId INT,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY

		DELETE FROM OperationTypes WHERE OperationTypeId = @OperationTypeId;

		EXEC __spTransactionInsert 'OperationTypes', @OperationTypeId, 'DELETE', @__AuthId, @__AuthIP;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spPartsPage

	@Page int = 0,
	@PageRows int = 1,
	@Keyword nvarchar(64) = NULL,
    @OrderBy nvarchar(64) = NULL,

    @PartId INT = NULL,
    @ProcessId VARCHAR(20) = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL DROP TABLE #TEMP;

	WITH TempResult AS (
		SELECT
			PartId,
			DtReg,
			ProcessId,
			Name
		FROM
			Parts
		WHERE
			(@PartId IS NULL OR PartId = @PartId)
		AND
			(@ProcessId IS NULL OR ProcessId = @ProcessId)
		AND
			(
				@Keyword IS NULL
			OR
				Name LIKE '%' + @Keyword + '%'
			)
	), TempCount AS (
		SELECT COUNT(*) AS MaxRows FROM TempResult
	)
	SELECT
		*
	INTO
		#TEMP
	FROM
		TempResult,
		TempCount
	ORDER BY
		TempResult.ProcessId ASC
	OFFSET
		@Page * @PageRows ROWS
	FETCH NEXT
		@PageRows ROWS ONLY;

	SELECT * FROM #TEMP;

	IF EXISTS(SELECT TOP 1 MaxRows FROM #TEMP)
		SELECT TOP 1 MaxRows FROM #TEMP
	ELSE
		SELECT 0 AS MaxRows;

END

GO
CREATE PROCEDURE _spPartsGet

    @PartId INT

AS
BEGIN

	SELECT
		PartId,
		DtReg,
		ProcessId,
		Name
	FROM
		Parts
	WHERE
		PartId = @PartId

END

GO
CREATE PROCEDURE _spPartsListIds

    @ProcessId VARCHAR(20) = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		PartId AS [Key],
		ProcessId AS Value
	FROM
		Parts
	WHERE
		(@ProcessId IS NULL OR ProcessId = @ProcessId)
	ORDER BY
		ProcessId ASC;

END
GO

CREATE PROCEDURE _spPartUpsert

	@PartId INT = 0,
	@DtReg DATETIME = NULL,
	@ProcessId VARCHAR(20) = NULL,
	@Name NVARCHAR(128) = NULL,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY
		IF @PartId = 0
		BEGIN
			INSERT INTO Parts
			(
				DtReg,
				ProcessId,
				Name
			)
			VALUES
			(
				@DtReg,
				@ProcessId,
				@Name
			);
		
			SET @PartId = SCOPE_IDENTITY();

			EXEC __spTransactionInsert 'Parts', @PartId, 'INSERT', @__AuthId, @__AuthIP;
		END
		ELSE
		BEGIN
			UPDATE
				Parts
			SET
				DtReg = @DtReg,
				ProcessId = @ProcessId,
				Name = @Name
			WHERE
				PartId = @PartId;

			EXEC __spTransactionInsert 'Parts', @PartId, 'UPDATE', @__AuthId, @__AuthIP;
		END

		SELECT @PartId;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spPartDelete

	@PartId INT,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY

		DELETE FROM Parts WHERE PartId = @PartId;

		EXEC __spTransactionInsert 'Parts', @PartId, 'DELETE', @__AuthId, @__AuthIP;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spPlansPage

	@Page int = 0,
	@PageRows int = 1,
	@Keyword nvarchar(64) = NULL,
    @OrderBy nvarchar(64) = NULL,

    @PlanId INT = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL DROP TABLE #TEMP;

	WITH TempResult AS (
		SELECT
			PlanId,
			DtReg,
			Quantity,
			PlanName,
			IsActive,
			Cost,
			CostExtra
		FROM
			Plans
		WHERE
			(@PlanId IS NULL OR PlanId = @PlanId)
		AND
			(
				@Keyword IS NULL
			OR
				PlanName LIKE '%' + @Keyword + '%'
			)
		AND
			IsActive = 1
	), TempCount AS (
		SELECT COUNT(*) AS MaxRows FROM TempResult
	)
	SELECT
		*
	INTO
		#TEMP
	FROM
		TempResult,
		TempCount
	ORDER BY
		TempResult.PlanName ASC
	OFFSET
		@Page * @PageRows ROWS
	FETCH NEXT
		@PageRows ROWS ONLY;

	SELECT * FROM #TEMP;

	IF EXISTS(SELECT TOP 1 MaxRows FROM #TEMP)
		SELECT TOP 1 MaxRows FROM #TEMP
	ELSE
		SELECT 0 AS MaxRows;

END

GO
CREATE PROCEDURE _spPlansGet

    @PlanId INT

AS
BEGIN

	SELECT
		PlanId,
		DtReg,
		Quantity,
		PlanName,
		IsActive,
		Cost,
		CostExtra
	FROM
		Plans
	WHERE
		PlanId = @PlanId
		AND
			IsActive = 1

END

GO
CREATE PROCEDURE _spPlansListIds


AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		PlanId AS [Key],
		PlanName AS Value
	FROM
		Plans
	WHERE
		IsActive = 1
	ORDER BY
		PlanName ASC;

END
GO

CREATE PROCEDURE _spPlanUpsert

	@PlanId INT = 0,
	@DtReg DATETIME = NULL,
	@Quantity INT = NULL,
	@PlanName NVARCHAR(32) = NULL,
	@IsActive BIT = NULL,
	@Cost DECIMAL(10,2) = NULL,
	@CostExtra DECIMAL(10,2) = NULL,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY
		IF @PlanId = 0
		BEGIN
			INSERT INTO Plans
			(
				DtReg,
				Quantity,
				PlanName,
				IsActive,
				Cost,
				CostExtra
			)
			VALUES
			(
				@DtReg,
				@Quantity,
				@PlanName,
				@IsActive,
				@Cost,
				@CostExtra
			);
		
			SET @PlanId = SCOPE_IDENTITY();

			EXEC __spTransactionInsert 'Plans', @PlanId, 'INSERT', @__AuthId, @__AuthIP;
		END
		ELSE
		BEGIN
			UPDATE
				Plans
			SET
				DtReg = @DtReg,
				Quantity = @Quantity,
				PlanName = @PlanName,
				IsActive = @IsActive,
				Cost = @Cost,
				CostExtra = @CostExtra
			WHERE
				PlanId = @PlanId;

			EXEC __spTransactionInsert 'Plans', @PlanId, 'UPDATE', @__AuthId, @__AuthIP;
		END

		SELECT @PlanId;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spPlanDelete

	@PlanId INT,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY

		UPDATE Plans SET IsActive = 0 WHERE PlanId = @PlanId;

		EXEC __spTransactionInsert 'Plans', @PlanId, 'DELETE', @__AuthId, @__AuthIP;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spProcessesPage

	@Page int = 0,
	@PageRows int = 1,
	@Keyword nvarchar(64) = NULL,
    @OrderBy nvarchar(64) = NULL,

    @ProcessId VARCHAR(20) = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL DROP TABLE #TEMP;

	WITH TempResult AS (
		SELECT
			ProcessId,
			DtReg,
			DtDistribution,
			Value,
			Vara,
			CpfCnpj,
			HasNotifications
		FROM
			Processes
		WHERE
			(@ProcessId IS NULL OR ProcessId = @ProcessId)
		AND
			(
				@Keyword IS NULL
			OR
				ProcessId LIKE '%' + @Keyword + '%'
			OR
				Vara LIKE '%' + @Keyword + '%'
			)
	), TempCount AS (
		SELECT COUNT(*) AS MaxRows FROM TempResult
	)
	SELECT
		*
	INTO
		#TEMP
	FROM
		TempResult,
		TempCount
	ORDER BY
		TempResult.ProcessId ASC
	OFFSET
		@Page * @PageRows ROWS
	FETCH NEXT
		@PageRows ROWS ONLY;

	SELECT * FROM #TEMP;

	IF EXISTS(SELECT TOP 1 MaxRows FROM #TEMP)
		SELECT TOP 1 MaxRows FROM #TEMP
	ELSE
		SELECT 0 AS MaxRows;

END

GO
CREATE PROCEDURE _spProcessesGet

    @ProcessId VARCHAR(20)

AS
BEGIN

	SELECT
		ProcessId,
		DtReg,
		DtDistribution,
		Value,
		Vara,
		CpfCnpj,
		HasNotifications
	FROM
		Processes
	WHERE
		ProcessId = @ProcessId

END

GO
CREATE PROCEDURE _spProcessesListIds


AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		ProcessId AS [Key],
		ProcessId AS Value
	FROM
		Processes
	ORDER BY
		ProcessId ASC;

END
GO

CREATE PROCEDURE _spProcessUpsert

	@ProcessId VARCHAR(20) = 0,
	@DtReg DATETIME = NULL,
	@DtDistribution DATETIME = NULL,
	@Value DECIMAL(16,2) = NULL,
	@Vara NVARCHAR(128) = NULL,
	@CpfCnpj NVARCHAR(16) = NULL,
	@HasNotifications BIT = NULL,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY
		IF @ProcessId = 0
		BEGIN
			INSERT INTO Processes
			(
				DtReg,
				DtDistribution,
				Value,
				Vara,
				CpfCnpj,
				HasNotifications
			)
			VALUES
			(
				@DtReg,
				@DtDistribution,
				@Value,
				@Vara,
				@CpfCnpj,
				@HasNotifications
			);
		
			SET @ProcessId = SCOPE_IDENTITY();

			EXEC __spTransactionInsert 'Processes', @ProcessId, 'INSERT', @__AuthId, @__AuthIP;
		END
		ELSE
		BEGIN
			UPDATE
				Processes
			SET
				DtReg = @DtReg,
				DtDistribution = @DtDistribution,
				Value = @Value,
				Vara = @Vara,
				CpfCnpj = @CpfCnpj,
				HasNotifications = @HasNotifications
			WHERE
				ProcessId = @ProcessId;

			EXEC __spTransactionInsert 'Processes', @ProcessId, 'UPDATE', @__AuthId, @__AuthIP;
		END

		SELECT @ProcessId;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spProcessDelete

	@ProcessId VARCHAR(20),

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY

		DELETE FROM Processes WHERE ProcessId = @ProcessId;

		EXEC __spTransactionInsert 'Processes', @ProcessId, 'DELETE', @__AuthId, @__AuthIP;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spSchedulerCreationsPage

	@Page int = 0,
	@PageRows int = 1,
	@Keyword nvarchar(64) = NULL,
    @OrderBy nvarchar(64) = NULL,

    @SchedulerCreationId INT = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL DROP TABLE #TEMP;

	WITH TempResult AS (
		SELECT
			SchedulerCreationId,
			DtReg
		FROM
			SchedulerCreations
		WHERE
			(@SchedulerCreationId IS NULL OR SchedulerCreationId = @SchedulerCreationId)
	), TempCount AS (
		SELECT COUNT(*) AS MaxRows FROM TempResult
	)
	SELECT
		*
	INTO
		#TEMP
	FROM
		TempResult,
		TempCount
	ORDER BY
		TempResult.SchedulerCreationId ASC
	OFFSET
		@Page * @PageRows ROWS
	FETCH NEXT
		@PageRows ROWS ONLY;

	SELECT * FROM #TEMP;

	IF EXISTS(SELECT TOP 1 MaxRows FROM #TEMP)
		SELECT TOP 1 MaxRows FROM #TEMP
	ELSE
		SELECT 0 AS MaxRows;

END

GO
CREATE PROCEDURE _spSchedulerCreationsGet

    @SchedulerCreationId INT

AS
BEGIN

	SELECT
		SchedulerCreationId,
		DtReg
	FROM
		SchedulerCreations
	WHERE
		SchedulerCreationId = @SchedulerCreationId

END

GO
CREATE PROCEDURE _spSchedulerCreationsListIds


AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		SchedulerCreationId AS [Key],
		SchedulerCreationId AS Value
	FROM
		SchedulerCreations
	ORDER BY
		SchedulerCreationId ASC;

END
GO

CREATE PROCEDURE _spSchedulerCreationUpsert

	@SchedulerCreationId INT = 0,
	@DtReg DATETIME = NULL,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY
		IF @SchedulerCreationId = 0
		BEGIN
			INSERT INTO SchedulerCreations
			(
				DtReg
			)
			VALUES
			(
				@DtReg
			);
		
			SET @SchedulerCreationId = SCOPE_IDENTITY();

			EXEC __spTransactionInsert 'SchedulerCreations', @SchedulerCreationId, 'INSERT', @__AuthId, @__AuthIP;
		END
		ELSE
		BEGIN
			UPDATE
				SchedulerCreations
			SET
				DtReg = @DtReg
			WHERE
				SchedulerCreationId = @SchedulerCreationId;

			EXEC __spTransactionInsert 'SchedulerCreations', @SchedulerCreationId, 'UPDATE', @__AuthId, @__AuthIP;
		END

		SELECT @SchedulerCreationId;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spSchedulerCreationDelete

	@SchedulerCreationId INT,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY

		DELETE FROM SchedulerCreations WHERE SchedulerCreationId = @SchedulerCreationId;

		EXEC __spTransactionInsert 'SchedulerCreations', @SchedulerCreationId, 'DELETE', @__AuthId, @__AuthIP;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spSchedulerStatusPage

	@Page int = 0,
	@PageRows int = 1,
	@Keyword nvarchar(64) = NULL,
    @OrderBy nvarchar(64) = NULL,

    @SchedulerStatusId INT = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL DROP TABLE #TEMP;

	WITH TempResult AS (
		SELECT
			SchedulerStatusId,
			Status
		FROM
			SchedulerStatus
		WHERE
			(@SchedulerStatusId IS NULL OR SchedulerStatusId = @SchedulerStatusId)
		AND
			(
				@Keyword IS NULL
			OR
				Status LIKE '%' + @Keyword + '%'
			)
	), TempCount AS (
		SELECT COUNT(*) AS MaxRows FROM TempResult
	)
	SELECT
		*
	INTO
		#TEMP
	FROM
		TempResult,
		TempCount
	ORDER BY
		TempResult.Status ASC
	OFFSET
		@Page * @PageRows ROWS
	FETCH NEXT
		@PageRows ROWS ONLY;

	SELECT * FROM #TEMP;

	IF EXISTS(SELECT TOP 1 MaxRows FROM #TEMP)
		SELECT TOP 1 MaxRows FROM #TEMP
	ELSE
		SELECT 0 AS MaxRows;

END

GO
CREATE PROCEDURE _spSchedulerStatusGet

    @SchedulerStatusId INT

AS
BEGIN

	SELECT
		SchedulerStatusId,
		Status
	FROM
		SchedulerStatus
	WHERE
		SchedulerStatusId = @SchedulerStatusId

END

GO
CREATE PROCEDURE _spSchedulerStatusListIds


AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		SchedulerStatusId AS [Key],
		Status AS Value
	FROM
		SchedulerStatus
	ORDER BY
		Status ASC;

END
GO

CREATE PROCEDURE _spSchedulerStatuUpsert

	@SchedulerStatusId INT = 0,
	@Status VARCHAR(32) = NULL,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY
		IF @SchedulerStatusId = 0
		BEGIN
			INSERT INTO SchedulerStatus
			(
				Status
			)
			VALUES
			(
				@Status
			);
		
			SET @SchedulerStatusId = SCOPE_IDENTITY();

			EXEC __spTransactionInsert 'SchedulerStatus', @SchedulerStatusId, 'INSERT', @__AuthId, @__AuthIP;
		END
		ELSE
		BEGIN
			UPDATE
				SchedulerStatus
			SET
				Status = @Status
			WHERE
				SchedulerStatusId = @SchedulerStatusId;

			EXEC __spTransactionInsert 'SchedulerStatus', @SchedulerStatusId, 'UPDATE', @__AuthId, @__AuthIP;
		END

		SELECT @SchedulerStatusId;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spSchedulerStatuDelete

	@SchedulerStatusId INT,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY

		DELETE FROM SchedulerStatus WHERE SchedulerStatusId = @SchedulerStatusId;

		EXEC __spTransactionInsert 'SchedulerStatus', @SchedulerStatusId, 'DELETE', @__AuthId, @__AuthIP;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spSchedulerTrackingCodesPage

	@Page int = 0,
	@PageRows int = 1,
	@Keyword nvarchar(64) = NULL,
    @OrderBy nvarchar(64) = NULL,

    @SchedulerId INT = NULL,
    @SchedulerStatusId INT = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL DROP TABLE #TEMP;

	WITH TempResult AS (
		SELECT
			SchedulerId,
			DtReg,
			CpfCnpj,
			SchedulerStatusId,
			DtProcessed,
			ServerChecking,
			DtStart
		FROM
			SchedulerTrackingCodes
		WHERE
			(@SchedulerId IS NULL OR SchedulerId = @SchedulerId)
		AND
			(@SchedulerStatusId IS NULL OR SchedulerStatusId = @SchedulerStatusId)
		AND
			(
				@Keyword IS NULL
			OR
				ServerChecking LIKE '%' + @Keyword + '%'
			)
	), TempCount AS (
		SELECT COUNT(*) AS MaxRows FROM TempResult
	)
	SELECT
		*
	INTO
		#TEMP
	FROM
		TempResult,
		TempCount
	ORDER BY
		TempResult.CpfCnpj ASC
	OFFSET
		@Page * @PageRows ROWS
	FETCH NEXT
		@PageRows ROWS ONLY;

	SELECT * FROM #TEMP;

	IF EXISTS(SELECT TOP 1 MaxRows FROM #TEMP)
		SELECT TOP 1 MaxRows FROM #TEMP
	ELSE
		SELECT 0 AS MaxRows;

END

GO
CREATE PROCEDURE _spSchedulerTrackingCodesGet

    @SchedulerId INT

AS
BEGIN

	SELECT
		SchedulerId,
		DtReg,
		CpfCnpj,
		SchedulerStatusId,
		DtProcessed,
		ServerChecking,
		DtStart
	FROM
		SchedulerTrackingCodes
	WHERE
		SchedulerId = @SchedulerId

END

GO
CREATE PROCEDURE _spSchedulerTrackingCodesListIds

    @SchedulerStatusId INT = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		SchedulerId AS [Key],
		CpfCnpj AS Value
	FROM
		SchedulerTrackingCodes
	WHERE
		(@SchedulerStatusId IS NULL OR SchedulerStatusId = @SchedulerStatusId)
	ORDER BY
		CpfCnpj ASC;

END
GO

CREATE PROCEDURE _spSchedulerTrackingCodeUpsert

	@SchedulerId INT = 0,
	@DtReg DATETIME = NULL,
	@CpfCnpj NVARCHAR(16) = NULL,
	@SchedulerStatusId INT = NULL,
	@DtProcessed DATETIME = NULL,
	@ServerChecking NVARCHAR(32) = NULL,
	@DtStart DATETIME = NULL,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY
		IF @SchedulerId = 0
		BEGIN
			INSERT INTO SchedulerTrackingCodes
			(
				DtReg,
				CpfCnpj,
				SchedulerStatusId,
				DtProcessed,
				ServerChecking,
				DtStart
			)
			VALUES
			(
				@DtReg,
				@CpfCnpj,
				@SchedulerStatusId,
				@DtProcessed,
				@ServerChecking,
				@DtStart
			);
		
			SET @SchedulerId = SCOPE_IDENTITY();

			EXEC __spTransactionInsert 'SchedulerTrackingCodes', @SchedulerId, 'INSERT', @__AuthId, @__AuthIP;
		END
		ELSE
		BEGIN
			UPDATE
				SchedulerTrackingCodes
			SET
				DtReg = @DtReg,
				CpfCnpj = @CpfCnpj,
				SchedulerStatusId = @SchedulerStatusId,
				DtProcessed = @DtProcessed,
				ServerChecking = @ServerChecking,
				DtStart = @DtStart
			WHERE
				SchedulerId = @SchedulerId;

			EXEC __spTransactionInsert 'SchedulerTrackingCodes', @SchedulerId, 'UPDATE', @__AuthId, @__AuthIP;
		END

		SELECT @SchedulerId;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spSchedulerTrackingCodeDelete

	@SchedulerId INT,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY

		DELETE FROM SchedulerTrackingCodes WHERE SchedulerId = @SchedulerId;

		EXEC __spTransactionInsert 'SchedulerTrackingCodes', @SchedulerId, 'DELETE', @__AuthId, @__AuthIP;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spStatesPage

	@Page int = 0,
	@PageRows int = 1,
	@Keyword nvarchar(64) = NULL,
    @OrderBy nvarchar(64) = NULL,

    @StateId INT = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL DROP TABLE #TEMP;

	WITH TempResult AS (
		SELECT
			StateId,
			Name,
			Abbreviation
		FROM
			States
		WHERE
			(@StateId IS NULL OR StateId = @StateId)
		AND
			(
				@Keyword IS NULL
			OR
				Name LIKE '%' + @Keyword + '%'
			OR
				Abbreviation LIKE '%' + @Keyword + '%'
			)
	), TempCount AS (
		SELECT COUNT(*) AS MaxRows FROM TempResult
	)
	SELECT
		*
	INTO
		#TEMP
	FROM
		TempResult,
		TempCount
	ORDER BY
		TempResult.Name ASC
	OFFSET
		@Page * @PageRows ROWS
	FETCH NEXT
		@PageRows ROWS ONLY;

	SELECT * FROM #TEMP;

	IF EXISTS(SELECT TOP 1 MaxRows FROM #TEMP)
		SELECT TOP 1 MaxRows FROM #TEMP
	ELSE
		SELECT 0 AS MaxRows;

END

GO
CREATE PROCEDURE _spStatesGet

    @StateId INT

AS
BEGIN

	SELECT
		StateId,
		Name,
		Abbreviation
	FROM
		States
	WHERE
		StateId = @StateId

END

GO
CREATE PROCEDURE _spStatesListIds


AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		StateId AS [Key],
		Name AS Value
	FROM
		States
	ORDER BY
		Name ASC;

END
GO

CREATE PROCEDURE _spStateUpsert

	@StateId INT = 0,
	@Name NVARCHAR(64),
	@Abbreviation NCHAR(2),

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY
		IF @StateId = 0
		BEGIN
			INSERT INTO States
			(
				Name,
				Abbreviation
			)
			VALUES
			(
				@Name,
				@Abbreviation
			);
		
			SET @StateId = SCOPE_IDENTITY();

			EXEC __spTransactionInsert 'States', @StateId, 'INSERT', @__AuthId, @__AuthIP;
		END
		ELSE
		BEGIN
			UPDATE
				States
			SET
				Name = @Name,
				Abbreviation = @Abbreviation
			WHERE
				StateId = @StateId;

			EXEC __spTransactionInsert 'States', @StateId, 'UPDATE', @__AuthId, @__AuthIP;
		END

		SELECT @StateId;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spStateDelete

	@StateId INT,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY

		DELETE FROM States WHERE StateId = @StateId;

		EXEC __spTransactionInsert 'States', @StateId, 'DELETE', @__AuthId, @__AuthIP;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spTrackingCodesPage

	@Page int = 0,
	@PageRows int = 1,
	@Keyword nvarchar(64) = NULL,
    @OrderBy nvarchar(64) = NULL,

    @TrackingCodeId INT = NULL,
    @CustomerId INT = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL DROP TABLE #TEMP;

	WITH TempResult AS (
		SELECT
			TrackingCodeId,
			DtReg,
			DtActivation,
			CpfCnpj,
			IsActive,
			CustomerId
		FROM
			TrackingCodes
		WHERE
			(@TrackingCodeId IS NULL OR TrackingCodeId = @TrackingCodeId)
		AND
			(@CustomerId IS NULL OR CustomerId = @CustomerId)
		AND
			IsActive = 1
	), TempCount AS (
		SELECT COUNT(*) AS MaxRows FROM TempResult
	)
	SELECT
		*
	INTO
		#TEMP
	FROM
		TempResult,
		TempCount
	ORDER BY
		TempResult.CpfCnpj ASC
	OFFSET
		@Page * @PageRows ROWS
	FETCH NEXT
		@PageRows ROWS ONLY;

	SELECT * FROM #TEMP;

	IF EXISTS(SELECT TOP 1 MaxRows FROM #TEMP)
		SELECT TOP 1 MaxRows FROM #TEMP
	ELSE
		SELECT 0 AS MaxRows;

END

GO
CREATE PROCEDURE _spTrackingCodesGet

    @TrackingCodeId INT

AS
BEGIN

	SELECT
		TrackingCodeId,
		DtReg,
		DtActivation,
		CpfCnpj,
		IsActive,
		CustomerId
	FROM
		TrackingCodes
	WHERE
		TrackingCodeId = @TrackingCodeId
		AND
			IsActive = 1

END

GO
CREATE PROCEDURE _spTrackingCodesListIds

    @CustomerId INT = NULL

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		TrackingCodeId AS [Key],
		CpfCnpj AS Value
	FROM
		TrackingCodes
	WHERE
		(@CustomerId IS NULL OR CustomerId = @CustomerId)
	AND
		IsActive = 1
	ORDER BY
		CpfCnpj ASC;

END
GO

CREATE PROCEDURE _spTrackingCodeUpsert

	@TrackingCodeId INT = 0,
	@DtReg DATETIME = NULL,
	@DtActivation DATETIME = NULL,
	@CpfCnpj NVARCHAR(16) = NULL,
	@IsActive BIT = NULL,
	@CustomerId INT = NULL,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY
		IF @TrackingCodeId = 0
		BEGIN
			INSERT INTO TrackingCodes
			(
				DtReg,
				DtActivation,
				CpfCnpj,
				IsActive,
				CustomerId
			)
			VALUES
			(
				@DtReg,
				@DtActivation,
				@CpfCnpj,
				@IsActive,
				@CustomerId
			);
		
			SET @TrackingCodeId = SCOPE_IDENTITY();

			EXEC __spTransactionInsert 'TrackingCodes', @TrackingCodeId, 'INSERT', @__AuthId, @__AuthIP;
		END
		ELSE
		BEGIN
			UPDATE
				TrackingCodes
			SET
				DtReg = @DtReg,
				DtActivation = @DtActivation,
				CpfCnpj = @CpfCnpj,
				IsActive = @IsActive,
				CustomerId = @CustomerId
			WHERE
				TrackingCodeId = @TrackingCodeId;

			EXEC __spTransactionInsert 'TrackingCodes', @TrackingCodeId, 'UPDATE', @__AuthId, @__AuthIP;
		END

		SELECT @TrackingCodeId;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO


CREATE PROCEDURE _spTrackingCodeDelete

	@TrackingCodeId INT,

	@__AuthId INT = NULL,
	@__AuthIP NVARCHAR(39) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

    BEGIN TRY

		UPDATE TrackingCodes SET IsActive = 0 WHERE TrackingCodeId = @TrackingCodeId;

		EXEC __spTransactionInsert 'TrackingCodes', @TrackingCodeId, 'DELETE', @__AuthId, @__AuthIP;

		COMMIT;

	END TRY
	BEGIN CATCH

		ROLLBACK;

		THROW;

    END CATCH
END

GO

